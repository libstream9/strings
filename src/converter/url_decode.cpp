#include <stream9/strings/converter/url_decode.hpp>

namespace stream9::strings {

namespace url_decode_ {

std::error_category const&
error_category() noexcept
{
    static struct impl : public std::error_category
    {
        char const* name() const noexcept
        {
            return "stream9::srings::url_decode";
        }

        std::string message(int ec) const
        {
            using enum errc;

            switch (static_cast<errc>(ec)) {
                case invalid_hex_character:
                    return "invalid hex character";
            }

            return "unknown error";
        }
    } instance;

    return instance;
}

} // namespace url_decode_

} // namespace stream9::strings
