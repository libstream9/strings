= CharacterPointer

[source,c++]
----
#include <strings/char/concepts.hpp>
namespace strings {
    template<typename S>
    concept bool CharacterPointer =
           std::is_pointer_v<std::remove_reference_t<S>>
        && Character<std::remove_pointer_t<
                         std::remove_reference_t<S> >>;
} // namespace strings
----

== See also

* link:Character.adoc[Character]
