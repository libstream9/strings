= RandomAccessString

[source, c++]
----
#include <strings/core/concepts.hpp>
namespace strings {
    namespace rng = std::ranges;

    template<typename T>
    concept bool RandomAccessString =
        BidirectionalString<T> && rng::RandomAccessIterator<iterator_t<T>>;
} // namespace strings
----

== See also

* link:BidirectionalString.adoc[BidirectionalString]
