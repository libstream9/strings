= CharacterRange

[source,c++]
----
#include <strings/char/concepts.hpp>
namespace strings {
    namespace rng = std::ranges;

    template<typename S>
    concept bool CharacterRange =
           !std::is_array_v<std::remove_reference_t<S>>
        && rng::Range<std::remove_reference_t<S>>
        && Character<rng::iter_value_t<
                         rng::iterator_t<std::remove_reference_t<S>> >>;
} // namespace strings
----

== See also

* link:Character.adoc[Character]
