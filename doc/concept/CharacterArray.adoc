= CharacterArray

[source,c++]
----
#include <strings/char/concepts.hpp>
namespace strings {
    template<typename S>
    concept bool CharacterArray =
           std::is_array_v<std::remove_reference_t<S>>
        && std::rank_v<std::remove_reference_t<S>> == 1
        && Character<std::remove_extent_t<
                        std::remove_reference_t<S> >>;
} // namespace strings
----

== See also

* link:Character.adoc[Character]
