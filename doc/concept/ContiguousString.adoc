= ContiguousString

[source, c++]
----
#include <strings/core/concepts.hpp>
namespace strings {
    namespace rng = std::ranges;

    template<typename T>
    concept bool ContiguousString =
        RandomAccessString<T> && rng::ContiguousIterator<iterator_t<T>>;
} // namespace strings
----

== See also

* link:RandomAccessString.adoc[RandomAccessString]
