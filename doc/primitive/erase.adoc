= erase()

erase character(s) from a string

== Synopsis

[source, c++]
----
#include <strings/primitive/erase.hpp>

namespace strings {
    namespace rng = std::ranges;

    // 1) erase single character
    template<String S, typename IteratorT>
        requires rng::ConvertibleTo<IteratorT, iterator_t<S const>>
              && !std::is_const_v<S>
    iterator_t<S>
    erase(S& s, IteratorT pos1);

    // 2) erase range of characters referred by iterator pair
    template<String S, typename IteratorT, typename SentinelT>
        requires rng::ConvertibleTo<IteratorT, iterator_t<S const>>
              && rng::Sentinel<SentinelT, IteratorT>
              && !std::is_const_v<S>
    iterator_t<S>
    erase(S& s, IteratorT first, SentinelT last);

    // 2) erase range of characters referred by index and length
    template<RandomAccessString S, typename IndexT, typename SizeT>
        requires rng::ConvertibleTo<IndexT, str_index_t<S>>
              && rng::ConvertibleTo<SizeT, str_size_t<S>>
              && !std::is_const_v<S>
    void
    erase(S& s, IndexT pos2, SizeT n);
} // namespace strings
----

== Parameters

[horizontal]
s:: string to modify
[first, last):: range of characters to be erased
pos1:: index of character to be erased
pos2:: start index of substring to be erased
n:: length of substring to be erased

== Return value

1,2) an iterator which refer to right after erased character(s)

== Exception

== Example

{% codetabs name="by iterator", type="c++" -%}
namespace str = strings;

std::string s1 = "123456";

std::string::iterator it = str::erase(s1, s1.begin());

assert(s1 == "23456");
assert(it == s1.begin());

{%- language name="by iterator pair", type="c++" -%}
namespace str = strings;

std::string s1 = "123456";
auto const first = s1.begin() + 2;
auto const lats = first + 2;

std::string::iterator it = str::erase(s1, first, last);

assert(s1 == "1256");
assert(it == s1.begin() + 2);

{%- language name="by index", type="c++" -%}
namespace str = strings;

std::string s1 = "123456";

str::erase(s1, 2, 2);

assert(s1 == "1256");

{%- endcodetabs %}

== See also
