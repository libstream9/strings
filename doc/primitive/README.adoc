= Primitive

Primitive functions act as basis of all other algorithms in this library.
They are also customization points. User defined overloads will be found
and called via ADL.

== Functions

|===
|Name | Description

|link:begin.adoc[begin() / cbegin()] | return begin iterator of a string
|link:end.adoc[end() / cend()] | return end iterator of a string
|link:size.adoc[size()] | return length of a string
|link:insert.adoc[insert()] | insert a character / a string to a string
|link:replace.adoc[replace()] | replace character(s) of a string
|link:erase.adoc[erase()] | erase character(s) from a string
|link:remove_prefix.adoc[remove_prefix()] | remove prefix of a string
|link:suffix_prefix.adoc[remove_suffix()] | remove suffix of a string
|===
