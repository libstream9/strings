= insert()

insert a character / a string into a string

== Synopsis

[source,c++]
----
#include <strings/primitive/insert.hpp>

namespace strings {
    // 1) insert character by index
    template<RandomAccessString S, typename IndexT, Character C>
        requires SameCharacter<S, C>
              && ConvertibleTo<IndexT, str_index_t<S>>
              && !std::is_const_v<S>
    void
    insert(S& s1, IndexT const pos, C const ch);

    // 2) insert character by iterator
    template<String S, typename IteratorT, Character C>
        requires SameCharacter<S, C>
              && ConvertibleTo<IteratorT, iterator_t<S const>>
              && !std::is_const_v<S>
    iterator_t<S>
    insert(S& s1, IteratorT const pos, C const ch);

    // 3) insert string by index
    template<RandomAccessString S1, typename IndexT, String S2>
        requires ConvertibleTo<IndexT, str_index_t<S1>>
              && SameCharacter<S1, S2>
              && !std::is_const_v<S1>
    void
    insert(S1& s1, IndexT const pos, S2&& s2);

    // 4) insert string by iterator
    template<String S1, typename IteratorT, String S2>
        requires ConvertibleTo<IteratorT, iterator_t<S1 const>>
              && SameCharacter<S1, S2>
              && !std::is_const_v<S1>
    iterator_t<S1>
    insert(S1& s1, IteratorT const pos, S2&& s2);
} // namespace strings
----

== Parameters

[horizontal]
s1:: string to be inserted
s2:: string to insert
ch:: character to insert
pos:: position to insert

== Return value

2) and 3) return an iterator which point to a beginning of
inserted element.

== Example

{% codetabs name="character", type="c++" -%}
namespace str = strings;

std::string s1 = "123";

// by iterator
std::string::iterator it1 = str::insert(s1, s1.end(), '4');
assert(s1 == "1234");
assert(it1 == s1.begin() + 3);

// by index
str::insert(s1, 2, 'X');
assert(s1 == "12X34");

{%- language name="string", type="c++" -%}
namespace str = strings;

std::string s1 = "123";

// by iterator
std::string::iterator it1 = str::insert(s1, s1.end(), "456");
assert(s1 == "123456");
assert(it1 == s1.begin() + 3);

// by index
str::insert(s1, 6, "789");
assert(s1 == "123456789");

{%- endcodetabs %}

== See also
