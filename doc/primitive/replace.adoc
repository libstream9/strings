= replace()

replace character(s) of a string

== Synopsis

[source, c++]
----
#include <strings/primitive/replace.hpp>

namespace strings {
        namespace rng = std::ranges;

        // 1) specify replaced range by iterator
        template<String S1, typename IteratorT, typename SentinelT, String S2>
            requires rng::ConvertibleTo<IteratorT, iterator_t<S1 const>>
                  && rng::Sentinel<SentinelT, IteratorT>
                  && SameCharacter<S1, S2>
                  && !std::is_const_v<S1>
        iterator_t<S1>
        replace(S1& s1, IteratorT first, SentinelT last, S2&& s2);

        // 2) specify replace range by index and length
        template<RandomAccessString S1, typename IndexT, typename SizeT, String S2>
            requires rng::ConvertibleTo<IndexT, str_index_t<S1>>
                  && rng::ConvertibleTo<SizeT, str_size_t<S1>>
                  && SameCharacter<S1, S2>
                  && !std::is_const_v<S1>
        void
        replace(S1& s1, IndexT pos, SizeT n, S2&& s2);
} // namespace strings
----

== Parameters

[horizontal]
s1:: string to modify
s2:: string to use for replacement
[first, last):: range of characters to be replaced
pos:: start index of substring to be replaced
n:: length of substring to be replaced

== Return value

1) an iterator which refer to beginning of replacement

== Exception

== Example

{% codetabs name="by iterator", type="c++" -%}
namespace str = strings;

std::string s1 = "123456";

auto from = s1.begin() + 2;
auto to = from + 2;

std::string::iterator it = str::replace(s1, from, to, "XYZ");

assert(s1 == "12XYZ56");
assert(it == (s1.begin() + 2));

{%- language name="by index", type="c++" -%}
namespace str = strings;

std::string s1 = "123456";

str::replace(s1, 2, 2, "XYZ");

assert(s1 == "12XYZ56");

{%- endcodetabs %}

== See also
