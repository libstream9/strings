= end() / cend()

return an iterator that refer to past-the-end element of a string

== Synopsis

{% codetabs name="end", type="c++" -%}
#include <strings/primitive/end.hpp>

namespace strings {
    // 1) from a pointer to a null terminated string
    template<Character C>
    constexpr C*
    end(C* const& ptr);

    // 2) from an array to characters
    template<Character C, std::size_t N>
    constexpr C*
    end(C (&arr)[N]);

    // 3) from a Range of characters
    template<CharacterRange S>
    constexpr auto
    end(S&& s);
} // namespace strings

{%- language name="cend", type="c++" -%}
#include <strings/primitive/end.hpp>

namespace strings {
    // 1) from a pointer to a null terminated string
    template<Character C>
    constexpr C const* 
    cend(C const* const& ptr);

    // 2) from an array to characters
    template<Character C, std::size_t N>
    constexpr C const*
    cend(C const (&arr)[N]);

    // 3) from a Range of characters
    template<CharacterRange S>
    constexpr auto
    cend(S&& s);
} // namespace strings

{%- endcodetabs %}

== Parameters
[horizontal]
ptr:: pointer to null terminated characters
arr:: character array
s:: Range that have Character as value type

== Return value

An iterator to the end of a string

== Exception

== Example
{% codetabs name="array", type="c++" -%}
namespace str = strings;

char const arr1[] = "Hello";
char const* it1 = str::end(arr1);
assert(it1 == &arr1[5]);

char const arr2[] = { 'X', '\0', 'Y', '\0', '\0' };
char const* it2 = str::end(arr2);
assert(it1 == &arr2[3]); // not the last null

char const arr3[] = { '1', '2', '3' };
char const* it3 = str::end(arr3);
// same as std::ranges::end when an array isn't null terminated
assert(it1 == &arr3[3]); 

{%- language name="pointer", type="c++" -%}
namespace str = strings;

char const* ptr1 = "Hello";
char const* it1 = str::end(ptr1);
assert(it1 == (ptr1 + 5)); // points to the null terminator

// what if string contains null in the middle
char const* ptr2 = "Hello\0World";
char const* it2 = str::end(ptr2);
assert(it2 == (ptr2 + 5));

{%- language name="range", type="c++" -%}
namespace str = strings;

std::string s1 = "Hello";
std::string::iterator it1 = str::end(s1);
assert(&*it1 == &s[5]);

// string can contain null if it is properly ranged
std::string_view s2 { "Hello\0World", 11 };
assert(str::end(s2) == s2.end());

{%- endcodetabs %}

== See also
. link:begin.adoc[begin]

