= Strings library

Generic algorithm collection for string, 
built on top of Concepts TS and Ranges TS.

== Feature

* design is based on boost string library
* algorithm's interface are specified in term of concepts
* provide safer abstraction for string than Ranges library
* String concept is loosely defined as range of characters
* as consequence, library can support various string representations
* support various regex engines
* bridge String types to a iostream interface (not just std::string)

== Requirements

* C++ compiler that support Concepts TS (GCC 7+ for example)
* link:https://github.com/CaseyCarter/cmcstl2[Ranges TS implementation]
* link:https://cmake.org[CMake]
* link:https://www.boost.org/[boost library] (if you want build test suites)

= Table of contents

. link:concept/README.adoc[Concepts]
. link:type/README.adoc[Types]
. link:primitive/README.adoc[Primitives]
