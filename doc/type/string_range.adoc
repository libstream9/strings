= string_range

std::ranges::subrange derivative for string

== Synopsis

[source,c++]
----
#include <strings/core/string_range.hpp>

namespace strings {
    namespace rng = std::ranges;

    template<rng::Iterator I, rng::Sentinel<I> S = I,
             rng::subrange_kind K = <depends on S is sized or not> >
        requires (K == rng::subrange_kind::sized || !rng::SizedSentinel<S, I>)
              && Character<rng::iter_value_t<I>>
    class string_range : public rng::subrange<I, S, K>
                       , public string_interface<string_range<I, S, K>>
    {
        using base_t = rng::subrange<I, S, K>;
    public:
        // constructor
        string_range() = default;

        template<String T>
            requires rng::ConvertibleTo<iterator_t<T>, I>
                  && rng::ConvertibleTo<sentinel_t<T>, S>
        string_range(T&& s);

        template<String T, typename SizeT>
            requires rng::ConvertibleTo<iterator_t<T>, I>
                  && rng::ConvertibleTo<sentinel_t<T>, S>
                  && rng::ConvertibleTo<SizeT, rng::iter_difference_t<I>>
                  && K == rng::subrange_kind::sized
        string_range(T&& s, SizeT const n);

        using base_t::base_t;

        // modifier
        void
        remove_prefix(rng::iter_difference_t<I> const n)
            requires rng::RandomAccessIterator<I>
            [[expects: n >= 0]];

        void
        remove_suffix(rng::iter_difference_t<I> const n)
            requires rng::RandomAccessIterator<I>
            [[expects: n >= 0]];
    };
} // namespace strings
----

== Description

std::ranges::subrange extract iterators with std::ranges::begin
and std::ranges::end therefore it can't handle null teminated character
pointer also it can't handle character array correctly.
string_range is a derivative of std::ranges::subrange so that it can
handle character pointer and character array.

It also supply capability specifically applicable to string.

== Example

{% codetabs name="construct from range", type="c++" -%}
namespace str = strings;

std::string s1 = "foo";
str::string_range r1 = s1;

{%- language name="construct from array", type="c++" -%}
namespace str = strings;
namespace rng = std::ranges;

char const s2[] = "foo";
str::string_range r2 = s2;
assert(r2.size() == 3);
assert(r2 == "foo");

// subrange doesn't know null teminated string semantics
rng::subrange r3 = s2;
assert(r3.size() == 4);
assert(r2[3] == '\0');

{%- language name="construct from pointer", type="c++" -%}
namespace str = strings;

char const* s3 = "foo";
str::string_range r3 = s3;
assert(r3.size() == 3);
assert(r3 == "foo");

{%- endcodetabs %}
