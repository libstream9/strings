#ifndef STRINGS_CONVERTER_TO_STRING_HPP
#define STRINGS_CONVERTER_TO_STRING_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"
#include "../core/float_format.hpp"
#include "../modifier/clear.hpp"
#include "../utility/cstring_view.hpp"

#include "../stream.hpp"

#include <algorithm>
#include <chrono>
#include <concepts>
#include <iomanip>
#include <iterator>
#include <locale>
#include <ratio>
#include <string>
#include <type_traits>

namespace stream9::strings {

namespace _to_string {

    template<typename S, typename V>
    inline constexpr bool streamable =
        requires (str::ostream<_unref<S>> s, V v) {
            s << v;
        };

    template<typename S>
    inline constexpr bool can_clear =
        requires (S s) {
            str::clear(s);
        };

    template<typename S>
    inline constexpr bool can_construct_string_from_iter_pair_v =
        requires (S&& s) {
            std::basic_string<character_t<S>> {
                str::begin(s), str::end(s)
            };
        };

    template<typename S1, typename S2>
    inline constexpr bool can_construct_from_iter_pair_v =
        requires (S2&& s) {
            S1 {
                str::begin(s), str::end(s)
            };
        };

    template<typename S>
    void
    from_bool(S&& s, bool const v, std::locale const& loc)
    {
        str::ostream os { s };
        os.imbue(loc);

        os << std::boolalpha << v;
    }

    template<typename S, typename I>
    void
    from_integer(S&& s, I const v, int base, std::locale const& loc)
    {
        str::ostream os { s };
        os.imbue(loc);

        os << std::setbase(base) << v;
    }

    template<typename S, typename F>
    void
    from_floating_point(S&& s, F const v,
               float_format const fmt, std::locale const& loc)
    {
        str::ostream os { s };
        os.imbue(loc);

        switch (fmt) {
            case float_format::general:
            default:
                os.unsetf(std::ios_base::floatfield);
                break;
            case float_format::scientific:
                os.setf(std::ios_base::scientific, std::ios_base::floatfield);
                break;
            case float_format::fixed:
                os.setf(std::ios_base::fixed, std::ios_base::floatfield);
                break;
            case float_format::hex:
                os.setf(std::ios_base::scientific | std::ios_base::fixed,
                        std::ios_base::floatfield);
                break;
        }

        os << v;
    }

    template<typename Clock>
    concept has_to_sys = requires {
        Clock::to_sys(Clock::now());
    };

    template<typename Clock, typename Duration, typename X = std::false_type>
    static std::chrono::sys_time<Duration>
    to_sys_time(std::chrono::time_point<Clock, Duration> const& tp)
    {
        if constexpr (std::same_as<Clock, std::chrono::system_clock>) {
            return tp;
        }
        else if constexpr (has_to_sys<Clock>) {
            return Clock::to_sys(tp);
        }
        else {
            static_assert(X(), "can't convert to sys_time");
        }
    }

    template<typename Rep, typename Period>
    static std::string
    from_duration(std::chrono::duration<Rep, Period> const& d)
    {
        std::string result = std::to_string(d.count());

        if constexpr (std::same_as<Period, std::nano>) {
            result += "ns";
        }
        else if constexpr (std::same_as<Period, std::micro>) {
            result += "us";
        }
        else if constexpr (std::same_as<Period, std::milli>) {
            result += "ms";
        }
        else if constexpr (std::same_as<Period, std::ratio<1>>) {
            result += "s";
        }
        else if constexpr (std::same_as<Period, std::ratio<60>>) {
            result += "min";
        }
        else if constexpr (std::same_as<Period, std::ratio<3600>>) {
            result += "hour";
        }
        else if constexpr (std::same_as<Period, std::ratio<86400>>) {
            result += "day";
        }
        else if constexpr (std::same_as<Period, std::ratio<604800>>) {
            result += "week";
        }
        else if constexpr (std::same_as<Period, std::ratio<2629746>>) {
            result += "month";
        }
        else if constexpr (std::same_as<Period, std::ratio<31556952>>) {
            result += "year";
        }

        return result;
    }

    struct api
    {
        template<string S>
            requires streamable<S, bool>
                  && can_clear<S>
        void
        operator()(bool const v, S&& s, std::locale const& loc = {}) const
        {
            str::clear(s);
            (from_bool)(s, v, loc);
        }

        template<string S, std::integral I>
            requires streamable<S, I>
                  && can_clear<S>
                  && (!std::same_as<_uncv<I>, bool>)
        void
        operator()(I const v, S&& s, int base = 0, std::locale const& loc = {}) const
        {
            str::clear(s);
            (from_integer)(s, v, base, loc);
        }

        template<string S, typename F>
            requires std::is_floating_point_v<F>
                  && can_clear<S>
                  && streamable<S, F>
        void
        operator()(F const v, S&& s,
                   float_format const fmt = float_format::general,
                   std::locale const& loc = {}) const
        {
            str::clear(s);
            (from_floating_point)(s, v, fmt, loc);
        }

        template<string S1>
            requires can_construct_from_iter_pair_v<std::basic_string<character_t<S1>>, S1>
        std::basic_string<character_t<S1>>
        operator()(S1&& s) const
        {
            return { str::begin(s), str::end(s) };
        }

        template<string S1>
            requires (!can_construct_from_iter_pair_v<std::basic_string<character_t<S1>>, S1>)
        std::basic_string<character_t<S1>>
        operator()(S1&& s) const
        {
            std::basic_string<character_t<S1>> result;
            rng::copy(s, std::back_inserter(result));

            return result;
        }

        template<typename Clock, typename Duration>
        std::string
        operator()(std::chrono::time_point<Clock, Duration> const& tp,
                   cstring_view const fmt = "%F %T") const
        {
            std::string result;
            result.resize(50);

            auto const stp = to_sys_time(tp); //TODO switch to C++20 clock_cast when it become available
            auto const t = std::chrono::system_clock::to_time_t(stp);

            auto const len = strftime(
                result.data(), result.size(), fmt, localtime(&t));

            result.resize(len);

            return result;
        }

        std::string
        operator()(std::timespec const& t,
                   cstring_view const fmt = "%F %T") const
        {
            std::string result;
            result.resize(50);

            struct ::tm tm {};
            auto const rv = ::localtime_r(&t.tv_sec, &tm);
            assert(rv != nullptr);//TODO nullptr on error

            auto const len = std::strftime(
                result.data(), result.size(), fmt, &tm);

            result.resize(len);

            return result;
        }

        template<typename Rep, typename Period>
        std::string
        operator()(std::chrono::duration<Rep, Period> const& d) const
        {
            return (from_duration)(d);
        }

        template<typename T>
            requires (!string<T>)
                  && streamable<std::string, T>
        std::string
        operator()(T&& v) const
        {
            std::string result;
            str::ostream os { result };
            os << v;

            os.exceptions(os.badbit);

            return result;
        }

    };

} // namespace _to_string

inline constexpr _to_string::api to_string;

} // namespace stream9::strings

#endif // STRINGS_CONVERTER_TO_STRING_HPP
