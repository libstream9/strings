#ifndef STREAM9_STRINGS_CONVERTER_FROM_STRING_HPP
#define STREAM9_STRINGS_CONVERTER_FROM_STRING_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"
#include "../core/float_format.hpp"
#include "../core/string_range.hpp"
#include "../core/string_traits.hpp"
#include "../accessor/begin.hpp"
#include "../accessor/end.hpp"
#include "../query/size.hpp"
#include "../query/equal.hpp"
#include "../utility/cstring_view.hpp"

#include "../error.hpp"
#include "../stream.hpp"

#include <charconv>
#include <chrono>
#include <concepts>
#include <iomanip>
#include <ios>
#include <limits>
#include <locale>
#include <system_error>
#include <type_traits>
#include <utility>

#include <time.h>

#include <stream9/tag_invoke.hpp>

namespace stream9::strings {

struct from_string_tag {};

template<typename Ret, str::readable_string S, typename... Args>
    requires std::is_default_constructible_v<Ret>
          && tag_invocable<from_string_tag, Ret&, S, Args...>
Ret
from_string(S&& s, Args&&... a)
    noexcept(nothrow_tag_invocable<from_string_tag, Ret&, S, Args...>)
{
    Ret result;

    stream9::tag_invoke(from_string_tag(),
        result, std::forward<S>(s), std::forward<Args>(a)... );

    return result;
}

} // namespace stream9::strings

namespace stream9::strings::from_string_ {

    enum class errc {
        fail_to_convert_from_istream = 100,
        invalid_character,
        invalid_bool_string,
        internal_error,
    };

    inline std::error_category const&
    error_category() noexcept //TODO move to cpp file
    {
        static struct impl : std::error_category
        {
            char const* name() const noexcept override
            {
                return "stream9::strings::from_string";
            }

            std::string message(int const ec) const override
            {
                using enum errc;

                switch (static_cast<errc>(ec)) {
                    case fail_to_convert_from_istream:
                        return "fail to convert with istream";
                    case invalid_character:
                        return "invalid character";
                    case invalid_bool_string:
                        return "invalid bool string";
                    case internal_error:
                        return "internal error";
                }

                return "unknown error";
            }
        } instance;

        return instance;
    }

    inline std::error_code
    make_error_code(errc const e) noexcept
    {
        return std::error_code(static_cast<int>(e), error_category());
    }

    template<typename S>
    inline std::string
    make_context(S&& s, std::ptrdiff_t const pos)
    {
        std::string cxt;
        str::ostream os { cxt };
        os << R"({"s":")" << s << R"(",)";
        os << R"("pos":)" << pos << R"(})";

        return cxt;
    }

} // namespace stream9::strings::from_string_

namespace std {

template<>
struct is_error_code_enum<stream9::strings::from_string_::errc>
    : true_type {};

} // namespace std

namespace stream9::strings {

namespace from_string_ {

    template<typename T, typename U>
    bool
    is_out_of_range(U const i)
    {
        return i < std::numeric_limits<T>::min()
            || i > std::numeric_limits<T>::max();
    }

    inline std::string_view
    to_string(float_format const fmt)
    {
        using enum float_format;

        switch (fmt) {
            case scientific:
                return "scientific";
            case fixed:
                return "fixed";
            case hex:
                return "hex";
            case general:
                return "general";
        }
        return "unknown";
    }

    template<typename S, typename T>
    void
    to_integer(S&& s, T& v, int base, std::locale const& loc)
    {
        str::istream is { s };
        is.imbue(loc);

        is >> std::setbase(base) >> v;

        if (is.fail()) {
            auto const pos = is.rdbuf()->pubseekoff(0, is.cur, is.in);

            throw error {
                errc::fail_to_convert_from_istream,
                make_context(s, pos),
            };
        }
    }

    template<typename S, typename T>
    void
    to_integer(S&& s_, T& v)
    {
        if constexpr (std::convertible_to<std::decay_t<S>, std::string_view>) {
            std::string_view s { s_ };
            auto const rv = std::from_chars(s.begin(), s.end(), v);
            if (rv.ec != std::errc()) {
                throw error {
                    "std::from_char()",
                    make_error_code(rv.ec),
                    make_context(s, rv.ptr - s.begin())
                };
            }
            else if (rv.ptr != s.end()) {
                throw error {
                    "std::from_char()",
                    errc::invalid_character,
                    make_context(s, rv.ptr - s.begin())
                };
            }
        }
        else {
            (to_integer)(std::forward<S>(s_), v, 0, {});
        }
    }

    template<typename S, typename T>
    void
    to_floating_point(S&& s, T& v,
                      str::float_format fmt, std::locale const& loc)
    {
        using f = str::float_format;

        str::istream is { s };
        is.imbue(loc);

        switch (fmt) {
            case f::general:
            default:
                is.unsetf(std::ios_base::floatfield);
                break;
            case f::scientific:
                is.setf(std::ios_base::scientific, std::ios_base::floatfield);
                break;
            case f::fixed:
                is.setf(std::ios_base::fixed, std::ios_base::floatfield);
                break;
            case f::hex:
                is.setf(std::ios_base::scientific | std::ios_base::fixed,
                        std::ios_base::floatfield);
                break;
        }

        is >> v;

        if (is.fail()) {
            auto const pos = is.rdbuf()->pubseekoff(0, is.cur, is.in);

            throw error {
                errc::fail_to_convert_from_istream,
                make_context(s, pos),
            };
        }
    }

    template<typename  S, typename T>
    void
    to_floating_point(S&& s_, T& v, str::float_format fmt_)
    {
        using F1 = str::float_format;
        using F2 = std::chars_format;

        if constexpr (std::convertible_to<std::decay_t<S>, std::string_view>) {
            std::string_view s { s_ };

            std::chars_format fmt;
            switch (fmt_) {
                case F1::general:
                    fmt = F2::general;
                    break;
                case F1::scientific:
                    fmt = F2::scientific;
                    break;
                case F1::fixed:
                    fmt = F2::fixed;
                    break;
                default:
                    assert(fmt_ == F1::hex);
                    fmt = F2::fixed;
                    break;
            }

            auto const rv = std::from_chars(s.begin(), s.end(), v, fmt);
            if (rv.ec != std::errc()) {
                throw error {
                    "std::from_char()",
                    make_error_code(rv.ec),
                    make_context(s, rv.ptr - s.begin())
                };
            }
            else if (rv.ptr != s.end()) {
                throw error {
                    "std::from_char()",
                    errc::invalid_character,
                    make_context(s, rv.ptr - s.begin())
                };
            }
        }
        else {
            to_floating_point(std::forward<S>(s_), v, fmt_, {});
        }
    }

    template<typename Clock>
    concept has_from_sys = requires {
        Clock::from_sys(std::chrono::system_clock::now());
    };

    template<typename Clock, typename Duration, typename X = std::false_type>
    std::chrono::time_point<Clock, Duration>
    from_sys_time(std::chrono::sys_time<Duration> const& stp)
    {
        if constexpr (std::same_as<Clock, std::chrono::system_clock>) {
            return stp;
        }
        else if constexpr (has_from_sys<Clock>) {
            return Clock::from_sys(stp);
        }
        else {
            static_assert(X(), "can't convert from sys_time");
        }
    }


    template<typename Clock, typename Duration>
    void
    to_time_point(str::cstring_view const s,
                  std::chrono::time_point<Clock, Duration>& v,
                  str::cstring_view const fmt = "%F %T")
    {
        namespace chrono = std::chrono;

        struct tm tm {};
        auto* const last = ::strptime(s.data(), fmt, &tm);
        if (last == nullptr) {
            throw internal_error {
                "strptime()",
            };
        }
        else {
            auto const t = timelocal(&tm);

            auto const system_tp = chrono::system_clock::from_time_t(t);
            v = chrono::time_point_cast<Duration>(from_sys_time<Clock>(system_tp));
        }
    }

} // namespace from_string_

/*
 * bool
 */
template<str::forward_string S>
void
tag_invoke(from_string_tag, bool& result, S&& s)
{
    if (str::iequal(s, "true")) {
        result = true;
    }
    else if (str::iequal(s, "false")) {
        result = false;
    }
    else {
        throw error {
            from_string_::errc::invalid_bool_string,
            from_string_::make_context(s, 0),
        };
    }
}

template<str::forward_string S>
void
tag_invoke(from_string_tag, bool& result, S&& s, std::locale const& loc)
{
    if (str::iequal(s, "true", loc)) {
        result = true;
    }
    else if (str::iequal(s, "false", loc)) {
        result = false;
    }
    else {
        throw error {
            from_string_::errc::invalid_bool_string,
            from_string_::make_context(s, 0),
        };
    }
}

/*
 * integral
 */
template<std::integral T, str::forward_string S>
    requires (!std::same_as<T, bool>)
          && (!std::is_const_v<T>)
void
tag_invoke(from_string_tag, T& result, S&& s)
{
    try {
        from_string_::to_integer(std::forward<S>(s), result);
    }
    catch (...) {
        std::string cxt;
        str::ostream os { cxt };
        os << R"({"s":")" << s << R"("})";

        throw_with_internal_error(std::move(cxt));
    }
}

template<std::integral T, str::forward_string S>
    requires (!std::same_as<T, bool>)
          && (!std::is_const_v<T>)
void
tag_invoke(from_string_tag, T& result, S&& s, int const base)
{
    try {
        from_string_::to_integer(std::forward<S>(s), result, base, {});
    }
    catch (...) {
        std::string cxt;
        str::ostream os { cxt };
        os << R"({"s":")" << s << R"(",)";
        os << R"("base":)" << base << R"(})";

        throw_with_internal_error(std::move(cxt));
    }
}

template<std::integral T, str::forward_string S>
    requires (!std::same_as<T, bool>)
          && (!std::is_const_v<T>)
void
tag_invoke(from_string_tag, T& result, S&& s, std::locale const& loc)
{
    try {
        from_string_::to_integer(std::forward<S>(s), result, 10, loc);
    }
    catch (...) {
        std::string cxt;
        str::ostream os { cxt };
        os << R"({"s":")" << s << R"(",)";
        os << R"("loc":")" << loc.name() << R"("})";

        throw_with_internal_error(std::move(cxt));
    }
}

template<std::integral T, str::forward_string S>
    requires (!std::same_as<T, bool>)
          && (!std::is_const_v<T>)
void
tag_invoke(from_string_tag, T& result, S&& s,
           int const base, std::locale const& loc)
{
    try {
        from_string_::to_integer(std::forward<S>(s), result, base, loc);
    }
    catch (...) {
        std::string cxt;
        str::ostream os { cxt };
        os << R"({"s":")" << s << R"(",)";
        os << R"("base":)" << base << R"(,)";
        os << R"("loc":")" << loc.name() << R"("})";

        throw_with_internal_error(std::move(cxt));
    }
}

/*
 * floating point
 */
template<typename T, str::forward_string S>
    requires std::is_floating_point_v<T>
          && (!std::is_const_v<T>)
void
tag_invoke(from_string_tag, T& result, S&& s,
           str::float_format fmt = str::float_format::general)
{
    try {
        from_string_::to_floating_point(std::forward<S>(s), result, fmt);
    }
    catch (...) {
        std::string cxt;
        str::ostream os { cxt };
        os << R"({"s":")" << s << R"(",)";
        os << R"("fmt":")" << from_string_::to_string(fmt) << R"("})";

        throw_with_internal_error(std::move(cxt));
    }
}

template<typename T, str::forward_string S>
    requires std::is_floating_point_v<T>
          && (!std::is_const_v<T>)
void
tag_invoke(from_string_tag, T& result, S&& s,
           std::locale const& loc)
{
    try {
        from_string_::to_floating_point(
            std::forward<S>(s), result, str::float_format::general, loc);
    }
    catch (...) {
        std::string cxt;
        str::ostream os { cxt };
        os << R"({"s":")" << s << R"(",)";
        os << R"("loc":")" << loc.name() << R"("})";

        throw_with_internal_error(std::move(cxt));
    }
}

template<typename T, str::forward_string S>
    requires std::is_floating_point_v<T>
          && (!std::is_const_v<T>)
void
tag_invoke(from_string_tag, T& result, S&& s,
           str::float_format fmt,
           std::locale const& loc)
{
    try {
        from_string_::to_floating_point(std::forward<S>(s), result, fmt, loc);
    }
    catch (...) {
        std::string cxt;
        str::ostream os { cxt };
        os << R"({"s":")" << s << R"(",)";
        os << R"("fmt":")" << from_string_::to_string(fmt) << R"(",)";
        os << R"("loc":")" << loc.name() << R"("})";

        throw_with_internal_error(std::move(cxt));
    }
}

/*
 * time point
 */
template<typename Clock, typename Duration>
void
tag_invoke(from_string_tag,
           std::chrono::time_point<Clock, Duration>& v,
           str::cstring_view s,
           str::cstring_view fmt = "%F %T")
{
    try {
        from_string_::to_time_point(s, v, fmt);
    }
    catch (...) {
        std::string cxt;
        str::ostream os { cxt };
        os << R"({"s":")" << s << R"(",)";
        os << R"("fmt":")" << fmt << R"("})";

        throw_with_internal_error(std::move(cxt));
    }
}

} // namespace stream9::strings

#endif // STREAM9_STRINGS_CONVERTER_FROM_STRING_HPP
