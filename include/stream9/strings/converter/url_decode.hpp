#ifndef STREAM9_STRINGS_CONVERTER_URL_DECODE_HPP
#define STREAM9_STRINGS_CONVERTER_URL_DECODE_HPP

#include "../error.hpp"
#include "../namespace.hpp"
#include "../core/concepts.hpp"
#include "../core/string_traits.hpp"
#include "../accessor/begin.hpp"
#include "../accessor/end.hpp"
#include "../query/size.hpp"

#include <sstream>
#include <string>
#include <system_error>

#include <stream9/errors.hpp>

namespace stream9::strings::url_decode_ {

    enum class errc {
        invalid_hex_character = 100,
    };

    std::error_category const& error_category() noexcept;

    inline std::error_code make_error_code(errc const e) noexcept
    {
        return { static_cast<int>(e), error_category() };
    }

} // namespace stream9::strings::url_decode_

namespace std {

template<>
struct is_error_code_enum<stream9::strings::url_decode_::errc>
    : true_type {};

} // namespace std

namespace stream9::strings {

namespace url_decode_ {

    inline char
    hex_to_int(char const c)
    {
        if ('0' <= c && c <= '9') {
            return c - '0';
        }
        else if ('A' <= c && c <= 'Z') {
            return 10 + c - 'A';
        }
        else if ('a' <= c && c <= 'z') {
            return 10 + c - 'a';
        }
        else {
            std::error_code e { errc::invalid_hex_character };
            throw error(errc::invalid_hex_character);
        }
    }

    struct api
    {
        template<str::readable_string S>
        std::basic_string<str_char_t<S>>
        operator()(S&& s) const
        {
            using Ch = str_char_t<S>;

            try {
                std::basic_string<Ch> result;
                result.reserve(str::size(s));

                for (auto it = str::begin(s); it != str::end(s);) {
                    auto const c = *it;
                    if (c != '%') {
                        result.push_back(c);
                        ++it;
                    }
                    else {
                        int decoded;

                        ++it;
                        decoded = hex_to_int(*it) << 4;

                        ++it;
                        decoded |= hex_to_int(*it);

                        result.push_back(static_cast<Ch>(decoded));
                        ++it;
                    }
                }
                return result;
            }
            catch (error const&) {
                throw;
            }
            catch (...) {
                std::ostringstream oss;
                oss << "{{\"s\",\"" << s << "\"}}"; //TODO convert s_ to utf8

                err::throw_with_internal_error(std::move(oss).str());
            }
        }
    };

} // namespace url_decode_

inline constexpr url_decode_::api url_decode;

} // namespace stream9::strings

#endif // STREAM9_STRINGS_CONVERTER_URL_DECODE_HPP
