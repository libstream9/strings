#ifndef STREAM9_STRINGS_CONVERTER_JOIN_HPP
#define STREAM9_STRINGS_CONVERTER_JOIN_HPP

#include "to_string.hpp"

#include "../core/concepts.hpp"
#include "../core/string_traits.hpp"
#include "../view/join.hpp"

namespace stream9::strings {

namespace join_ {

    struct api
    {
        template<rng::range InputT, forward_string SeparatorT>
            requires forward_string<std::iter_value_t<iterator_t<InputT>>>
                  && is_same_char_v<std::iter_value_t<iterator_t<InputT>>, SeparatorT>
        auto
        operator()(InputT&& inputs, SeparatorT&& sep) const
        {
            return to_string(
                views::join(std::move(inputs), std::move(sep))
            );
        }
    };

} // namespace join_

inline constexpr join_::api join;

} // namespace stream9::strings

#endif // STREAM9_STRINGS_CONVERTER_JOIN_HPP
