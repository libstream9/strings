#ifndef STREAM9_STRINGS_ERROR_HPP
#define STREAM9_STRINGS_ERROR_HPP

#include <stream9/errors.hpp>

namespace stream9::strings {

using stream9::errors::error;
using stream9::errors::internal_error;

using stream9::errors::throw_with_internal_error;

} // namespace stream9::strings

#endif // STREAM9_STRINGS_ERROR_HPP
