#ifndef STRINGS_QUERY_CONTAINS_HPP
#define STRINGS_QUERY_CONTAINS_HPP

#include "../core/concepts.hpp"
#include "../core/string_traits.hpp"

#include "find_first.hpp"

#include <concepts>

namespace stream9::strings {

namespace _contains {

    struct api
    {
        template<forward_string S, character C, typename PredicateT>
            requires is_same_char_v<S, C>
                  && std::predicate<PredicateT, C, C>
        bool
        operator()(S&& s, C const c, PredicateT comp) const
        {
            return !!find_first(s, c, comp);
        }

        template<forward_string S, character C>
            requires is_same_char_v<S, C>
        bool
        operator()(S&& s, C const c) const
        {
            return !!find_first(s, c);
        }

        template<forward_string S1, forward_string S2, typename PredicateT>
            requires is_same_char_v<S1, S2>
                  && std::predicate<PredicateT, str_char_t<S1>, str_char_t<S1>>
        bool
        operator()(S1&& s1, S2&& s2, PredicateT comp) const
        {
            return !!find_first(s1, s2, comp);
        }

        template<forward_string S1, forward_string S2>
            requires is_same_char_v<S1, S2>
        bool
        operator()(S1&& s1, S2&& s2) const
        {
            return !!find_first(s1, s2);
        }
    };

} // namespace _contains

inline constexpr _contains::api contains;

namespace _icontains {

    struct api
    {
        template<forward_string S, character C>
            requires is_same_char_v<S, C>
        bool
        operator()(S&& s, C const c) const
        {
            return !!ifind_first(s, c);
        }

        template<forward_string S1, forward_string S2>
            requires is_same_char_v<S1, S2>
        bool
        operator()(S1&& s1, S2&& s2) const
        {
            return !!ifind_first(s1, s2);
        }
    };

} // namespace _icontains

inline constexpr _icontains::api icontains;

} // namespace stream9::strings

#endif // STRINGS_QUERY_CONTAINS_HPP
