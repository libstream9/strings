#ifndef STRINGS_QUERY_SIZE_HPP
#define STRINGS_QUERY_SIZE_HPP

#include "../namespace.hpp"

#include "../core/char/concepts.hpp"

#include "../detail/pointer.hpp"

#include <concepts>
#include <ranges>

namespace stream9::strings {

namespace size_ {

    template<typename S>
    concept has_member_size = requires (S s) {
        { s.size() } -> std::integral;
    };

    struct api {

        template<character C>
        constexpr auto
        operator()(C* const& s) const
        {
            return detail::ptr_length(s);
        }

        template<character C, std::size_t N>
        constexpr auto
        operator()(C (&s)[N]) const
        {
            return static_cast<std::size_t>(detail::arr_length(s));
        }

        template<character_range S>
            requires rng::sized_range<S>
        constexpr auto
        operator()(S&& s) const
        {
            return rng::size(s);
        }

        template<character_range S>
            requires (!rng::sized_range<S>)
                  && has_member_size<S>
        constexpr auto
        operator()(S&& s) const
        {
            return s.size();
        }

    };

} // namespace size_

constexpr size_::api size;

} // namespace stream9::strings

#endif // STRINGS_QUERY_SIZE_HPP
