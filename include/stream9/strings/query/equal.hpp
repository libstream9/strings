#ifndef STRINGS_QUERY_EQUAL_HPP
#define STRINGS_QUERY_EQUAL_HPP

#include "../namespace.hpp"

#include "../core/char/comparator.hpp"
#include "../core/concepts.hpp"
#include "../core/string_range.hpp"
#include "../core/string_traits.hpp"

#include <algorithm>
#include <concepts>
#include <locale>
#include <type_traits>

namespace stream9::strings {

namespace _equal {

    template<typename S1, typename S2>
    inline constexpr bool has_operator = requires (S1 s1, S2 s2) {
        { s1 == s2 } -> std::convertible_to<bool>;
    };

    template<typename S>
    inline constexpr bool is_address = std::is_pointer_v<_uncvref<S>>
                                    || std::is_array_v<_uncvref<S>>;

    template<typename S1, typename S2>
    inline constexpr bool both_address = is_address<S1> && is_address<S2>;

    template<typename S1, typename S2, typename ComparatorT>
    bool
    equal(S1&& lhs, S2&& rhs, ComparatorT fn)
    {
        return rng::equal(string_range(lhs), string_range(rhs), fn);
    }

    template<typename S1, typename S2>
    bool
    equal(S1&& lhs, S2&& rhs)
    {
        if constexpr (has_operator<S1, S2> && !both_address<S1, S2>) {
            return lhs == rhs;
        }
        else {
            return _equal::equal(lhs, rhs, str::equal_to<str_char_t<S1>>());
        }
    }

    struct api
    {
        template<readable_string S1, readable_string S2>
            requires is_same_char_v<S1, S2>
        bool
        operator()(S1&& lhs, S2&& rhs) const
        {
            return _equal::equal(lhs, rhs);
        }

        template<readable_string S1, readable_string S2, typename ComparatorT>
            requires is_same_char_v<S1, S2>
                  && char_comparator<ComparatorT, str_char_t<S1>>
        bool
        operator()(S1&& lhs, S2&& rhs, ComparatorT comp) const
        {
            return _equal::equal(lhs, rhs, comp);
        }
    };

} // namespace _equal

inline constexpr _equal::api equal;

namespace _iequal {

    struct api
    {
        template<readable_string S1, readable_string S2>
            requires is_same_char_v<S1, S2>
        bool
        operator()(S1&& lhs, S2&& rhs, std::locale const& loc = {}) const
        {
            return str::equal(lhs, rhs, str::iequal_to<str_char_t<S1>>(loc));
        }
    };

} // namespace _iequal

inline constexpr _iequal::api iequal;

} // namespace stream9::strings

#endif // STRINGS_QUERY_EQUAL_HPP
