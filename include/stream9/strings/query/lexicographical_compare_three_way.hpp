#ifndef STREAM9_STRINGS_QUERY_LEXICOGRAPHICAL_COMPARE_THREE_WAY_HPP
#define STREAM9_STRINGS_QUERY_LEXICOGRAPHICAL_COMPARE_THREE_WAY_HPP

#include "../namespace.hpp"

#include "../accessor/begin.hpp"
#include "../accessor/end.hpp"
#include "../core/char/comparator.hpp"
#include "../core/concepts.hpp"
#include "../core/string_traits.hpp"

#include <functional>
#include <locale>

#include <stream9/ranges.hpp>

namespace stream9::strings {

namespace lexicographical_compare_three_way_ {

    struct api
    {
        template<readable_string S1, readable_string S2,
                 class Proj1 = std::identity,
                 class Proj2 = std::identity,
                 class Comp = str::compare_three_way<str_char_t<S1>> >
            requires is_same_char_v<S1, S2>
        auto
        operator()(S1&& s1, S2&& s2,
                   Comp comp = {},
                   Proj1 proj1 = {}, Proj2 proj2 = {}) const noexcept
        {
            return rng::lexicographical_compare_three_way(
                str::begin(s1), str::end(s1),
                str::begin(s2), str::end(s2),
                std::ref(comp), std::ref(proj1), std::ref(proj2) );
        }
    };

} // namespace lexicographical_compare_three_way_

inline constexpr lexicographical_compare_three_way_::api lexicographical_compare_three_way;

namespace ilexicographical_compare_three_way_ {

    struct api
    {
        template<readable_string S1, readable_string S2,
                 class Proj1 = std::identity,
                 class Proj2 = std::identity,
                 class Comp = str::icompare_three_way<str_char_t<S1>> >
            requires is_same_char_v<S1, S2>
        auto
        operator()(S1&& s1, S2&& s2,
                   std::locale const& loc = std::locale(),
                   Proj1 proj1 = {}, Proj2 proj2 = {}) const noexcept
        {
            auto comp = str::icompare_three_way<str_char_t<S1>>(loc);

            return rng::lexicographical_compare_three_way(
                str::begin(s1), str::end(s1),
                str::begin(s2), str::end(s2),
                std::ref(comp), std::ref(proj1), std::ref(proj2) );
        }
    };

} // namespace ilexicographical_compare_three_way_

inline constexpr ilexicographical_compare_three_way_::api ilexicographical_compare_three_way;

} // namespace stream9::strings

#endif // STREAM9_STRINGS_QUERY_LEXICOGRAPHICAL_COMPARE_THREE_WAY_HPP
