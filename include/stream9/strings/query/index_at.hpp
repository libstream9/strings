#ifndef STRINGS_QUERY_INDEX_AT_HPP
#define STRINGS_QUERY_INDEX_AT_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"
#include "../core/string_traits.hpp"

#include "../accessor/begin.hpp"

namespace stream9::strings {

namespace _index_at {

    struct api
    {
        template<random_access_string S>
        str_index_t<S>
        operator()(S&& s, iterator_t<S> const it) const
        {
            return it - str::begin(s);
        }
    };

} // namespace _index_at

inline constexpr _index_at::api index_at;

} // namespace stream9::strings

#endif // STRINGS_QUERY_INDEX_AT_HPP
