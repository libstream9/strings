#ifndef STRINGS_STARTS_WITH_HPP
#define STRINGS_STARTS_WITH_HPP

#include "../namespace.hpp"

#include "../core/char/comparator.hpp"
#include "../core/concepts.hpp"
#include "../core/string_range.hpp"
#include "../core/string_traits.hpp"

#include "../accessor/front.hpp"

#include "empty.hpp"

#include <concepts>
#include <locale>

namespace stream9::strings {

namespace _starts_with {

    /*
     * member checker
     */
    template<typename S1, typename T>
    inline constexpr bool has_member =
        string<S1> &&
        requires (S1 s1, T s2) {
            { s1.starts_with(s2) } -> std::convertible_to<bool>;
        };

    /*
     * with character
     */
    template<typename S, typename C, typename ComparatorT>
    bool
    starts_with_char(S&& s, C const c, ComparatorT fn)
    {
        if (str::empty(s)) return false;

        return fn(str::front(s), c);
    }

    template<typename S, typename C>
    bool
    starts_with_char(S&& s1, C const c)
    {
        if constexpr (has_member<S, C>) {
            return s1.starts_with(c);
        }
        else {
            return starts_with_char(s1, c, str::equal_to<C>());
        }
    }

    /*
     * with substring
     */
    template<typename S1, typename S2, typename ComparatorT>
        requires is_same_char_v<S1, S2>
    bool
    starts_with_str(S1&& s1, S2&& s2, ComparatorT fn)
    {
        auto [it1, last1] = string_range { s1 };
        auto [it2, last2] = string_range { s2 };

        for (; it1 != last1 && it2 != last2; ++it1, ++it2) {
            if (!fn(*it1, *it2)) return false;
        }

        return it2 == last2;
    }

    template<typename S1, typename S2>
    bool
    starts_with_str(S1&& s1, S2&& s2)
    {
        if constexpr (has_member<S1, S2>) {
            return s1.starts_with(s2);
        }
        else {
            return starts_with_str(s1, s2, str::equal_to<str_char_t<S1>>());
        }
    }

    struct api
    {
        template<readable_string S, character C>
            requires is_same_char_v<S, C>
        bool
        operator()(S&& s, C const c) const
        {
            return starts_with_char(s, c);
        }

        template<readable_string S, character C, typename ComparatorT>
            requires is_same_char_v<S, C>
                  && char_comparator<ComparatorT, C>
        bool
        operator()(S&& s, C const c, ComparatorT comp) const
        {
            return starts_with_char(s, c, comp);
        }

        template<forward_string S1, forward_string S2>
            requires is_same_char_v<S1, S2>
        bool
        operator()(S1&& s1, S2&& s2) const
        {
            return starts_with_str(s1, s2);
        }

        template<forward_string S1, forward_string S2, typename ComparatorT>
            requires is_same_char_v<S1, S2>
                  && char_comparator<ComparatorT, str_char_t<S1>>
        bool
        operator()(S1&& s1, S2&& s2, ComparatorT comp) const
        {
            return starts_with_str(s1, s2, comp);
        }
    };

} // namespace _starts_with

inline constexpr _starts_with::api starts_with;

namespace _istarts_with {

    struct api
    {
        template<forward_string S, character C>
            requires is_same_char_v<S, C>
        bool
        operator()(S&& s, C const c, std::locale const& loc = {}) const
        {
            auto comp = iequal_to<C>(loc);
            return str::starts_with(s, c, comp);
        }

        template<forward_string S1, forward_string S2>
            requires is_same_char_v<S1, S2>
        bool
        operator()(S1&& s1, S2&& s2, std::locale const& loc = {}) const
        {
            auto comp = iequal_to<str_char_t<S1>>(loc);
            return str::starts_with(s1, s2, comp);
        }
    };

} // namespace _istarts_with

inline constexpr _istarts_with::api istarts_with;

} // namespace stream9::strings

#endif // STRINGS_STARTS_WITH_HPP
