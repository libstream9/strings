#ifndef STRINGS_QUERY_FIND_ALL_HPP
#define STRINGS_QUERY_FIND_ALL_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"
#include "../core/finder.hpp"
#include "../core/regex.hpp"
#include "../core/string_range.hpp"
#include "../core/view_interface.hpp"

#include "../finder/first_finder.hpp"
#include "../finder/regex_finder.hpp"

#include "../detail/view_base.hpp"

#include <functional>
#include <iterator>
#include <locale>
#include <optional>
#include <ranges>
#include <type_traits>

#include <stream9/iterators.hpp>

namespace stream9::strings {

namespace _find_all {

    template<forward_string S, finder<S> FinderT>
    class view : public view_interface<view<S, FinderT>>
               , public rng::view_interface<view<S, FinderT>>
               , public view_base
    {
    public:
        class iterator;
        using sentinel = std::default_sentinel_t;

    public:
        view() = default;

        template<forward_string S1>
        view(S1&& s, FinderT finder)
            : m_str { s }
            , m_finder { std::move(finder) }
        {}

        iterator begin() const { return { *this }; }

        sentinel end() const { return {}; }

    private:
        string_range<iterator_t<S>> m_str;
        std::optional<FinderT> m_finder;
    };

    template<forward_string S, typename FinderT>
    class view<S, FinderT>::iterator
        : public iter::iterator_facade<iterator,
                                 std::forward_iterator_tag,
                                 string_range<iterator_t<S>> const&>
    {
    public:
        iterator() = default;

        iterator(view const& parent)
            : m_parent { &parent }
            , m_string { m_parent->m_str }
        {
            increment();
        }

        //TODO iter_move, iter_swap

    private:
        friend class iter::iterator_core_access;

        auto const&
        dereference() const
        {
            return m_current;
        }

        void
        increment()
        {
            m_current = (*m_parent->m_finder)(m_string);

            if constexpr (forward_finder<FinderT>) {
                m_string = string_range { m_current.end(), m_string.end() };
            }
            else {
                m_string = string_range { m_string.begin(), m_current.begin() };
            }
        }

        bool
        equal(iterator const& other) const
        {
            return m_current == other.m_current
                && m_string == other.m_string;
        }

        bool
        equal(sentinel const&) const
        {
            return !m_current;
        }

    private:
        view const* m_parent = nullptr;
        string_range<iterator_t<S>> m_string;
        string_range<iterator_t<S>> m_current;
    };

    struct api
    {
        template<forward_string S, character C>
            requires is_same_char_v<S, C>
        auto
        operator()(S&& s, C const c) const
        {
            str::first_finder finder { c };

            return operator()(s, std::move(finder));
        }

        template<forward_string S, forward_string KeywordT>
            requires is_same_char_v<S, KeywordT>
        auto
        operator()(S&& s1, KeywordT&& s2) const
        {
            str::first_finder finder { s2 };

            return operator()(s1, std::move(finder));
        }

        template<forward_string S, classifier<str_char_t<S>> C>
        auto
        operator()(S&& s, C&& c) const
        {
            str::first_finder<str_char_t<S>, _unref<C>> finder { std::forward<C>(c) };

            return operator()(s, std::move(finder));
        }

        template<bidirectional_string S, regexp R>
            requires is_same_char_v<S, R>
        auto
        operator()(S&& s, R&& e) const
        {
            if constexpr (std::is_trivially_copyable_v<_unref<R>>) {
                str::regex_finder fn { e };
                return operator()(s, std::move(fn));
            }
            else {
                str::regex_finder fn { std::cref(e) };
                return operator()(s, std::move(fn));
            }
        }

        template<forward_string S, finder<S> FinderT>
        auto
        operator()(S&& s, FinderT&& finder) const
        {
            return view<_unref<S>, _unref<FinderT>> { s, std::move(finder) };
        }
    };

} // namespace _find_all

inline constexpr _find_all::api find_all;

namespace _ifind_all {

    struct api
    {
        template<forward_string S, character C>
        auto
        operator()(S&& s, C const c, std::locale const& loc = {}) const
        {
            str::first_finder fn { c, str::iequal_to<C>(loc) };

            return find_all(s, std::move(fn));
        }

        template<forward_string S, forward_string KeywordT>
        auto
        operator()(S&& s1, KeywordT&& s2, std::locale const& loc = {}) const
        {
            str::first_finder fn { s2, str::iequal_to<str_char_t<KeywordT>>(loc) };

            return find_all(s1, std::move(fn));
        }
    };

} // namespace _ifind_all

inline constexpr _ifind_all::api ifind_all;

} // namespace stream9::strings

#endif // STRINGS_QUERY_FIND_ALL_HPP
