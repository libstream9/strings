#ifndef STRINGS_QUERY_LEXICOGRAPHICAL_COMPARE_HPP
#define STRINGS_QUERY_LEXICOGRAPHICAL_COMPARE_HPP

#include "../namespace.hpp"

#include "../core/char/comparator.hpp"
#include "../core/concepts.hpp"
#include "../core/string_range.hpp"
#include "../core/string_traits.hpp"

#include <algorithm>
#include <locale>

namespace stream9::strings {

namespace _lexicographical_compare {

    struct api
    {
        template<readable_string S1, readable_string S2, typename ComparatorT>
            requires is_same_char_v<S1, S2>
                  && char_comparator<ComparatorT, str_char_t<S1>>
        bool
        operator()(S1&& s1, S2&& s2, ComparatorT comp) const
        {
            return rng::lexicographical_compare(
                string_range(s1), string_range(s2), comp
            );
        }

        template<readable_string S1, readable_string S2>
            requires is_same_char_v<S1, S2>
        bool
        operator()(S1&& s1, S2&& s2) const
        {
            return operator()(s1, s2, str::less<str_char_t<S1>>());
        }
    };

} // namespace _lexicographical_compare

inline constexpr _lexicographical_compare::api lexicographical_compare;

namespace _ilexicographical_compare {

    struct api
    {
        template<readable_string S1, readable_string S2>
            requires is_same_char_v<S1, S2>
        bool
        operator()(S1&& s1, S2&& s2, std::locale const& loc = {}) const
        {
            return str::lexicographical_compare(s1, s2, str::iless<str_char_t<S1>>(loc));
        }
    };

} // namespace _ilexicographical_compare

inline constexpr _ilexicographical_compare::api ilexicographical_compare;

} // namespace stream9::strings

#endif // STRINGS_QUERY_LEXICOGRAPHICAL_COMPARE_HPP
