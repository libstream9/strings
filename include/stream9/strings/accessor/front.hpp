#ifndef STRINGS_ACCESSOR_FRONT_HPP
#define STRINGS_ACCESSOR_FRONT_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"
#include "../core/string_traits.hpp"

#include "begin.hpp"

namespace stream9::strings {

namespace _front {

    struct api
    {
        template<readable_string S>
        str_char_t<S>
        operator()(S&& s) const
        // [[expects: !str::empty(s)]]
        {
            return *str::begin(s);
        }
    };

} // namespace _front

inline constexpr _front::api front;

} // namespace stream9::strings

#endif // STRINGS_ACCESSOR_FRONT_HPP
