#ifndef STRINGS_ACCESSOR_AT_HPP
#define STRINGS_ACCESSOR_AT_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"
#include "../core/string_traits.hpp"

#include "begin.hpp"

#include <concepts>

namespace stream9::strings {

namespace _at {

    template<typename S>
    str_char_t<S>
    at(S&& s, str_index_t<S> n)
    {
        return *(str::begin(s) + to_difference<S>(n));
    }

    struct api
    {
        template<random_access_string S>
        str_char_t<S>
        operator()(S&& s, str_index_t<S> n) const
        // [[expects: !str::empty(s)]]
        // [[expects: n <= std::numeric_limits<str_difference_t<S>>::max()]]
        {
            return at(s, n);
        }
    };

} // namespace _at

inline constexpr _at::api at;

} // namespace stream9::strings

#endif // STRINGS_ACCESSOR_AT_HPP
