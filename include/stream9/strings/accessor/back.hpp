#ifndef STRINGS_ACCESSOR_BACK_HPP
#define STRINGS_ACCESSOR_BACK_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"
#include "../core/string_traits.hpp"

#include "end.hpp"

namespace stream9::strings {

namespace _back {

    struct api
    {
        template<bidirectional_string S>
        str_char_t<S>
        operator()(S&& s) const
        // [[expects: !str::empty(s)]]
        {
            if constexpr (random_access_string<S>) {
                return *(str::end(s) - 1);
            }
            else {
                auto it = str::end(s);
                --it;
                return *it;
            }
        }
    };

} // namespace _back

inline constexpr _back::api back;

} // namespace stream9::strings

#endif // STRINGS_ACCESSOR_BACK_HPP
