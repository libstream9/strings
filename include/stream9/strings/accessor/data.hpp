#ifndef STRINGS_ACCESSOR_DATA_HPP
#define STRINGS_ACCESSOR_DATA_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"

#include <ranges>

namespace stream9::strings {

namespace data_ {

    struct api {
        template<character C>
        constexpr C*
        operator()(C* const& s) const
        {
            return s;
        }

        template<character C, std::size_t N>
        constexpr C*
        operator()(C (&s)[N]) const
        {
            return s;
        }

        template<character_range S>
            requires random_access_string<S>
        constexpr auto
        operator()(S&& s) const
        {
            return rng::data(s);
        }
    };

} // namespace data_

constexpr data_::api data;

namespace cdata_ {

    struct api {
        template<character C>
        constexpr C const*
        operator()(C const* const& s) const
        {
            return s;
        }

        template<character C, std::size_t N>
        constexpr C const*
        operator()(C const (&s)[N]) const
        {
            return s;
        }

        template<character_range S>
            requires random_access_string<S>
        constexpr auto
        operator()(S&& s) const
        {
            return rng::cdata(s);
        }
    };

} // namespace cdata_

constexpr cdata_::api cdata;

} // namespace stream9::strings

#endif // STRINGS_ACCESSOR_DATA_HPP
