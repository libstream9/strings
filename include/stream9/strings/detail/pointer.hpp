#ifndef STRINGS_DETAIL_POINTER_HPP
#define STRINGS_DETAIL_POINTER_HPP

#include "../core/char/concepts.hpp"

#include <string>

namespace stream9::strings {

namespace detail {

    template<character C>
    auto
    ptr_length(C* const& s)
    {
        return std::char_traits<C>::length(s);
    }

    template<character C>
    C*
    ptr_end(C* const& s)
    {
        return s + ptr_length(s);
    }

    template<character C, std::size_t N>
    constexpr C*
    arr_end(C (&s)[N])
    {
        static_assert(N > 0);

        auto last = s + N;
        for (; last != s && *(last - 1) == null_char_v<C>; --last);

        return last;
    }

    template<character C, std::size_t N>
    constexpr auto
    arr_length(C (&s)[N])
    {
        return arr_end(s) - s;
    }

} // namespace detail

} // namespace stream9::strings

#endif // STRINGS_DETAIL_POINTER_HPP
