#ifndef STRINGS_DETAIL_CTYPE_FACET_HPP
#define STRINGS_DETAIL_CTYPE_FACET_HPP

#include "../core/string_traits.hpp"

#include <locale>

namespace stream9::strings {

namespace detail {

    template<typename StringT>
    inline auto&
    ctype_facet(std::locale const& loc)
    {
        using char_type = str_char_t<StringT>;
        using facet_t = std::ctype<char_type>;

        return std::use_facet<facet_t>(loc);
    }

} // namespace detail

} // namespace stream9::strings

#endif // STRINGS_DETAIL_CTYPE_FACET_HPP
