#ifndef STRINGS_STREAMBUF_HPP
#define STRINGS_STREAMBUF_HPP

#include "namespace.hpp"

#include "core/concepts.hpp"
#include "core/string_traits.hpp"
#include "core/string_range.hpp"

#include "accessor/begin.hpp"
#include "accessor/data.hpp"
#include "accessor/end.hpp"
#include "modifier/insert.hpp"
#include "query/size.hpp"

#include <streambuf>

namespace stream9::strings {

namespace _streambuf {

    template<string S>
    class base_t : public std::basic_streambuf<str_char_t<S>>
    {
    protected:
        int
        sync() override
        {
            this->sync_put_area();
            this->sync_get_area();

            return 0;
        }

        virtual S* str() = 0;
        virtual void sync_put_area() = 0;
        virtual void sync_get_area() = 0;
    };

    /*
     * get_area
     */
    template<typename S> class get_area;

    //TODO string

    template<readable_string S>
        requires (!contiguous_string<S>)
    class get_area<S> : virtual public base_t<S>
    {
        using typename base_t<S>::char_type;
        using typename base_t<S>::pos_type;
        using typename base_t<S>::off_type;
        using typename base_t<S>::traits_type;
        using typename base_t<S>::int_type;

    protected:
        int_type
        underflow() override // std::basic_streambuf
        {
            if (this->gptr() == nullptr) {
                auto* s = this->str();
                if (!s) return base_t<S>::uderflow();

                m_current = str::begin(*s);
                m_end = str::end(*s);

                m_buf = *m_current++;
                this->setg(&m_buf, &m_buf, &m_buf + 1);
                return m_buf;

            }
            else {
                return traits_type::eof();
            }
        }

        void
        sync_get_area() override {} // base_t

    private:
        iterator_t<S> m_current;
        iterator_t<S> m_end;
        str_char_t<S> m_buf;
    };

    //TODO bidirectional_string
    //TODO random_access_string

    template<contiguous_string S>
    class get_area<S> : virtual public base_t<S>
    {
        using typename base_t<S>::char_type;
        using typename base_t<S>::pos_type;
        using typename base_t<S>::off_type;
        using typename base_t<S>::traits_type;
        using typename base_t<S>::int_type;

    protected:
        int_type
        underflow() override // std::basic_streambuf
        {
            if (this->gptr() == nullptr) {
                auto* s = this->str();
                if (!s) return base_t<S>::underflow();

                auto* const first = const_cast<str_char_t<S>*>(str::data(*s));
                auto* const last = first + str::size(*s);

                this->setg(first, first, last);

                return *first;
            }
            else {
                return traits_type::eof();
            }
        }

        pos_type
        seekoff(off_type const off, std::ios_base::seekdir const dir,
                std::ios_base::openmode const which
                    = std::ios_base::in | std::ios_base::out) override // std::basic_streambuf
        {
            if (which & std::ios_base::out) {
                return error_pos();
            }

            char_type* ptr = nullptr;

            switch (dir) {
                case std::ios_base::beg:
                    ptr = this->eback() + off;
                    break;
                case std::ios_base::end:
                    ptr = this->egptr() + off;
                    break;
                case std::ios_base::cur:
                    ptr = this->gptr() + off;
                    break;
                default:
                    return error_pos();
            }

            if (this->eback() <= ptr && ptr <= this->egptr()) {
                this->setg(this->eback(), ptr, this->egptr());
                return static_cast<pos_type>(ptr - this->eback());
            }
            else {
                return error_pos();
            }
        }

        pos_type
        seekpos(pos_type const pos,
                std::ios_base::openmode const which
                    = std::ios_base::in | std::ios_base::out) override // std::basic_streambuf
        {
            return seekoff(pos, std::ios_base::beg, which);
        }

        int_type
        pbackfail(int_type const c = traits_type::eof()) override // std::basic_streambuf
        // [[expects: this->gptr() > this->eback()]]
        {
            if (c != traits_type::eof()) {
                if constexpr (std::is_const_v<S>) {
                    return traits_type::eof();
                }
                else {
                    this->setg(this->eback(), this->gptr() - 1, this->egptr());
                    *this->gptr() = static_cast<str_char_t<S>>(c);
                }
            }
            else {
                this->setg(this->eback(), this->gptr() - 1, this->egptr());
            }

            return 1; // success
        }

        void
        sync_get_area() override // base_t
        {
            auto const pos = this->gptr() - this->eback();
            auto* s = this->str();
            if (!s) return;

            auto const first = const_cast<str_char_t<S>*>(str::data(*s));
            auto const last = first + str::size(*s);
            this->setg(first, first + pos, last);
        }

    private:
        pos_type
        error_pos() const
        {
            return static_cast<pos_type>(static_cast<off_type>(-1));
        }
    };

    /*
     * put_area
     */
    template<typename S1, typename S2>
    concept can_insert = requires (S1&& s1, S2&& s2) {
        str::insert(s1, str::end(s1), s2);
    };

    template<string S>
    using put_area_buffer_t = std::array<str_char_t<S>, 10>;

    template<typename S> class put_area;

    template<string S>
        requires (!can_insert<S, string_range<str_char_t<S>*>>)
    class put_area<S> : virtual public base_t<S>
    {
        using typename base_t<S>::char_type;
        using typename base_t<S>::pos_type;
        using typename base_t<S>::off_type;
        using typename base_t<S>::traits_type;
        using typename base_t<S>::int_type;

    protected:
        int_type
        overflow(int_type const ch = traits_type::eof()) override // std::basic_streambuf
        {
            return base_t<S>::overflow(ch);
        }

        void sync_put_area() override {}
    };

    template<string S>
        requires can_insert<S, string_range<str_char_t<S>*>>
    class put_area<S> : virtual public base_t<S>
    {
        using typename base_t<S>::char_type;
        using typename base_t<S>::pos_type;
        using typename base_t<S>::off_type;
        using typename base_t<S>::traits_type;
        using typename base_t<S>::int_type;

    public:
        put_area()
        {
            this->setp(m_buf.begin(), m_buf.end());
        }

    protected:
        int_type
        overflow(int_type ch = traits_type::eof()) override // std::basic_streambuf
        {
            this->sync();

            if (ch != traits_type::eof()) {
                *this->pptr() = static_cast<str_char_t<S>>(ch);
                this->pbump(1);
            }

            return 1; // success
        }

        void
        sync_put_area() override // base_t
        {
            auto* s = this->str();
            if (!s) return;

            str::insert(*s, str::end(*s),
                        string_range(this->pbase(), this->pptr()));

            this->setp(this->pbase(), this->epptr());
        }

    private:
        put_area_buffer_t<S> m_buf;
    };

} // namespace _streambuf

template<readable_string S>
class streambuf : public _streambuf::get_area<S>
                , public _streambuf::put_area<S>
{
public:
    streambuf() = default;

    streambuf(S& s)
        : m_str { &s } {}

protected:
    S*
    str() override // base_t
    {
        return m_str;
    }

private:
    S* m_str = nullptr;
};

template<readable_string S>
streambuf(S&&) -> streambuf<_unref<S>>;

} // namespace stream9::strings

#endif // STRINGS_STREAMBUF_HPP
