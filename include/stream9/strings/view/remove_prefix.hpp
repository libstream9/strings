#ifndef STRINGS_VIEW_REMOVE_PREFIX_HPP
#define STRINGS_VIEW_REMOVE_PREFIX_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"
#include "../core/string_range.hpp"
#include "../core/string_traits.hpp"

#include "../accessor/begin.hpp"
#include "../accessor/end.hpp"

#include <concepts>
#include <iterator>

namespace stream9::strings::views {

namespace _remove_prefix {

    struct api
    {
        template<forward_string S>
        auto
        operator()(S&& s, str_size_t<S> n) const
        {
            auto const first = rng::next(str::begin(s), to_difference<S>(n));
            auto const last = str::end(s);

            return string_range { first, last };
        }

        template<forward_string S, std::forward_iterator I>
            requires std::convertible_to<I, iterator_t<_unref<S> const>>
        auto
        operator()(S&& s, I it) const
        {
            return string_range { it, str::end(s) };
        }
    };

} // namespace _remove_prefix

inline constexpr _remove_prefix::api remove_prefix;

} // namespace stream9::strings::views

#endif // STRINGS_VIEW_REMOVE_PREFIX_HPP
