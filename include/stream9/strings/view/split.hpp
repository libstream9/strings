#ifndef STRINGS_VIEW_SPLIT_HPP
#define STRINGS_VIEW_SPLIT_HPP

#include "../namespace.hpp"

#include "../core/char/classifier.hpp"
#include "../core/concepts.hpp"
#include "../core/finder.hpp"
#include "../core/regex.hpp"
#include "../core/string_range.hpp"

#include "../finder/first_finder.hpp"
#include "../finder/regex_finder.hpp"

#include "../detail/view_base.hpp"

#include <iterator>
#include <optional>
#include <ranges>
#include <utility>

#include <stream9/iterators.hpp>

namespace stream9::strings::views {

namespace _split {

    enum class split_option {
        none = 0,
        skip_empty_item = 1,
    };

    template<typename S, finder<S> FinderT>
    class view : public rng::view_interface<view<S, FinderT>>
               , public str::view_base
    {
    public:
        class iterator;
        using sentinel = std::default_sentinel_t;

    public:
        view() = default;

        template<typename S1>
        view(S1&& s, FinderT fn, split_option o)
            : m_str { s }
            , m_finder { std::move(fn) }
            , m_option { o }
        {}


        iterator begin() const
        {
            return { *this };
        }

        sentinel end() const { return {}; }

    private:
        string_range<iterator_t<S>> m_str;
        std::optional<FinderT> m_finder;
        split_option m_option;
    };

    template<typename S, typename Fn>
    view(S&&, Fn, split_option) -> view<_unref<S>, Fn>;

    template<typename S, finder<S> FinderT>
    class view<S, FinderT>::iterator
        : public iter::iterator_facade<iterator,
                                 std::forward_iterator_tag,
                                 string_range<iterator_t<S>> const& >
    {
    public:
        iterator() = default;

        iterator(view const& parent)
            : m_parent { &parent }
            , m_str { m_parent->m_str }
        {
            increment();
        }

    private:
        friend class iter::iterator_core_access;

        auto const& dereference() const { return m_current; }

        void
        increment()
        {
            auto const& find = *m_parent->m_finder;

            auto match = find(m_str);

            if (match.empty()) {
                m_current = m_str;
                m_str = { m_str.end(), m_str.end() };
            }
            else {
                if constexpr (forward_finder<FinderT>) {
                    string_range remainder { match.end(), m_str.end() };

                    if (m_parent->m_option == split_option::skip_empty_item) {
                        consume_forward_consective_match(find, match, remainder);
                    }

                    m_current = string_range { m_str.begin(), match.begin() };
                    m_str = remainder;
                }
                else {
                    string_range remainder { m_str.begin(), match.begin() };

                    if (m_parent->m_option == split_option::skip_empty_item) {
                        consume_backward_consective_match(find, match, remainder);
                    }

                    m_current = string_range { match.end(), m_str.end() };
                    m_str = remainder;
                }
            }
        }

        bool
        equal(iterator const& other) const
        {
            return m_str == other.m_str;
        }

        bool
        equal(sentinel const&) const
        {
            return !m_current && !m_str;
        }

        template<typename Fn, typename T>
        void
        consume_forward_consective_match(Fn& find, T& match, T& remainder)
        {
            auto const n = match.size();

            while (true) {
                auto const& [rem_first, rem_last] = remainder;
                auto const n_ = static_cast<
                            std::iter_difference_t<decltype(rem_first)> >(n);

                auto const it = rng::next(rem_first, n_, rem_last);

                string_range const prefix { rem_first, it };
                auto const& m = find(prefix);

                if (!m) {
                    break;
                }
                else {
                    match = string_range { match.begin(), it };
                    remainder = string_range { it, rem_last };
                }
            }
        }

        template<typename Fn, typename T>
        void
        consume_backward_consective_match(Fn& find, T& match, T& remainder)
        {
            auto const n = match.size();

            while (true) {
                auto const& [rem_first, rem_last] = remainder;
                auto const n_ = static_cast<
                            std::iter_difference_t<decltype(rem_first)> >(n);

                auto const it = rng::prev(rem_last, n_, rem_first);

                string_range const suffix { it, rem_last };
                auto const& m = find(suffix);

                if (!m) {
                    break;
                }
                else {
                    match = string_range { it, match.end() };
                    remainder = string_range { rem_first, it };
                }
            }
        }

    private:
        view const* m_parent = nullptr;
        string_range<iterator_t<S>> m_str;
        string_range<iterator_t<S>> m_current;
    };

    struct api
    {
        /*
         * with whitespace(s)
         */
        template<forward_string S>
        auto
        operator()(S&& s, split_option const o = split_option::skip_empty_item) const
        {
            is_space<str_char_t<S>> pred;
            return view { s, first_finder<str_char_t<S>, is_space<str_char_t<S>>>(pred), o };
        }

        /*
         * with delimiter char
         */
        template<forward_string S>
        auto
        operator()(S&& s, str_char_t<S> const c,
                            split_option const o = split_option::none) const
        {
            return view { s, first_finder(c), o };
        }

        /*
         * with pattern string
         */
        template<forward_string S, forward_string PatternT>
        auto
        operator()(S&& s1, PatternT&& s2,
                            split_option const o = split_option::none) const
        {
            return view { s1, first_finder(s2), o };
        }

        /*
         * with regex
         */
        template<bidirectional_string S, regexp R>
        auto
        operator()(S&& s, R&& e,
                            split_option const o = split_option::none) const
        {
            return view { s, regex_finder(e), o };
        }

        /*
         * with finder
         */
        template<forward_string S, finder<S> FinderT>
        auto
        operator()(S&& s, FinderT&& fn,
                            split_option const o = split_option::none) const
        {
            return view { s, std::forward<FinderT>(fn), o };
        }
    };

} // namespace _split

using _split::split_option;

inline constexpr _split::api split;

} // namespace stream9::strings::views

#endif // STRINGS_VIEW_SPLIT_HPP
