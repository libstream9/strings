#ifndef STRINGS_VIEW_FIND_REPLACE_ALL_HPP
#define STRINGS_VIEW_FIND_REPLACE_ALL_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"
#include "../core/finder.hpp"
#include "../core/string_range.hpp"
#include "../core/string_traits.hpp"
#include "../core/view_interface.hpp"

#include "../finder/first_finder.hpp"
#include "../finder/regex_finder.hpp"

#include "../detail/view_base.hpp"

#include <iterator>
#include <optional>
#include <ranges>
#include <utility>

#include <stream9/iterators.hpp>

namespace stream9::strings::views {

namespace _find_replace_all {

    template<typename S1, typename FinderT, typename S2>
    class view : public view_interface<view<S1, FinderT, S2>>
               , public rng::view_interface<view<S1, FinderT, S2>>
               , public view_base
    {
    public:
        class iterator;
        using sentinel = std::default_sentinel_t;

    public:
        view() = default;

        template<typename T1, typename T2>
        view(T1&& src, FinderT finder, T2&& replacement)
            : m_src { src }
            , m_finder { std::move(finder) }
            , m_replacement { replacement }
        {}

        iterator begin() const { return { *this }; }
        sentinel end() const { return {}; }

    private:
        string_range<iterator_t<S1>> m_src;
        std::optional<FinderT> m_finder;
        string_range<iterator_t<S2>> m_replacement;

        friend class iterator;
    };

    template<typename S1, typename FinderT, typename S2>
    view(S1&&, FinderT&&, S2&&) -> view<_unref<S1>, _unref<FinderT>, _unref<S2>>;

    template<typename S1, typename FinderT, typename S2>
    class view<S1, FinderT, S2>::iterator
        : public iter::iterator_facade<iterator,
                                 std::forward_iterator_tag,
                                 str_char_t<S1> const& >
    {
        enum state { copy, replace, end };
    public:
        iterator() = default;

        iterator(view const& v)
            : m_view { &v }
            , m_src { v.m_src }
        {
            init();
        }

    private:
        friend class iter::iterator_core_access;

        void
        init()
        // [[assert: m_view]]
        {
            if (m_src.empty()) {
                m_state = end;
            }
            else {
                m_skip = (*m_view->m_finder)(m_src);

                if (m_skip.empty()) {
                    m_state = copy;
                }
                else {
                    if (m_skip.begin() == m_src.begin()) {
                        m_src = m_src.next(
                            to_difference<decltype(m_skip)>(m_skip.size()));

                        m_replacement = m_view->m_replacement;

                        if (m_replacement.empty()) {
                            m_state = copy;
                        }
                        else {
                            m_state = replace;
                        }
                    }
                    else {
                        m_state = copy;
                    }
                }
            }
        }

        auto const&
        dereference() const
        {
            switch (m_state) {
                case copy:
                    return *m_src.begin();
                case replace:
                    return *m_replacement.begin();
                case end:
                default:
                    return *m_src.begin(); // undefined behavior
            }
        }

        void
        increment()
        {
            auto& find = *m_view->m_finder;

            switch (m_state) {
                case copy:
                    m_src = m_src.next();

                    if (m_src.empty()) {
                        m_state = end;
                    }
                    else if (m_skip && m_src.begin() == m_skip.begin()) {
                        m_src = m_src.next(
                            to_difference<decltype(m_skip)>(m_skip.size()));

                        m_skip = find(m_src);
                        m_replacement = m_view->m_replacement;

                        if (m_src.empty()) {
                            m_state = end;
                        }
                        else if (m_replacement.empty()) {
                            while (m_skip && m_skip.begin() == m_src.begin()) {
                                m_src = m_src.next(
                                    to_difference<decltype(m_skip)>(m_skip.size()));
                                m_skip = find(m_src);
                            }
                        }
                        else {
                            m_state = replace;
                        }
                    }
                    break;
                case replace:
                    m_replacement = m_replacement.next();

                    if (m_replacement.empty()) {
                        if (m_src.empty()) {
                            m_state = end;
                        }
                        else {
                            m_state = copy;
                        }
                    }
                    break;
                case end:
                    break; //Undefined Behavior
            }
        }

        bool
        equal(iterator const& other) const
        {
            return m_state == other.m_state
                && m_src == other.m_src
                && m_skip == other.m_skip
                && m_replacement == other.m_replacement;
        }

        bool
        equal(sentinel const&) const
        {
            return m_state == end;
        }

    private:
        view const* m_view; // non-null
        string_range<iterator_t<S1>> m_src;
        string_range<iterator_t<S1>> m_skip;
        string_range<iterator_t<S2>> m_replacement;
        state m_state = end;
    };

    struct api
    {
        template<forward_string S, forward_string KeywordT, forward_string ReplacementT>
            requires is_same_char_v<S, KeywordT, ReplacementT>
        auto
        operator()(S&& s1, KeywordT&& s2, ReplacementT&& s3) const
        {
            return operator()(s1, first_finder { s2 }, s3);
        }

        template<bidirectional_string S, regexp R, forward_string ReplacementT>
            requires is_same_char_v<S, R, ReplacementT>
        auto
        operator()(S&& s1, R&& e, ReplacementT&& s2) const
        {
            return operator()(s1, regex_finder { std::forward<R>(e) }, s2);
        }

        template<forward_string S, typename FinderT, forward_string ReplacementT>
            requires is_same_char_v<S, ReplacementT>
                  && finder<FinderT, S>
        auto
        operator()(S&& s1, FinderT&& find, ReplacementT&& s2) const
        {
            return view { s1, std::forward<FinderT>(find), s2 };
        }
    };

} // namespace _find_replace_all

inline constexpr _find_replace_all::api find_replace_all;

} // namespace stream9::strings::views

#endif // STRINGS_VIEW_FIND_REPLACE_ALL_HPP
