#ifndef STRINGS_VIEW_TRIM_RIGHT_HPP
#define STRINGS_VIEW_TRIM_RIGHT_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"
#include "../core/string_range.hpp"
#include "../core/string_traits.hpp"
#include "../core/char/classifier.hpp"

#include "../accessor/begin.hpp"
#include "../modifier/trim_right.hpp"

#include <concepts>
#include <locale>

namespace stream9::strings::views {

namespace _trim_right {

    struct api
    {
        template<forward_string S>
        string_range<iterator_t<S>>
        operator()(S&& s, std::locale const& loc = {}) const
        {
            return operator()(s, is_space<str_char_t<S>>(loc));
        }

        template<forward_string S, typename PredicateT>
            requires std::predicate<PredicateT, str_char_t<S>>
        string_range<iterator_t<S>>
        operator()(S&& s, PredicateT pred) const
        {
            auto const it = str::_trim_right::find_suffix(s, pred);

            return { str::begin(s), it };
        }
    };

} // namespace _trim_right

inline constexpr _trim_right::api trim_right;

} // namespace stream9::strings::views

#endif // STRINGS_VIEW_TRIM_RIGHT_HPP
