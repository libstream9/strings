#ifndef STREAM9_STRINGS_VIEW_SLICE_HPP
#define STREAM9_STRINGS_VIEW_SLICE_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"
#include "../core/string_range.hpp"

#include "../accessor/begin.hpp"
#include "../accessor/end.hpp"
#include "../query/size.hpp"

namespace stream9::strings::views {

namespace _slice {

    struct api
    {
        template<str::random_access_string S>
        str::string_range<str::iterator_t<S>>
        operator()(S&& s, str_difference_t<S> begin_idx,
                          str_difference_t<S> end_idx) const
        {
            auto const begin = str::begin(s);
            auto const end = str::end(s);

            if (begin == end) return {};

            auto b = begin_idx >= 0 ? begin + begin_idx
                                    : end + begin_idx;
            auto e = end_idx >= 0 ? begin + end_idx
                                  : end + end_idx;

            if (b < begin) b = begin;
            if (e > end) e = end;

            if (b > e) return {};

            return { b, e };
        }

        template<str::random_access_string S>
        str::string_range<str::iterator_t<S>>
        operator()(S&& s, str_difference_t<S> begin_idx) const
        {
            return operator()(s,
                begin_idx,
                static_cast<str_difference_t<S>>(str::size(s)) );
        }
    };

} // namespace _slice

inline constexpr _slice::api slice;

} // namespace stream9::strings::views

#endif // STREAM9_STRINGS_VIEW_SLICE_HPP
