#ifndef STRINGS_VIEW_TRIM_HPP
#define STRINGS_VIEW_TRIM_HPP

#include "../namespace.hpp"

#include "../core/char/classifier.hpp"
#include "../core/concepts.hpp"
#include "../core/string_traits.hpp"

#include "trim_left.hpp"
#include "trim_right.hpp"

#include <concepts>
#include <locale>

namespace stream9::strings::views {

namespace _trim {

    struct api
    {
        template<forward_string S>
        auto
        operator()(S&& s, std::locale const& loc = {}) const
        {
            return operator()(s, is_space<str_char_t<S>>(loc));
        }

        template<forward_string S, typename PredicateT>
            requires std::predicate<PredicateT, str_char_t<S>>
        auto
        operator()(S&& s, PredicateT pred) const
        {
            auto result = str::views::trim_left(s, pred);
            return str::views::trim_right(result, pred);
        }
    };

} // namespace _trim

inline constexpr _trim::api trim;

} // namespace stream9::strings::views

#endif // STRINGS_VIEW_TRIM_HPP
