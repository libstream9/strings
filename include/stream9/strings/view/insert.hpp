#ifndef STRINGS_VIEW_INSERT_HPP
#define STRINGS_VIEW_INSERT_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"
#include "../core/string_range.hpp"
#include "../core/string_traits.hpp"
#include "../core/view_interface.hpp"

#include "../query/iterator_at.hpp"

#include "../detail/view_base.hpp"

#include <concepts>
#include <iterator>
#include <ranges>

#include <stream9/iterators.hpp>

namespace stream9::strings::views {

namespace _insert {

    template<typename S1, typename S2>
    class view : public str::view_interface<view<S1, S2>>
               , public rng::view_interface<view<S1, S2>>
               , public view_base
    {
    public:
        class iterator;
        using sentinel = std::default_sentinel_t;

    public:
        view() = default;

        view(S1 const& s1, iterator_t<S1> const pos, S2 const& s2)
            : m_s1 { s1 }
            , m_s2 { s2 }
            , m_pos { pos }
        {}

        iterator begin() const { return { *this }; }
        sentinel end() const { return {}; }

    private:
        S1 m_s1;
        S2 m_s2;
        iterator_t<S1> m_pos;
    };

    template<typename S1, typename S2>
    class view<S1, S2>::iterator
        : public iter::iterator_facade<iterator,
                                 std::forward_iterator_tag,
                                 str_char_t<S1> const& >
    {
        enum state { s1, s2, end };
    public:
        iterator() = default;

        iterator(view const& parent)
            : m_parent { &parent }
            , m_it1 { parent.m_s1.begin() }
            , m_it2 { parent.m_s2.begin() }
        {
            if (m_it1 == m_parent->m_pos) {
                if (m_it2 == m_parent->m_s2.end()) {
                    if (m_it1 == m_parent->m_s1.end()) {
                        m_state = end;
                    }
                    else [[likely]] {
                        m_state = s1;
                    }
                }
                else [[likely]] {
                    m_state = s2;
                }
            }
            else [[likely]] {
                if (m_it1 == m_parent->m_s1.end()) {
                    if (m_it2 == m_parent->m_s2.end()) {
                        m_state = end;
                    }
                    else [[likely]] {
                        m_state = s2;
                    }
                }
                else [[likely]] {
                    m_state = s1;
                }
            }
        }

    private:
        friend class iter::iterator_core_access;

        auto const&
        dereference() const
        {
            if (m_state == s1) {
                return *m_it1;
            }
            else {
                return *m_it2;
            }
        }

        void
        increment()
        // [[expects: m_parent]]
        {
            if (m_state == s1) {
                ++m_it1;

                if (m_it1 == m_parent->m_pos) {
                    if (m_it2 == m_parent->m_s2.end()) {
                        if (m_it1 == m_parent->m_s1.end()) {
                            m_state = end;
                        }
                        else {
                            m_state = s1;
                        }
                    }
                    else [[likely]] {
                        m_state = s2;
                    }
                }
                else if (m_it1 == m_parent->m_s1.end()) {
                    m_state = end;
                }
                else [[likely]] {}
            }
            else {
                ++m_it2;

                if (m_it2 == m_parent->m_s2.end()) {
                    if (m_it1 != m_parent->m_s1.end()) {
                        m_state = s1;
                    }
                    else {
                        m_state = end;
                    }
                }
                else [[likely]] {}
            }
        }

        bool
        equal(iterator const& other) const
        {
            return m_state == other.m_state
                && m_it1 == other.m_it1
                && m_it2 == other.m_it2 ;
        }

        bool
        equal(sentinel const&) const
        {
            return m_state == end;
        }

    private:
        view const* m_parent = nullptr;
        iterator_t<S1 const> m_it1;
        iterator_t<S2 const> m_it2;
        state m_state = end;
    };

    template<typename S1, typename S2>
    view(S1&&, iterator_t<S1>, S2&&) -> view<_uncvref<S1>, _uncvref<S2>>;

    struct api
    {
        /*
         * by index
         */
        template<random_access_string S, character C>
            requires is_same_char_v<S, C>
        auto
        operator()(S&& s, str_index_t<S> pos, C ch) const
        {
            return operator()(s, str::iterator_at(s, pos), ch);
        }

        template<random_access_string S1, forward_string S2>
            requires is_same_char_v<S1, S2>
        auto
        operator()(S1&& s1, str_index_t<S1> pos, S2&& s2) const
        {
            return operator()(s1, str::iterator_at(s1, pos), s2);
        }

        /*
         * by iterator
         */
        template<forward_string S, std::forward_iterator I, character C>
            requires is_same_char_v<S, C>
                  && std::convertible_to<I, iterator_t<_unref<S> const>>
        auto
        operator()(S&& s, I pos, C c) const
        {
            string_range const s1 = s;
            auto const s2 = rng::views::single(c);

            return view { s1, pos, s2 };
        }

        template<forward_string S1, std::forward_iterator I, forward_string S2>
            requires is_same_char_v<S1, S2>
                  && std::convertible_to<I, iterator_t<_unref<S1> const>>
        auto
        operator()(S1 const& s1_, I pos, S2 const& s2_) const
        {
            string_range const s1 = s1_;
            string_range const s2 = s2_;

            return view { s1, pos , s2 };
        }
    };

} // namespace _insert

inline constexpr _insert::api insert;

} // namespace stream9::strings::views

#endif // STRINGS_VIEW_INSERT_HPP
