#ifndef STRINGS_VIEW_JOIN_HPP
#define STRINGS_VIEW_JOIN_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"
#include "../core/string_range.hpp"
#include "../core/string_traits.hpp"
#include "../core/view_interface.hpp"

#include "../detail/view_base.hpp"

#include <cassert>
#include <concepts>
#include <iterator>
#include <ranges>

#include <stream9/iterators.hpp>

namespace stream9::strings::views {

namespace _join {

    template<rng::range InputT, forward_string SeparatorT>
    class view : public view_interface<view<InputT, SeparatorT>>
               , public rng::view_interface<view<InputT, SeparatorT>>
               , public view_base
    {
    public:
        class iterator;
        using sentinel = std::default_sentinel_t;
        using element_t = rng::range_value_t<InputT>;

    public:
        view() = default;

        template<typename T, typename U>
        view(T&& inputs, U&& separator)
            : m_inputs { inputs }
            , m_separator { separator }
        {}

        iterator begin() const { return { *this }; }
        sentinel end() const { return {}; }

    private:
        rng::subrange<iterator_t<InputT>> m_inputs;
        string_range<iterator_t<SeparatorT>> m_separator;
    };

    template<rng::range InputT, forward_string SeparatorT>
    class view<InputT, SeparatorT>::iterator
        : public iter::iterator_facade<iterator,
                                 std::forward_iterator_tag,
                                 str_char_t<element_t> const& >
    {
        enum state { element, separator, end };
    public:
        iterator() = default;

        iterator(view const& v)
            : m_inputs { v.m_inputs }
            , m_separator { v.m_separator }
        {
            next_element(true);
        }

    private:
        friend class iter::iterator_core_access;

        auto const&
        dereference() const
        // [[expects: m_state != end]]
        {
            switch (m_state) {
                default:
                case element:
                    return *m_element.begin();
                case separator:
                    return *m_sep_it;
            }
        }

        void
        increment()
        // [[expects: m_state != end]]
        {
            if (m_state == element) {
                m_element.advance(1);

                if (m_element.empty()) {
                    next_element();
                }
            }
            else if (m_state == separator) {
                ++m_sep_it;

                if (m_sep_it == m_separator.end()) {
                    if (m_element.empty()) {
                        next_element();
                    }
                    else {
                        m_state = element;
                    }
                }
            }
        }

        bool
        equal(iterator const& other) const
        {
            return rng::equal(m_inputs, other.m_inputs)
                && m_separator == other.m_separator
                && m_element == other.m_element
                && m_sep_it == other.m_sep_it
                && m_state == other.m_state;
        }

        bool
        equal(sentinel const&) const
        {
            return m_state == end;
        }

        void next_element(bool first = false)
        {
            while (true) {
                if (m_inputs.empty()) {
                    m_state = end;
                    break;
                }
                else {
                    m_element = m_inputs.front();
                    m_inputs.advance(1);

                    if (!first && !m_separator.empty()) {
                        m_sep_it = m_separator.begin();
                        m_state = separator;
                        break;
                    }

                    if (!m_element.empty()) {
                        m_state = element;
                        break;
                    }
                }
                first = false;
            }
        }

    private:
        rng::subrange<iterator_t<InputT>> m_inputs;
        string_range<iterator_t<SeparatorT>> m_separator;
        string_range<iterator_t<element_t>> m_element;
        iterator_t<SeparatorT> m_sep_it;
        state m_state = end;
    };

    struct api
    {
        template<rng::range InputT, forward_string SeparatorT>
            requires forward_string<std::iter_value_t<iterator_t<InputT>>>
                  && is_same_char_v<std::iter_value_t<iterator_t<InputT>>, SeparatorT>
        auto
        operator()(InputT&& inputs, SeparatorT&& sep) const
        {
            return view<_unref<InputT>, _unref<SeparatorT>> { inputs, sep };
        }
    };

} // namespace _join

inline constexpr _join::api join;

} // namespace stream9::strings::views

#endif // STRINGS_VIEW_JOIN_HPP
