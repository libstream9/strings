#ifndef STRINGS_VIEW_TO_UPPER_HPP
#define STRINGS_VIEW_TO_UPPER_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"
#include "../core/string_range.hpp"
#include "../core/string_adaptor.hpp"

#include "../detail/ctype_facet.hpp"

#include <cassert>
#include <locale>
#include <ranges>

namespace stream9::strings::views {

namespace _to_upper {

    struct api
    {
        template<forward_string S>
        auto
        operator()(S&& s_, std::locale const& loc = {}) const
        {
            string_range const s = s_;

            auto* const facet = &detail::ctype_facet<S>(loc);
            assert(facet);

            auto f = [facet](auto c) { return facet->toupper(c); };

            return string_adaptor { rng::views::transform(s, f) };
        }
    };

} // namespace _to_upper

inline constexpr _to_upper::api to_upper;

} // namespace stream9::strings::views

#endif // STRINGS_VIEW_TO_UPPER_HPP
