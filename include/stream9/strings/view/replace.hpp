#ifndef STRINGS_VIEW_REPLACE_HPP
#define STRINGS_VIEW_REPLACE_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"
#include "../core/string_traits.hpp"
#include "../core/string_range.hpp"
#include "../core/view_interface.hpp"

#include "../query/substr.hpp"

#include "../detail/view_base.hpp"

#include <cassert>
#include <concepts>
#include <iterator>
#include <ranges>

#include <stream9/iterators.hpp>

namespace stream9::strings::views {

namespace _replace {

    template<typename S, typename ReplacementT>
    class view : public view_interface<view<S, ReplacementT>>
               , public rng::view_interface<view<S, ReplacementT>>
               , public view_base
    {
    public:
        class iterator;
        using sentinel = std::default_sentinel_t;

    public:
        view() = default;

        view(string_range<iterator_t<S>> src,
             string_range<iterator_t<S>> skip,
             string_range<iterator_t<ReplacementT>> replacement)
            : m_src { src }
            , m_skip { skip }
            , m_replacement { replacement }
        {}

        iterator begin() const
        {
            return { *this, m_src.begin(), m_replacement.begin() };
        }

        sentinel end() const { return {}; }

    private:
        string_range<iterator_t<S>> m_src;
        string_range<iterator_t<S>> m_skip;
        string_range<iterator_t<ReplacementT>> m_replacement;
    };

    template<typename S1, typename S2, typename S3>
    view(S1&&, S2&&, S3&&) -> view<_unref<S1>, _unref<S3>>;

    template<typename S, typename ReplacementT>
    class view<S, ReplacementT>::iterator
        : public iter::iterator_facade<iterator,
                                 std::forward_iterator_tag,
                                 str_char_t<S> const& >
    {
        enum state { copy, replace, end };
    public:
        iterator() = default;

        iterator(view const& v,
                 iterator_t<S> const src,
                 iterator_t<ReplacementT> const replacement)
            : m_parent { &v }
            , m_src { src }
            , m_replacement { replacement }
        {
            init();
        }

    private:
        friend class iter::iterator_core_access;

        void
        init()
        {
            auto const& skip = m_parent->m_skip;

            if (m_src == m_parent->m_src.end()) {
                if (m_replacement == m_parent->m_replacement.end()) {
                    m_state = end;
                }
                else {
                    m_state = replace;
                }
            }
            else if (skip.begin() == m_src) {
                std::advance(m_src,
                    to_difference<decltype(skip)>(skip.size()) );

                if (m_replacement == m_parent->m_replacement.end()) {
                    m_state = copy;
                }
                else {
                    m_state = replace;
                }
            }
            else {
                m_state = copy;
            }
        }

        auto const&
        dereference() const
        {
            switch (m_state) {
                case copy:
                    return *m_src;
                case replace:
                    return *m_replacement;
                case end:
                    break;
            }

            return *m_src; // undefined behavior
        }

        void
        increment()
        {
            switch (m_state) {
                case copy: {
                    ++m_src;

                    auto const& skip = m_parent->m_skip;
                    if (m_src == skip.begin()) {
                        std::advance(m_src,
                            to_difference<decltype(skip)>(skip.size()));

                        if (m_replacement == m_parent->m_replacement.end()) {
                            if (m_src == m_parent->m_src.end()) {
                                m_state = end;
                            }
                        }
                        else {
                            m_state = replace;
                        }
                    }
                    else if (m_src == m_parent->m_src.end()) {
                        m_state = end;
                    }
                    break;
                }
                case replace:
                    ++m_replacement;

                    if (m_replacement == m_parent->m_replacement.end()) {
                        if (m_src == m_parent->m_src.end()) {
                            m_state = end;
                        }
                        else {
                            m_state = copy;
                        }
                    }
                    break;
                case end:
                    break; // undefined behavior
            }
        }

        bool
        equal(iterator const& other) const
        {
            return m_state == other.m_state
                && m_src == other.m_src
                && m_replacement == other.m_replacement;
        }

        bool
        equal(sentinel const&) const
        {
            return m_state == end;
        }

    private:
        view const* m_parent = nullptr;
        iterator_t<S> m_src;
        iterator_t<ReplacementT> m_replacement;
        state m_state = end;
    };

    struct api
    {
        template<forward_string S1, forward_string S2, readable_string S3>
            requires std::convertible_to<iterator_t<S2>, iterator_t<S1>>
                  && std::convertible_to<sentinel_t<S2>, sentinel_t<S1>>
                  && is_same_char_v<S1, S2, S3>
        auto
        operator()(S1&& src, S2&& subrange_of_src, S3&& replacement) const
        {
            return view { src, subrange_of_src, replacement };
        }

        template<forward_string S1, std::forward_iterator I, readable_string S2>
            requires is_same_char_v<S1, S2>
                  && std::convertible_to<I, iterator_t<S1 const>>
        auto
        operator()(S1&& s1, I first, sentinel_t<S1 const> last, S2&& s2) const
        {
            return view { s1, string_range(first, last), s2 };
        }

        template<random_access_string S1, readable_string S2>
            requires is_same_char_v<S1, S2>
        auto
        operator()(S1&& s1, str_index_t<S1> pos, str_size_t<S1> n, S2&& s2) const
        {
            assert(to_size<S1>(pos) + n <= str::size(s1));

            return view { s1, str::substr(s1, pos, n), s2 };
        }
    };

} // namespace _replace

inline constexpr _replace::api replace;

} // namespace stream9::strings::views

#endif // STRINGS_VIEW_REPLACE_HPP
