#ifndef STRINGS_VIEW_FIND_REPLACE_HPP
#define STRINGS_VIEW_FIND_REPLACE_HPP

#include "../core/concepts.hpp"
#include "../core/finder.hpp"
#include "../core/regex.hpp"
#include "../core/string_range.hpp"

#include "../query/find_first.hpp"

#include "replace.hpp"

namespace stream9::strings::views {

namespace _find_replace {

    template<typename S, typename ReplacementT>
    class view : public view_interface<view<S, ReplacementT>>
               , public rng::view_interface<view<S, ReplacementT>>
               , public view_base
    {
    public:
        class iterator;
        using sentinel = std::default_sentinel_t;

    public:
        view() = default;

        view(string_range<iterator_t<S>> src,
             string_range<iterator_t<S>> subrange,
             string_range<iterator_t<ReplacementT>> replacement)
            : m_src { src }
            , m_subrange { subrange }
            , m_replacement { replacement }
        {}

        iterator begin() const
        {
            return { *this, m_src.begin(), m_replacement.begin() };
        }

        sentinel end() const { return {}; }

    private:
        string_range<iterator_t<S>> m_src;
        string_range<iterator_t<S>> m_subrange;
        string_range<iterator_t<ReplacementT>> m_replacement;
    };

    template<typename S1, typename S2, typename S3>
    view(S1&&, S2&&, S3&&) -> view<_unref<S1>, _unref<S3>>;

    template<typename S, typename ReplacementT>
    class view<S, ReplacementT>::iterator
        : public iter::iterator_facade<iterator,
                                 std::forward_iterator_tag,
                                 str_char_t<S> const& >
    {
        enum state { copy, replace, end };
    public:
        iterator() = default;

        iterator(view const& v,
                 iterator_t<S> const src,
                 iterator_t<ReplacementT> const replacement)
            : m_parent { &v }
            , m_src { src }
            , m_replacement { replacement }
        {
            init();
        }

    private:
        friend class iter::iterator_core_access;

        void
        init()
        {
            auto const& subrange = m_parent->m_subrange;

            if (m_parent->m_src.empty()) {
                m_state = end;
            }
            else if (subrange.empty()) {
                m_state = copy;
            }
            else if (subrange.begin() == m_src) {
                std::advance(m_src,
                    to_difference<decltype(subrange)>(subrange.size()) );

                if (m_parent->m_replacement.empty()) {
                    m_state = copy;
                }
                else {
                    m_state = replace;
                }
            }
            else {
                m_state = copy;
            }
        }

        auto const&
        dereference() const
        {
            switch (m_state) {
                case copy:
                    return *m_src;
                case replace:
                    return *m_replacement;
                case end:
                    break;
            }

            return *m_src; // undefined behavior
        }

        void
        increment()
        {
            switch (m_state) {
                case copy: {
                    ++m_src;

                    auto const& subrange = m_parent->m_subrange;
                    if (m_src == subrange.begin() && !subrange.empty()) {
                        std::advance(m_src,
                            to_difference<decltype(subrange)>(subrange.size()));

                        if (m_replacement == m_parent->m_replacement.end()) {
                            if (m_src == m_parent->m_src.end()) {
                                m_state = end;
                            }
                        }
                        else {
                            m_state = replace;
                        }
                    }
                    else if (m_src == m_parent->m_src.end()) {
                        m_state = end;
                    }
                    break;
                }
                case replace:
                    ++m_replacement;

                    if (m_replacement == m_parent->m_replacement.end()) {
                        if (m_src == m_parent->m_src.end()) {
                            m_state = end;
                        }
                        else {
                            m_state = copy;
                        }
                    }
                    break;
                case end:
                    break; // undefined behavior
            }
        }

        bool
        equal(iterator const& other) const
        {
            return m_state == other.m_state
                && m_src == other.m_src
                && m_replacement == other.m_replacement;
        }

        bool
        equal(sentinel const&) const
        {
            return m_state == end;
        }

    private:
        view const* m_parent = nullptr;
        iterator_t<S> m_src;
        iterator_t<ReplacementT> m_replacement;
        state m_state = end;
    };

    struct api
    {
        template<forward_string S, forward_string KeywordT, forward_string ReplacementT>
            requires is_same_char_v<S, KeywordT, ReplacementT>
        auto
        operator()(S&& s1, KeywordT&& s2, ReplacementT&& s3) const
        {
            auto const subrange = str::find_first(s1, s2);

            return view { s1, subrange, s3 };
        }

        template<bidirectional_string S, regexp R, forward_string ReplacementT>
            requires is_same_char_v<S, R, ReplacementT>
        auto
        operator()(S&& s1, R&& e, ReplacementT&& s2) const
        {
            auto const subrange = str::find_first(s1, e);

            return view { s1, subrange, s2 };
        }

        template<forward_string S, typename FinderT, forward_string ReplacementT>
            requires is_same_char_v<S, ReplacementT>
                  && finder<FinderT, S>
        auto
        operator()(S&& s1, FinderT&& finder, ReplacementT&& s2) const
        {
            auto const subrange = finder(s1);

            return view { s1, subrange, string_range(s2) };
        }
    };

} // namespace _find_replace

inline constexpr _find_replace::api find_replace;

} // namespace stream9::strings::views

#endif // STRINGS_VIEW_FIND_REPLACE_HPP
