#ifndef STRINGS_VIEW_ERASE_HPP
#define STRINGS_VIEW_ERASE_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"
#include "../core/string_traits.hpp"

#include "replace.hpp"

#include <iterator>

namespace stream9::strings::views {

namespace _erase {

    struct api
    {
        /*
         * by index
         */
        template<forward_string S>
        auto
        operator()(S&& s, str_index_t<S> pos) const
        {
            return operator()(s, pos, 1);
        }

        template<forward_string S>
        auto
        operator()(S&& s, str_index_t<S> pos, str_size_t<S> n) const
        {
            return str::views::replace(s, pos, n, "");
        }

        /*
         * by iterator
         */
        template<forward_string S>
        auto
        operator()(S&& s, iterator_t<_unref<S const>> pos) const
        {
            string_range s1 { pos, rng::next(pos) };
            return operator()(s, s1);
        }

        /*
         * by range
         */
        template<forward_string S>
        auto
        operator()(S&& s, string_range<iterator_t<_unref<S const>>> range) const
        {
            return str::views::replace(s, range, "");
        }
    };

} // namespace _erase

inline constexpr _erase::api erase;

} // namespace stream9::strings::views

#endif // STRINGS_VIEW_ERASE_HPP
