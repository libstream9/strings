#ifndef STREAM9_STRINGS_UTILITY_CSTRING_VIEW_HPP
#define STREAM9_STRINGS_UTILITY_CSTRING_VIEW_HPP

#include "../core/char/concepts.hpp"

#include <cassert>
#include <cstddef>
#include <iosfwd>
#include <iterator>
#include <ranges>
#include <stdexcept>

#include <stream9/iterators.hpp>

namespace stream9::strings {

template<character> class cstring_view_iterator;

template<character CharT, typename Traits = std::char_traits<CharT>>
class basic_cstring_view
{
public:
    using value_type = CharT;
    using traits_type = Traits;
    using const_pointer = CharT const*;
    using const_reference = CharT const&;
    using size_type = size_t;
    using difference_type = std::ptrdiff_t;
    using const_iterator = cstring_view_iterator<CharT>;
    using sentinel = std::default_sentinel_t;
    using comparison_category = typename Traits::comparison_category;

public:
    constexpr basic_cstring_view() noexcept = default; // partially-formed

    constexpr basic_cstring_view(const_pointer) noexcept;

    template<typename T>
        requires requires (T s) {
            { s.c_str() } -> std::convertible_to<CharT const*>;
        }
    constexpr basic_cstring_view(T const&) noexcept;

    explicit constexpr basic_cstring_view(std::basic_string_view<CharT, Traits> const&);

    basic_cstring_view(std::nullptr_t) = delete;

    // accessor
    constexpr const_iterator begin() const noexcept { return m_begin; }
    constexpr const_iterator cbegin() const noexcept { return m_begin; }

    constexpr sentinel end() const noexcept { return {}; }
    constexpr sentinel cend() const noexcept { return {}; }

    constexpr const_reference operator[](size_type pos) const noexcept { return m_begin[pos]; }

    constexpr const_reference front() const noexcept { return operator[](0); }

    constexpr const_pointer data() const noexcept { return m_begin; }
    constexpr const_pointer c_str() const noexcept { return m_begin ? m_begin : ""; }

    // query
    constexpr size_t size() const noexcept;
    constexpr bool empty() const noexcept;

    // modifier
    constexpr void remove_prefix(size_type n) noexcept;

    // operator
    constexpr bool operator==(basic_cstring_view const&) const noexcept;
    constexpr bool operator==(const_pointer) const noexcept;
    constexpr bool operator==(std::basic_string_view<CharT, Traits> const&) const noexcept;
    template<typename Allocator>
    constexpr bool operator==(std::basic_string<CharT, Traits, Allocator> const&) const noexcept;

    constexpr comparison_category operator<=>(basic_cstring_view const&) const noexcept;
    constexpr comparison_category operator<=>(const_pointer) const noexcept;
    constexpr comparison_category operator<=>(std::basic_string_view<CharT, Traits> const&) const noexcept;
    template<typename Allocator>
    constexpr comparison_category operator<=>(std::basic_string<CharT, Traits, Allocator> const&) const noexcept;

    // conversion
    constexpr operator const_pointer () const noexcept { return m_begin; }
    constexpr operator std::basic_string_view<CharT, Traits> () const noexcept; //TODO explicit?

    template<typename Allocator>
    explicit constexpr operator std::basic_string<CharT, Traits, Allocator> () const noexcept;

private:
    const_pointer m_begin = nullptr;
};

using cstring_view = basic_cstring_view<char>;

template<typename CharT, typename Traits>
inline std::basic_ostream<CharT, Traits>&
operator<<(std::basic_ostream<CharT, Traits>& os,
           basic_cstring_view<CharT, Traits> const& s)
{
    return os << s.c_str();
}

template<character CharT>
class cstring_view_iterator : public iter::iterator_facade<
                                        cstring_view_iterator<CharT>,
                                        std::contiguous_iterator_tag,
                                        CharT const&>
{
public:
    cstring_view_iterator() = default;

    constexpr cstring_view_iterator(CharT const* ptr) noexcept
        : m_ptr { ptr }
    {}

public:
    friend class iter::iterator_core_access;

    constexpr CharT const& dereference() const noexcept { return *m_ptr; }

    constexpr void increment() noexcept { ++m_ptr; }

    constexpr void decrement() noexcept { --m_ptr; }

    constexpr void advance(std::ptrdiff_t const n) noexcept { m_ptr += n; }

    constexpr std::ptrdiff_t distance_to(cstring_view_iterator const& other) const noexcept
    {
        return other.m_ptr - m_ptr;
    }

    constexpr bool equal(cstring_view_iterator const& other) const noexcept
    {
        return m_ptr == other.m_ptr;
    }

    constexpr auto compare(cstring_view_iterator const& other) const noexcept
    {
        return m_ptr <=> other.m_ptr;
    }

    constexpr bool equal(std::default_sentinel_t const s) const noexcept
    {
        return compare(s) == std::strong_ordering::equivalent;
    }

    constexpr auto compare(std::default_sentinel_t) const noexcept
    {
        if (*m_ptr == null_char_v<CharT>) {
            return std::strong_ordering::equivalent;
        }
        else {
            return std::strong_ordering::greater;
        }
    }

private:
    CharT const* m_ptr = nullptr;
};

template<character CharT, typename Traits>
constexpr basic_cstring_view<CharT, Traits>::
basic_cstring_view(const_pointer const s) noexcept
    : m_begin { s }
{
    assert(s);
}

template<character CharT, typename Traits>
constexpr basic_cstring_view<CharT, Traits>::
basic_cstring_view(std::basic_string_view<CharT, Traits> const& s)
    : m_begin { s.data() }
{
    if (m_begin && *(m_begin + s.size()) != null_char_v<CharT>) {
        throw std::range_error("string is not terminated with null");
    }
}

template<character CharT, typename Traits>
template<typename T>
    requires requires (T s) {
        { s.c_str() } -> std::convertible_to<CharT const*>;
    }
constexpr basic_cstring_view<CharT, Traits>::
basic_cstring_view(T const& s) noexcept
    : basic_cstring_view { s.c_str() }
{}

template<character CharT, typename Traits>
constexpr size_t basic_cstring_view<CharT, Traits>::
size() const noexcept
{
    if (m_begin == nullptr) {
        return 0;
    }
    else {
        return traits_type::length(m_begin);
    }
}

template<character CharT, typename Traits>
constexpr bool basic_cstring_view<CharT, Traits>::
empty() const noexcept
{
    if (m_begin == nullptr) {
        return true;
    }
    else {
        return *m_begin == null_char_v<CharT>;
    }
}

template<character CharT, typename Traits>
constexpr void basic_cstring_view<CharT, Traits>::
remove_prefix(size_type const n) noexcept
{
    m_begin += n;
}

template<character CharT, typename Traits>
constexpr bool basic_cstring_view<CharT, Traits>::
operator==(basic_cstring_view const& other) const noexcept
{
    return (*this <=> other) == comparison_category::equivalent;
}

template<character CharT, typename Traits>
constexpr bool basic_cstring_view<CharT, Traits>::
operator==(const_pointer const other) const noexcept
{
    return (*this <=> other) == comparison_category::equivalent;
}

template<character CharT, typename Traits>
constexpr bool basic_cstring_view<CharT, Traits>::
operator==(std::basic_string_view<CharT, Traits> const& other) const noexcept
{
    return (*this <=> other) == comparison_category::equivalent;
}

template<character CharT, typename Traits>
template<typename Allocator>
constexpr bool basic_cstring_view<CharT, Traits>::
operator==(std::basic_string<CharT, Traits, Allocator> const& other) const noexcept
{
    return (*this <=> other) == comparison_category::equivalent;
}

template<character CharT, typename Traits>
constexpr basic_cstring_view<CharT, Traits>::comparison_category basic_cstring_view<CharT, Traits>::
operator<=>(basic_cstring_view const& other) const noexcept
{
    auto const terminator = null_char_v<CharT>;

    if (empty()) {
        return other.empty() ? comparison_category::equivalent
                             : comparison_category::less;
    }
    else if (other.empty()) {
        return comparison_category::greater;
    }

    auto it1 = m_begin, it2 = other.m_begin;
    while (true) {
        if (*it1 == terminator) {
            if (*it2 == terminator) {
                return comparison_category::equivalent;
            }
            else {
                return comparison_category::less;
            }
        }
        else if (*it2 == terminator) {
            return comparison_category::greater;
        }
        else if (auto cmp = *it1 <=> *it2; cmp != 0) {
            return cmp;
        }

        ++it1, ++it2;
    }
}

template<character CharT, typename Traits>
constexpr basic_cstring_view<CharT, Traits>::comparison_category basic_cstring_view<CharT, Traits>::
operator<=>(const_pointer const other) const noexcept
{
    return *this <=> basic_cstring_view { other };
}

template<character CharT, typename Traits>
constexpr basic_cstring_view<CharT, Traits>::comparison_category basic_cstring_view<CharT, Traits>::
operator<=>(std::basic_string_view<CharT, Traits> const& other) const noexcept
{
    auto const terminator = null_char_v<CharT>;

    if (empty()) {
        return other.empty() ? comparison_category::equivalent
                             : comparison_category::less;
    }
    else if (other.empty()) {
        return comparison_category::greater;
    }

    auto it1 = m_begin, it2 = other.data();
    auto end2 = it2 + other.size();
    while (true) {
        if (*it1 == terminator) {
            if (it2 == end2) {
                return comparison_category::equivalent;
            }
            else {
                return comparison_category::less;
            }
        }
        else if (it2 == end2) {
            return comparison_category::greater;
        }
        else if (auto cmp = *it1 <=> *it2; cmp != 0) {
            return cmp;
        }

        ++it1, ++it2;
    }
}

template<character CharT, typename Traits>
template<typename Allocator>
constexpr basic_cstring_view<CharT, Traits>::comparison_category basic_cstring_view<CharT, Traits>::
operator<=>(std::basic_string<CharT, Traits, Allocator> const& other) const noexcept
{
    return *this <=> basic_cstring_view { other };
}

template<character CharT, typename Traits>
constexpr basic_cstring_view<CharT, Traits>::
operator std::basic_string_view<CharT, Traits> () const noexcept
{
    if (m_begin) {
        return m_begin;
    }
    else {
        return {};
    }
}

template<character CharT, typename Traits>
template<typename Allocator>
constexpr basic_cstring_view<CharT, Traits>::
operator std::basic_string<CharT, Traits, Allocator> () const noexcept
{
    if (m_begin) {
        return m_begin;
    }
    else {
        return {};
    }
}

} // namespace stream9::strings

namespace std::ranges {

template<typename CharT, typename Traits>
inline constexpr bool enable_borrowed_range<
    stream9::strings::basic_cstring_view<CharT, Traits> > = true;

template<typename CharT, typename Traits>
inline constexpr bool enable_view<
    stream9::strings::basic_cstring_view<CharT, Traits> > = true;

template<typename CharT, typename Traits>
inline constexpr bool disable_sized_range<
    stream9::strings::basic_cstring_view<CharT, Traits> > = true;

} // namespace std::ranges

#endif // STREAM9_STRINGS_UTILITY_CSTRING_VIEW_HPP
