#ifndef STREAM9_STRINGS_UTILITY_SIZED_CSTRING_VIEW_HPP
#define STREAM9_STRINGS_UTILITY_SIZED_CSTRING_VIEW_HPP

#include "../core/char/concepts.hpp"

#include "cstring_view.hpp"

#include <algorithm>
#include <cassert>
#include <cstddef>
#include <iosfwd>
#include <iterator>
#include <ranges>
#include <stdexcept>

namespace stream9::strings {

template<character CharT, typename Traits = std::char_traits<CharT>>
class basic_sized_cstring_view
{
public:
    using value_type = CharT;
    using traits_type = Traits;
    using const_pointer = CharT const*;
    using const_reference = CharT const&;
    using size_type = size_t;
    using difference_type = std::ptrdiff_t;
    using const_iterator = const_pointer;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;
    using comparison_category = typename Traits::comparison_category;

public:
    constexpr basic_sized_cstring_view() = default;

    explicit constexpr basic_sized_cstring_view(const_pointer) noexcept;

    template<size_t N>
    constexpr basic_sized_cstring_view(CharT const (&s)[N]);

    constexpr basic_sized_cstring_view(const_pointer, size_type len);
    constexpr basic_sized_cstring_view(const_pointer begin, const_pointer end);

    explicit constexpr basic_sized_cstring_view(basic_cstring_view<CharT, Traits> const&) noexcept;

    template<typename Allocator>
    constexpr basic_sized_cstring_view(std::basic_string<CharT, Traits, Allocator> const&);

    explicit constexpr basic_sized_cstring_view(std::basic_string_view<CharT, Traits> const&);

    basic_sized_cstring_view(std::nullptr_t) = delete;

    // accessor
    constexpr const_iterator begin() const noexcept { return m_begin; }
    constexpr const_iterator cbegin() const noexcept { return m_begin; }
    constexpr const_reverse_iterator rbegin() const noexcept { return end(); }
    constexpr const_reverse_iterator crbegin() const noexcept { return end(); }

    constexpr const_iterator end() const noexcept { return m_end; }
    constexpr const_iterator cend() const noexcept { return m_end; }
    constexpr const_reverse_iterator rend() const noexcept { return begin(); }
    constexpr const_reverse_iterator crend() const noexcept { return begin(); }

    constexpr const_reference operator[](size_type pos) const noexcept { return m_begin[pos]; }
    constexpr const_reference at(size_type pos) const;

    constexpr const_reference front() const noexcept { return operator[](0); }
    constexpr const_reference back() const noexcept;

    constexpr const_pointer data() const noexcept { return m_begin; }
    constexpr const_pointer c_str() const noexcept { return m_begin ? m_begin : ""; }

    // query
    constexpr size_t size() const noexcept;
    constexpr bool empty() const noexcept;

    // modifier
    constexpr void remove_prefix(size_type n) noexcept;

    // operator
    constexpr bool operator==(basic_sized_cstring_view const&) const noexcept;
    constexpr bool operator==(basic_cstring_view<CharT, Traits> const&) const noexcept;
    constexpr bool operator==(const_pointer) const noexcept;
    template<typename Allocator>
    constexpr bool operator==(std::basic_string<CharT, Traits, Allocator> const&) const noexcept;
    constexpr bool operator==(std::basic_string_view<CharT, Traits> const&) const noexcept;

    constexpr comparison_category operator<=>(basic_sized_cstring_view const&) const noexcept;
    constexpr comparison_category operator<=>(basic_cstring_view<CharT, Traits> const&) const noexcept;
    constexpr comparison_category operator<=>(const_pointer) const noexcept;
    template<typename Allocator>
    constexpr comparison_category operator<=>(std::basic_string<CharT, Traits, Allocator> const&) const noexcept;
    constexpr comparison_category operator<=>(std::basic_string_view<CharT, Traits> const&) const noexcept;

    // conversion
    constexpr operator const_pointer () const noexcept { return m_begin; }
    constexpr operator basic_cstring_view<CharT, Traits> () const noexcept;
    constexpr operator std::basic_string_view<CharT, Traits> () const noexcept;
    template<typename Allocator>
    constexpr explicit operator std::basic_string<CharT, Traits, Allocator> () const noexcept;

private:
    const_pointer m_begin = nullptr;
    const_pointer m_end = nullptr;
};

using sized_cstring_view = basic_sized_cstring_view<char>;

template<typename CharT, typename Traits>
inline std::basic_ostream<CharT, Traits>&
operator<<(std::basic_ostream<CharT, Traits>& os,
           basic_sized_cstring_view<CharT, Traits> const& s)
{
    return os << s.c_str();
}

inline namespace literals {

    inline sized_cstring_view
    operator""_csv(char const* const str, size_t const len)
    {
        return { str, len };
    }

} // literals

template<character CharT, typename Traits>
constexpr basic_sized_cstring_view<CharT, Traits>::
basic_sized_cstring_view(const_pointer const s) noexcept
    : basic_sized_cstring_view { s, Traits::length(s) }
{}

template<character CharT, typename Traits>
template<size_t N>
constexpr basic_sized_cstring_view<CharT, Traits>::
basic_sized_cstring_view(CharT const (&s)[N])
    : basic_sized_cstring_view { s, s + N - 1 }
{}

template<character CharT, typename Traits>
constexpr basic_sized_cstring_view<CharT, Traits>::
basic_sized_cstring_view(std::basic_string_view<CharT, Traits> const& s)
    : basic_sized_cstring_view { s.data(), s.size() }
{}

template<character CharT, typename Traits>
constexpr basic_sized_cstring_view<CharT, Traits>::
basic_sized_cstring_view(const_pointer const p, size_type const len)
    : basic_sized_cstring_view { p, p + len }
{}

template<character CharT, typename Traits>
constexpr basic_sized_cstring_view<CharT, Traits>::
basic_sized_cstring_view(const_pointer const begin, const_pointer const end)
    : m_begin { begin }
    , m_end { end }
{
    assert(m_begin);

    if (*m_end != null_char_v<CharT>) {
        throw std::range_error("string is not terminated with null");
    }
}

template<character CharT, typename Traits>
constexpr basic_sized_cstring_view<CharT, Traits>::
basic_sized_cstring_view(basic_cstring_view<CharT, Traits> const& s) noexcept
    : basic_sized_cstring_view { s.data(), Traits::length(s.data()) }
{}

template<character CharT, typename Traits>
template<typename Allocator>
constexpr basic_sized_cstring_view<CharT, Traits>::
basic_sized_cstring_view(std::basic_string<CharT, Traits, Allocator> const& s)
    : basic_sized_cstring_view { s.c_str(), s.size() }
{}

template<character CharT, typename Traits>
constexpr basic_sized_cstring_view<CharT, Traits>::const_reference basic_sized_cstring_view<CharT, Traits>::
at(size_type const pos) const
{
    if (m_begin + pos >= m_end) {
        throw std::out_of_range("basic_sized_cstring_view::at() index is out of range");
    }

    return m_begin[pos];
}

template<character CharT, typename Traits>
constexpr basic_sized_cstring_view<CharT, Traits>::const_reference basic_sized_cstring_view<CharT, Traits>::
back() const noexcept
{
    return *(m_end - 1);
}

template<character CharT, typename Traits>
constexpr size_t basic_sized_cstring_view<CharT, Traits>::
size() const noexcept
{
    return static_cast<size_t>(m_end - m_begin);
}

template<character CharT, typename Traits>
constexpr bool basic_sized_cstring_view<CharT, Traits>::
empty() const noexcept
{
    return m_begin == m_end;
}

template<character CharT, typename Traits>
constexpr void basic_sized_cstring_view<CharT, Traits>::
remove_prefix(size_type const n) noexcept
{
    m_begin += n;
}

template<character CharT, typename Traits>
constexpr bool basic_sized_cstring_view<CharT, Traits>::
operator==(basic_sized_cstring_view const& other) const noexcept
{
    return (*this <=> other) == comparison_category::equivalent;
}

template<character CharT, typename Traits>
constexpr bool basic_sized_cstring_view<CharT, Traits>::
operator==(basic_cstring_view<CharT, Traits> const& other) const noexcept
{
    return (*this <=> other) == comparison_category::equivalent;
}

template<character CharT, typename Traits>
constexpr bool basic_sized_cstring_view<CharT, Traits>::
operator==(const_pointer const other) const noexcept
{
    return (*this <=> other) == comparison_category::equivalent;
}

template<character CharT, typename Traits>
template<typename Allocator>
constexpr bool basic_sized_cstring_view<CharT, Traits>::
operator==(std::basic_string<CharT, Traits, Allocator> const& other) const noexcept
{
    return (*this <=> other) == comparison_category::equivalent;
}

template<character CharT, typename Traits>
constexpr bool basic_sized_cstring_view<CharT, Traits>::
operator==(std::basic_string_view<CharT, Traits> const& other) const noexcept
{
    return (*this <=> other) == comparison_category::equivalent;
}

template<character CharT, typename Traits>
constexpr basic_sized_cstring_view<CharT, Traits>::comparison_category basic_sized_cstring_view<CharT, Traits>::
operator<=>(basic_sized_cstring_view const& other) const noexcept
{
    if (empty()) {
        return other.empty() ? comparison_category::equivalent
                             : comparison_category::less;
    }
    else if (other.empty()) {
        return comparison_category::greater;
    }

    return std::lexicographical_compare_three_way(
        m_begin, m_end,
        other.m_begin, other.m_end
    );
}

template<character CharT, typename Traits>
constexpr basic_sized_cstring_view<CharT, Traits>::comparison_category basic_sized_cstring_view<CharT, Traits>::
operator<=>(basic_cstring_view<CharT, Traits> const& other) const noexcept
{
    return basic_cstring_view { m_begin } <=> other;
}

template<character CharT, typename Traits>
constexpr basic_sized_cstring_view<CharT, Traits>::comparison_category basic_sized_cstring_view<CharT, Traits>::
operator<=>(const_pointer const other) const noexcept
{
    return basic_cstring_view<CharT, Traits> { m_begin } <=> other;
}

template<character CharT, typename Traits>
template<typename Allocator>
constexpr basic_sized_cstring_view<CharT, Traits>::comparison_category basic_sized_cstring_view<CharT, Traits>::
operator<=>(std::basic_string<CharT, Traits, Allocator> const& other) const noexcept
{
    return *this <=> basic_sized_cstring_view { other };
}

template<character CharT, typename Traits>
constexpr basic_sized_cstring_view<CharT, Traits>::comparison_category basic_sized_cstring_view<CharT, Traits>::
operator<=>(std::basic_string_view<CharT, Traits> const& other) const noexcept
{
    return std::lexicographical_compare_three_way(
        m_begin, m_end,
        other.begin(), other.end()
    );
}

template<character CharT, typename Traits>
constexpr basic_sized_cstring_view<CharT, Traits>::
operator basic_cstring_view<CharT, Traits> () const noexcept
{
    return { m_begin };
}

template<character CharT, typename Traits>
constexpr basic_sized_cstring_view<CharT, Traits>::
operator std::basic_string_view<CharT, Traits> () const noexcept
{
    return { m_begin, m_end };
}

template<character CharT, typename Traits>
template<typename Allocator>
constexpr basic_sized_cstring_view<CharT, Traits>::
operator std::basic_string<CharT, Traits, Allocator> () const noexcept
{
    return { m_begin, m_end };
}

} // namespace stream9::strings

namespace std::ranges {

template<typename CharT, typename Traits>
inline constexpr bool enable_borrowed_range<
    stream9::strings::basic_sized_cstring_view<CharT, Traits> > = true;

template<typename CharT, typename Traits>
inline constexpr bool enable_view<
    stream9::strings::basic_sized_cstring_view<CharT, Traits> > = true;

} // namespace std::ranges

#endif // STREAM9_STRINGS_UTILITY_SIZED_CSTRING_VIEW_HPP
