#ifndef STRINGS_UTILITY_CSTRING_PTR_HPP
#define STRINGS_UTILITY_CSTRING_PTR_HPP

#include "../namespace.hpp"

#include "../core/char/concepts.hpp"
#include "../core/view_interface.hpp"

#include "../accessor/at.hpp"
#include "../accessor/data.hpp"
#include "../query/size.hpp"

#include <cassert>
#include <iterator>
#include <string>
#include <string_view>
#include <variant>

#include <stream9/iterators.hpp>

namespace stream9::strings {

template<character> class cstring_ptr_iterator;

template<typename C1, typename C2>
concept convertible_char =
        character<C1>
     && character<C2>
     && sizeof(C1) == sizeof(C2)
     && ((std::is_const_v<C2>) ||
         (!std::is_const_v<C2> && !std::is_const_v<C1>) )
     && std::is_volatile_v<C1> == std::is_volatile_v<C2>;

template<typename P1, typename P2>
concept convertible_ptr =
    std::is_pointer_v<P1> && std::is_pointer_v<P2> &&
    convertible_char<std::remove_pointer_t<P1>, std::remove_pointer_t<P2>>;

template<typename S, typename Ch>
concept convertible_character_range =
    character_range<S> &&
    character<Ch> &&
    requires (S&& s) {
        { str::data(s) } -> convertible_ptr<Ch const*>;
    };

template<typename S, typename Ch>
concept has_convertible_member_c_str = requires (S const s) {
    { s.c_str() } -> convertible_ptr<Ch const*>;
};

template<character Ch>
class basic_cstring_ptr : public view_interface<basic_cstring_ptr<Ch>>
{
public:
    using const_iterator = cstring_ptr_iterator<Ch const>;
    using sentinel = std::default_sentinel_t;

public:
    // essential
    basic_cstring_ptr() noexcept;

    basic_cstring_ptr(basic_cstring_ptr const&) noexcept;
    basic_cstring_ptr& operator=(basic_cstring_ptr const&) noexcept;

    basic_cstring_ptr(basic_cstring_ptr&&) = default;
    basic_cstring_ptr& operator=(basic_cstring_ptr&&) = default;

    basic_cstring_ptr(Ch const* ptr) noexcept;

    template<convertible_char<Ch> T>
    basic_cstring_ptr(T const* ptr) noexcept;

    template<convertible_character_range<Ch> S>
    basic_cstring_ptr(S&&);

    template<has_convertible_member_c_str<Ch> S>
        requires (!convertible_character_range<S, Ch>)
    basic_cstring_ptr(S&&);

    // accessor
    const_iterator begin() const noexcept;

    sentinel end() const noexcept;

    Ch const* data() const noexcept;

    // query
    size_t size() const noexcept;

    // comverter
    operator Ch const* () const noexcept;

private:
    using string_type = std::basic_string<_uncvref<Ch>>;
    using string_view_type = std::basic_string_view<_uncvref<Ch>>;

    std::variant<Ch const*, string_view_type, string_type> m_data;
};

using cstring_ptr = basic_cstring_ptr<char>;

//
// cstring_ptr_iterator
//
template<character Ch>
class cstring_ptr_iterator : public iter::iterator_facade<
                                    cstring_ptr_iterator<Ch>,
                                    std::contiguous_iterator_tag,
                                    Ch&>
{
public:
    cstring_ptr_iterator() = default;

    cstring_ptr_iterator(Ch const* const begin, Ch const* const end) noexcept
        : m_it { begin }
        , m_end { end }
    {}

private:
    friend class iter::iterator_core_access;

    Ch const& dereference() const noexcept
    {
        return *m_it;
    }

    void increment() noexcept
    {
        ++m_it;
    }

    void decrement() noexcept
    {
        --m_it;
    }

    void advance(std::ptrdiff_t const n) noexcept
    {
        m_it += n;
    }

    auto
    distance_to(cstring_ptr_iterator const& other) const noexcept
    {
        return other.m_it - m_it;
    }

    bool equal(cstring_ptr_iterator const& other) const noexcept
    {
        return m_it == other.m_it;
    }

    bool equal(basic_cstring_ptr<Ch>::sentinel const&) const noexcept
    {
        if (m_end) {
            return m_it == m_end;
        }
        else {
            return *m_it == null_char_v<Ch>;
        }
    }

    auto compare(cstring_ptr_iterator const& other) const noexcept
    {
        return m_it <=> other.m_it;
    }

private:
    Ch const* m_it; // non-null
    Ch const* m_end = nullptr;
};

//
// definition
//
template<character Ch>
basic_cstring_ptr<Ch>::
basic_cstring_ptr() noexcept
    : m_data { nullptr }
{}

template<character Ch>
basic_cstring_ptr<Ch>::
basic_cstring_ptr(basic_cstring_ptr const& other) noexcept
{
    if (auto* const o_ptr = std::get_if<Ch const*>(&other.m_data)) {
        m_data = *o_ptr;
    }
    else if (auto* const o_sv = std::get_if<string_view_type>(&other.m_data)) {
        m_data = *o_sv;
    }
    else {
        assert(std::holds_alternative<string_type>(other.m_data));

        auto& s = std::get<string_type>(other.m_data);

        m_data = string_view_type { s };
    }
}

template<character Ch>
basic_cstring_ptr<Ch>& basic_cstring_ptr<Ch>::
operator=(basic_cstring_ptr const& other) noexcept
{
    if (auto* const o_ptr = std::get_if<Ch const*>(&other.m_data)) {
        m_data = *o_ptr;
    }
    else if (auto* const o_sv = std::get_if<string_view_type>(&other.m_data)) {
        m_data = *o_sv;
    }
    else {
        assert(std::holds_alternative<string_type>(other.m_data));

        auto& s = std::get<string_type>(other.m_data);

        m_data = string_view_type { s };
    }
}

template<character Ch>
basic_cstring_ptr<Ch>::
basic_cstring_ptr(Ch const* const ptr) noexcept
    : m_data { ptr }
{}

template<character Ch>
template<convertible_char<Ch> T>
basic_cstring_ptr<Ch>::
basic_cstring_ptr(T const* const ptr) noexcept
    : m_data { reinterpret_cast<Ch const*>(ptr) }
{}

template<character Ch>
template<convertible_character_range<Ch> S>
basic_cstring_ptr<Ch>::
basic_cstring_ptr(S&& s)
{
    auto* const data = reinterpret_cast<Ch const*>(str::data(s));
    auto const size = str::size(s);

    if (data == nullptr) {
        m_data = nullptr;
    }
    else if (str::at(s, to_index<S>(size)) == null_char_v<Ch>) {
        m_data = string_view_type { data, size };
    }
    else {
        m_data = string_type { data, size };
    }
}

template<character Ch>
template<has_convertible_member_c_str<Ch> S>
    requires (!convertible_character_range<S, Ch>)
basic_cstring_ptr<Ch>::
basic_cstring_ptr(S&& s)
    : m_data { reinterpret_cast<Ch const*>(s.c_str()) }
{}

template<character Ch>
basic_cstring_ptr<Ch>::const_iterator basic_cstring_ptr<Ch>::
begin() const noexcept
{
    if (auto* const o_ptr = std::get_if<Ch const*>(&m_data)) {
        return { *o_ptr, nullptr };
    }
    else if (auto* const o_sv = std::get_if<string_view_type>(&m_data)) {
        return { o_sv->begin(), o_sv->end() };
    }
    else {
        assert(std::holds_alternative<string_type>(m_data));

        auto& s = std::get<string_type>(m_data);

        return { s.data(), s.data() + s.size() };
    }
}

template<character Ch>
basic_cstring_ptr<Ch>::sentinel basic_cstring_ptr<Ch>::
end() const noexcept
{
    return {};
}

template<character Ch>
Ch const* basic_cstring_ptr<Ch>::
data() const noexcept
{
    if (auto* const o_ptr = std::get_if<Ch const*>(&m_data)) {
        return *o_ptr;
    }
    else if (auto* const o_sv = std::get_if<string_view_type>(&m_data)) {
        return o_sv->data();
    }
    else {
        assert(std::holds_alternative<string_type>(m_data));

        auto& s = std::get<string_type>(m_data);

        return s.data();
    }
}

template<character Ch>
size_t basic_cstring_ptr<Ch>::
size() const noexcept
{
    if (auto* const o_ptr = std::get_if<Ch const*>(&m_data)) {
        return str::size(*o_ptr);
    }
    else if (auto* const o_sv = std::get_if<string_view_type>(&m_data)) {
        return o_sv->size();
    }
    else {
        assert(std::holds_alternative<string_type>(m_data));

        auto& s = std::get<string_type>(m_data);

        return s.size();
    }
}

template<character Ch>
basic_cstring_ptr<Ch>::
operator Ch const* () const noexcept
{
    return data();
}

} // namespace stream9::strings

#endif // STRINGS_UTILITY_CSTRING_PTR_HPP
