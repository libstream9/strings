#ifndef STRINGS_CORE_STRING_RANGE_HPP
#define STRINGS_CORE_STRING_RANGE_HPP

#include "../namespace.hpp"

#include "concepts.hpp"
#include "string_traits.hpp"

#include "../accessor/begin.hpp"
#include "../accessor/end.hpp"
#include "../query/size.hpp"

#include <algorithm>
#include <concepts>
#include <ostream>
#include <ranges>

namespace stream9::strings {

namespace detail {

    template<std::input_or_output_iterator I, std::sentinel_for<I> S>
    constexpr rng::subrange_kind
    subrange_kind()
    {
        return std::sized_sentinel_for<S, I> ? rng::subrange_kind::sized
                                             : rng::subrange_kind::unsized;
    }

    template<string S>
    constexpr rng::subrange_kind
    subrange_kind()
    {
        return rng::sized_range<S> ? rng::subrange_kind::sized
                                   : subrange_kind<iterator_t<S>, sentinel_t<S>>();
    }

} // namespace detail

template<std::input_or_output_iterator I, std::sentinel_for<I> S = I,
         rng::subrange_kind K = detail::subrange_kind<I, S>() >
    requires (K == rng::subrange_kind::sized || !std::sized_sentinel_for<S, I>)
          && character<std::iter_value_t<I>>
class string_range : public rng::subrange<I, S, K>
{
    using base_t = rng::subrange<I, S, K>;

    static constexpr bool store_size =
        K == rng::subrange_kind::sized && !std::sized_sentinel_for<S, I>;
public:
    // constructor
    string_range() = default;

    template<string T>
        requires (std::convertible_to<iterator_t<T>, I>
               && std::convertible_to<sentinel_t<T>, S>
               && !store_size )
    string_range(T&& s)
        : base_t { str::begin(s), str::end(s) }
    {}

    template<string T>
        requires (std::convertible_to<iterator_t<T>, I>
               && std::convertible_to<sentinel_t<T>, S>
               && store_size && rng::sized_range<T> )
    string_range(T&& s)
        : base_t { str::begin(s), str::end(s), str::size(s) }
    {}

    template<string T, typename SizeT>
        requires (std::convertible_to<iterator_t<T>, I>
               && std::convertible_to<sentinel_t<T>, S>
               && std::convertible_to<SizeT, std::make_unsigned_t<std::iter_difference_t<I>>>
               && K == rng::subrange_kind::sized )
    string_range(T&& s, SizeT const n)
        : base_t { str::begin(s), str::end(s), n }
    {}

    using base_t::base_t;

    // modifier
    void
    remove_prefix(std::iter_difference_t<I> const n)
        requires std::random_access_iterator<I>
        //[[expects: n >= 0]]
    {
        auto [first, last] = *this;
        rng::advance(first, n, last);

        *this = string_range { first, last };
    }

    void
    remove_suffix(std::iter_difference_t<I> const n)
        requires std::random_access_iterator<I>
        //[[expects: n >= 0]]
    {
        auto [first, last] = *this;
        rng::advance(last, -n, first);

        *this = string_range { first, last };
    }

    // operator
    template<string T>
        requires is_same_char_v<character_t<I>, T>
    bool operator==(T const& other) const
    {
        return rng::equal(str::begin(*this), str::end(*this),
                          str::begin(other), str::end(other) );
    }

    template<string T>
        requires is_same_char_v<character_t<I>, T>
    std::strong_ordering operator<=>(T const& other) const
    {
        return std::lexicographical_compare_three_way(
            str::begin(*this), str::end(*this),
            str::begin(other), str::end(other)
        );
    }

    explicit operator std::basic_string<character_t<I>> () const
    {
        return { this->begin(), this->end() };
    }

    operator std::basic_string_view<character_t<I>> () const
    {
        return { this->begin(), this->end() };
    }

    friend std::ostream&
    operator<<(std::ostream& os, string_range const& s)
    {
        if constexpr (contiguous_string<string_range> && sized_string<string_range>) {
            return os.write(s.data(), static_cast<std::streamsize>(str::size(s)));
        }
        else {
            for (auto const c: s) {
                os.put(c);
            }

            return os;
        }
    }
};

/*
 * deduction guide
 */
template<string S>
string_range(S&&)
    -> string_range<iterator_t<S>, sentinel_t<S>, detail::subrange_kind<S>()>;

template<string S, typename N>
    requires std::convertible_to<N, str_difference_t<S>>
string_range(S&&, N)
    -> string_range<iterator_t<S>, sentinel_t<S>, rng::subrange_kind::sized>;

template<std::input_or_output_iterator I, std::sentinel_for<I> S>
string_range(I, S) -> string_range<I, S>;

template<std::input_or_output_iterator I, std::sentinel_for<I> S, typename N>
    requires std::convertible_to<N, std::iter_difference_t<I>>
string_range(I, S, N) -> string_range<I, S, rng::subrange_kind::sized>;

/*
 * literal
 */
inline string_range<char const*>
operator ""_sr(char const* ptr, size_t const len)
{
    //TODO literal for other char types
    return { ptr, ptr + len };
}

/*
 * decomposition support
 */
template<std::size_t N, typename I, typename S, rng::subrange_kind K>
    requires (N < 2)
constexpr auto
get(string_range<I, S, K> const& s)
{
    if constexpr (N == 0) {
        return s.begin();
    } else {
        return s.end();
    }
}

} // namespace stream9::strings

namespace std {
    template<typename I, typename S, ranges::subrange_kind K>
    struct tuple_size<stream9::strings::string_range<I, S, K>>
      : std::integral_constant<size_t, 2> {};

    template<typename I, typename S, ranges::subrange_kind K>
    struct tuple_element<0, stream9::strings::string_range<I, S, K>>
    {
        using type = I;
    };

    template<typename I, typename S, ranges::subrange_kind K>
    struct tuple_element<1, stream9::strings::string_range<I, S, K>>
    {
        using type = S;
    };
}

#endif // STRINGS_CORE_STRING_RANGE_HPP
