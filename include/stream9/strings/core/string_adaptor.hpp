#ifndef STRINGS_CORE_STRING_ADAPTOR_HPP
#define STRINGS_CORE_STRING_ADAPTOR_HPP

#include "../namespace.hpp"

#include "meta.hpp"
#include "view_interface.hpp"

#include "../detail/view_base.hpp"

#include <ranges>

namespace stream9::strings {

template<rng::view V>
class string_adaptor : public view_interface<string_adaptor<V>>
                     , public V
{
public:
    string_adaptor() = default;

    string_adaptor(V v)
        : V { std::move(v) }
    {}
};

template<typename T>
string_adaptor(T&&) -> string_adaptor<_unref<T>>;

} // namespace stream9::strings

#endif // STRINGS_CORE_STRING_ADAPTOR_HPP
