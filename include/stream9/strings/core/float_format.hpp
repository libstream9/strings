#ifndef STRINGS_CORE_FLOAT_FORMAT_HPP
#define STRINGS_CORE_FLOAT_FORMAT_HPP

namespace stream9::strings {

enum class float_format {
    scientific = 1,
    fixed = 2,
    hex = 4,
    general = fixed | scientific,
};

inline float_format
operator|(float_format const lhs, float_format const rhs)
{
    return static_cast<float_format>(
        static_cast<int>(lhs) | static_cast<int>(rhs) );
}

inline float_format
operator&(float_format const lhs, float_format const rhs)
{
    return static_cast<float_format>(
        static_cast<int>(lhs) & static_cast<int>(rhs) );
}

inline bool
test(float_format const lhs, float_format const rhs)
{
    return static_cast<int>(lhs & rhs);
}

} // namespace stream9::strings

#endif // STRINGS_CORE_FLOAT_FORMAT_HPP
