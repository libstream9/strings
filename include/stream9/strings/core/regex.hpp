#ifndef STRINGS_CORE_REGEX_HPP
#define STRINGS_CORE_REGEX_HPP

#include "concepts.hpp"
#include "string_range.hpp"
#include "string_traits.hpp"

#include <concepts>
#include <functional>
#include <string>

namespace stream9::strings {

template<typename> struct regex_adaptor;

template<typename T>
struct regex_adaptor<std::reference_wrapper<T>>
    : public regex_adaptor<_uncv<T>>
{};

namespace _regex {

    template<typename R>
    using string_t = std::basic_string<
                        typename regex_adaptor<_uncvref<R>>::char_type >;

    template<typename R>
    concept regexp = requires (R e) {
        typename regex_adaptor<_uncvref<R>>::char_type;

        { regex_adaptor<_uncvref<R>>::find(string_t<R>(), e) }
            -> std::same_as<string_range<iterator_t<string_t<R>>>>;
    };

} // namespace _regex

using _regex::regexp;

template<regexp T>
    requires (!string<T>)
struct character_type<T>
{
    using type = typename regex_adaptor<_uncvref<T>>::char_type;
};

} // namespace stream9::strings

#endif // STRINGS_CORE_REGEX_HPP
