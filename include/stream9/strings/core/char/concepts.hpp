#ifndef STRINGS_CORE_CHAR_CONCEPTS_HPP
#define STRINGS_CORE_CHAR_CONCEPTS_HPP

#include "../../namespace.hpp"

#include "../meta.hpp"

#include <concepts>
#include <iterator>
#include <ranges>
#include <type_traits>

namespace stream9::strings {

template<typename T>
concept character =
             std::same_as<_uncvref<T>, char>
          || std::same_as<_uncvref<T>, signed char>
          || std::same_as<_uncvref<T>, unsigned char>
          || std::same_as<_uncvref<T>, wchar_t>
          || std::same_as<_uncvref<T>, char8_t> //TODO test char8_t
          || std::same_as<_uncvref<T>, char16_t>
          || std::same_as<_uncvref<T>, char32_t>
          ;

template<typename S>
concept character_pointer =
       std::is_pointer_v<_unref<S>>
    && character<std::remove_pointer_t<_unref<S>>>;

template<typename S>
concept character_array =
       std::is_array_v<_unref<S>>
    && std::rank_v<_unref<S>> == 1
    && character<std::remove_extent_t<_unref<S>>>;

template<typename S>
concept character_range =
       !std::is_array_v<_unref<S>>
    && rng::range<_unref<S>>
    && character<std::iter_value_t<rng::iterator_t<_unref<S>>>>;

/*
 * null_char
 */
template<typename C> struct null_char;

template<typename C>
    requires std::same_as<C, char>
          || std::same_as<C, signed char>
          || std::same_as<C, unsigned char>
struct null_char<C> : std::integral_constant<C, '\0'> {};

template<>
struct null_char<char8_t> : std::integral_constant<char8_t, u8'\0'> {};

template<>
struct null_char<wchar_t> : std::integral_constant<wchar_t, L'\0'> {};

template<>
struct null_char<char16_t> : std::integral_constant<char16_t, u'\0'> {};

template<>
struct null_char<char32_t> : std::integral_constant<char32_t, U'\0'> {};

template<typename C>
constexpr auto null_char_v = null_char<_uncvref<C>>::value;

} // namespace stream9::strings

#endif // STRINGS_CORE_CHAR_CONCEPTS_HPP
