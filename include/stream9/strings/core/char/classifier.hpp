#ifndef STRINGS_CORE_CHAR_CLASSIFIER_HPP
#define STRINGS_CORE_CHAR_CLASSIFIER_HPP

#include "../../namespace.hpp"

#include "../concepts.hpp"
#include "../string_range.hpp"
#include "../string_traits.hpp"

#include <cassert>
#include <concepts>
#include <locale>
#include <ranges>

namespace stream9::strings {

template<typename Fn, typename C>
concept classifier = character<C> && std::predicate<Fn, C>;

/*
 * classifiers
 */
namespace detail {

    template<typename C, std::ctype_base::mask mask>
    class classifier_base
    {
    public:
        classifier_base(std::locale const& loc = {})
            : m_facet { &std::use_facet<std::ctype<C>>(loc) }
        {
            assert(m_facet);
        }

        bool
        operator()(C const c) const
        {
            return m_facet->is(mask, c);
        }

    private:
        std::ctype<C> const* m_facet; // non-null
    };

} // namespace detail

template<character C>
class is_space : public detail::classifier_base<C, std::ctype_base::space>
{
    using base_t = detail::classifier_base<C, std::ctype_base::space>;
public:
    using base_t::base_t;
    using base_t::operator=;
};

template<character C>
class is_alnum : public detail::classifier_base<C, std::ctype_base::alnum>
{
    using base_t = detail::classifier_base<C, std::ctype_base::alnum>;
public:
    using base_t::base_t;
    using base_t::operator=;
};

template<character C>
class is_alpha : public detail::classifier_base<C, std::ctype_base::alpha>
{
    using base_t = detail::classifier_base<C, std::ctype_base::alpha>;
public:
    using base_t::base_t;
    using base_t::operator=;
};

template<character C>
class is_cntrl : public detail::classifier_base<C, std::ctype_base::cntrl>
{
    using base_t = detail::classifier_base<C, std::ctype_base::cntrl>;
public:
    using base_t::base_t;
    using base_t::operator=;
};

template<character C>
class is_digit : public detail::classifier_base<C, std::ctype_base::digit>
{
    using base_t = detail::classifier_base<C, std::ctype_base::digit>;
public:
    using base_t::base_t;
    using base_t::operator=;
};

template<character C>
class is_graph : public detail::classifier_base<C, std::ctype_base::graph>
{
    using base_t = detail::classifier_base<C, std::ctype_base::graph>;
public:
    using base_t::base_t;
    using base_t::operator=;
};

template<character C>
class is_lower : public detail::classifier_base<C, std::ctype_base::lower>
{
    using base_t = detail::classifier_base<C, std::ctype_base::lower>;
public:
    using base_t::base_t;
    using base_t::operator=;
};

template<character C>
class is_print : public detail::classifier_base<C, std::ctype_base::print>
{
    using base_t = detail::classifier_base<C, std::ctype_base::print>;
public:
    using base_t::base_t;
    using base_t::operator=;
};

template<character C>
class is_punct : public detail::classifier_base<C, std::ctype_base::punct>
{
    using base_t = detail::classifier_base<C, std::ctype_base::punct>;
public:
    using base_t::base_t;
    using base_t::operator=;
};

template<character C>
class is_upper : public detail::classifier_base<C, std::ctype_base::upper>
{
    using base_t = detail::classifier_base<C, std::ctype_base::upper>;
public:
    using base_t::base_t;
    using base_t::operator=;
};

template<character C>
class is_xdigit : public detail::classifier_base<C, std::ctype_base::xdigit>
{
    using base_t = detail::classifier_base<C, std::ctype_base::xdigit>;
public:
    using base_t::base_t;
    using base_t::operator=;
};

template<character C>
class is_blank : public detail::classifier_base<C, std::ctype_base::blank>
{
    using base_t = detail::classifier_base<C, std::ctype_base::blank>;
public:
    using base_t::base_t;
    using base_t::operator=;
};

//TODO boost::algorithm::is_from_range

template<typename C>
class is_classified
{
public:
    is_classified(std::locale const& loc = {})
        : m_facet { &std::use_facet<std::ctype<C>>(loc) }
    {
        assert(m_facet);
    }


    bool
    operator()(std::ctype_base::mask const mask, C const c) const
    {
        return m_facet->is(mask, c);
    }

    private:
        std::ctype<C> const* m_facet; // non-null
};

template<forward_string S>
class is_any_of
{
public:
    template<forward_string T>
        requires std::same_as<_unref<T>, _unref<S>>
    is_any_of(T&& s)
        : m_chars { s }
    {}

    bool
    operator()(str_char_t<S> const c) const
    {
        return rng::find(m_chars, c) != m_chars.end();
    }

private:
    string_range<iterator_t<S>> m_chars;
};

template<forward_string S>
is_any_of(S&&) -> is_any_of<_unref<S>>;

} // namespace stream9::strings

#endif // STRINGS_CORE_CHAR_CLASSIFIER_HPP
