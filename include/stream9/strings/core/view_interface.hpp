#ifndef STREAM9_STRINGS_CORE_VIEW_INTERFACE_HPP
#define STREAM9_STRINGS_CORE_VIEW_INTERFACE_HPP

#include "../namespace.hpp"

#include "concepts.hpp"
#include "string_traits.hpp"

#include "../accessor/begin.hpp"
#include "../accessor/end.hpp"
#include "../query/size.hpp"

#include <algorithm>
#include <compare>
#include <concepts>
#include <ostream>
#include <ranges>
#include <type_traits>

namespace stream9::strings {

template<typename Derived>
    requires std::is_class_v<Derived>
          && std::same_as<Derived, std::remove_cv_t<Derived>>
class view_interface
{
public:
    template<string S>
        requires is_same_char_v<character_t<Derived>, S>
    bool operator==(S const& other) const
    {
        return rng::equal(str::begin(derived()), str::end(derived()),
                          str::begin(other), str::end(other) );
    }

    template<string S>
        requires is_same_char_v<character_t<Derived>, S>
    std::strong_ordering operator<=>(S const& other) const
    {
        return std::lexicographical_compare_three_way(
            str::begin(derived()), str::end(derived()),
            str::begin(other), str::end(other)
        );
    }

private:
    Derived& derived()
    {
        return static_cast<Derived&>(*this);
    }

    Derived const& derived() const
    {
        return static_cast<Derived const&>(*this);
    }
};

template<typename S>
    requires std::derived_from<_uncvref<S>, view_interface<_uncvref<S>>>
inline std::ostream&
operator<<(std::ostream& os, S const& s)
{
    if constexpr (contiguous_string<S> && sized_string<S>) {
        return os.write(s.data(), static_cast<std::streamsize>(str::size(s)));
    }
    else {
        for (auto const c: s) {
            os.put(c);
        }

        return os;
    }
}

} // namespace stream9::strings

#endif // STREAM9_STRINGS_CORE_VIEW_INTERFACE_HPP
