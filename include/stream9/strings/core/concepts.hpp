#ifndef STRINGS_CORE_CONCEPTS_HPP
#define STRINGS_CORE_CONCEPTS_HPP

#include "../namespace.hpp"

#include "meta.hpp"

#include "char/concepts.hpp"

#include "../query/size.hpp"

#include <concepts>
#include <iterator>

namespace stream9::strings {

namespace detail {

    template<typename> struct iterator;

    template<rng::range S>
    struct iterator<S>
    {
        using type = rng::iterator_t<S>;
    };

    template<character_pointer S>
    struct iterator<S>
    {
        using type = _uncvref<S>;
    };

    template<typename> struct sentinel;

    template<rng::range S>
    struct sentinel<S>
    {
        using type = rng::sentinel_t<S>;
    };

    template<character_pointer S>
    struct sentinel<S>
    {
        using type = _uncvref<S>;
    };

} // namespace detail

template<typename S>
using iterator_t = typename detail::iterator<S>::type;

template<typename S>
using sentinel_t = typename detail::sentinel<S>::type;

/*
 * string concepts
 */
template<typename S>
concept string =
    character_pointer<S> || character_array<S> || character_range<S>;

/*
 * common refinement
 */
template<typename T>
concept readable_string = string<T> && std::input_iterator<iterator_t<T>>;

template<typename T>
concept writable_string = string<T> &&
             std::output_iterator<iterator_t<T>, std::iter_value_t<iterator_t<T>>>;

template<typename T>
concept forward_string =
            readable_string<T> && std::forward_iterator<iterator_t<T>>;

template<typename T>
concept bidirectional_string =
            forward_string<T> && std::bidirectional_iterator<iterator_t<T>>;

template<typename T>
concept random_access_string =
            bidirectional_string<T> && std::random_access_iterator<iterator_t<T>>;

template<typename T>
concept contiguous_string =
            random_access_string<T> && std::contiguous_iterator<iterator_t<T>>;

template<typename T>
concept sized_string = string<T> &&
    requires (T&& s) {
        str::size(s);
    };

} // namespace stream9::strings

#endif // STRINGS_CORE_CONCEPTS_HPP
