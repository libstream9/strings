#ifndef STREAM9_STRINGS_NAMESPACE_HPP
#define STREAM9_STRINGS_NAMESPACE_HPP

namespace std::ranges {}
namespace std::ranges::views {}
namespace stream9::errors {}
namespace stream9::ranges {}
namespace stream9::ranges::views {}
namespace stream9::container {}
namespace stream9::iterators {}

namespace stream9::strings {

namespace con = stream9::container;
namespace err = stream9::errors;
namespace rng { using namespace std::ranges; }
namespace rng { using namespace stream9::ranges; }
namespace rng::views { using namespace std::ranges::views; }
namespace rng::views { using namespace stream9::ranges::views; }
namespace str = strings;
namespace iter { using namespace stream9::iterators; }

} // namespace stream9::strings

#endif // STREAM9_STRINGS_NAMESPACE_HPP
