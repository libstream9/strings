#ifndef STRINGS_MODIFIER_FIND_REPLACE_HPP
#define STRINGS_MODIFIER_FIND_REPLACE_HPP

#include "../core/concepts.hpp"
#include "../core/finder.hpp"
#include "../core/regex.hpp"
#include "../core/string_range.hpp"
#include "../core/string_traits.hpp"

#include "../query/find_first.hpp"

#include "replace.hpp"

#include <type_traits>

namespace stream9::strings {

namespace str = strings;

/*
 * find_replace
 */
namespace _find_replace {

    template<typename S1, typename S2>
    concept can_invoke_find_first_with_str =
        requires (S1&& s1, S2&& s2) {
            str::find_first(s1, s2);
        };

    template<typename S, typename R>
    concept can_invoke_find_first_with_regexp =
        requires (S&& s, R&& re) {
            str::find_first(s, re);
        };

    template<typename S, typename F>
    concept can_invoke_find_first_with_finder =
        requires (S&& s, F&& f) {
            str::find_first(s, f);
        };

    template<typename S1, typename S2>
    concept can_invoke_replace =
        requires (S1&& s1, S2&& s2, iterator_t<S1> it) {
            str::replace(s1, it, it, s2);
        };

    struct api
    {
        template<forward_string S,
                 forward_string KeywordT, forward_string ReplacementT>
            requires is_same_char_v<S, KeywordT, ReplacementT>
                  && can_invoke_find_first_with_str<S, KeywordT>
                  && can_invoke_replace<S, ReplacementT>
        void
        operator()(S& s1, KeywordT&& s2, ReplacementT&& s3) const
        {
            auto const range = str::find_first(s1, s2);

            if (!range.empty()) {
                str::replace(s1, range.begin(), range.end(), s3);
            }
        }

        template<forward_string S, typename FinderT, forward_string ReplacementT>
            requires is_same_char_v<S, ReplacementT>
                  && finder<FinderT, S>
                  && can_invoke_find_first_with_finder<S, FinderT>
                  && can_invoke_replace<S, ReplacementT>
        void
        operator()(S& s1, FinderT&& finder, ReplacementT&& s2) const
        {
            auto const range = finder(s1);

            if (!range.empty()) {
                str::replace(s1, range.begin(), range.end(), s2);
            }
        }

        template<bidirectional_string S, regexp R, forward_string ReplacementT>
            requires is_same_char_v<S, R, ReplacementT>
                  && can_invoke_find_first_with_regexp<S, R>
                  && can_invoke_replace<S, ReplacementT>
        void
        operator()(S& s1, R&& e, ReplacementT&& s2) const
        {
            auto const range = str::find_first(s1, e);

            if (!range.empty()) {
                str::replace(s1, range.begin(), range.end(), s2);
            }
        }
    };

} // namespace _find_replace

inline constexpr _find_replace::api find_replace;

} // namespace stream9::strings

#endif // STRINGS_MODIFIER_FIND_REPLACE_HPP
