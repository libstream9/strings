#ifndef STRINGS_MODIFIER_TRIM_LEFT_HPP
#define STRINGS_MODIFIER_TRIM_LEFT_HPP

#include "../core/char/classifier.hpp"
#include "../core/concepts.hpp"
#include "../core/string_traits.hpp"

#include "remove_prefix.hpp"

#include <algorithm>
#include <concepts>
#include <locale>

namespace stream9::strings {

namespace _trim_left {

    namespace rng = std::ranges;
    namespace str = strings;

    template<typename S, typename I>
    concept has_remove_prefix =
        requires (S s, I i) {
            str::remove_prefix(s, i);
        };

    struct api
    {
        template<forward_string S>
        void
        operator()(S& input, std::locale const& loc = {}) const
        {
            operator()(input, is_space<str_char_t<S>>(loc));
        }

        template<forward_string S, typename PredicateT>
            requires std::predicate<PredicateT, str_char_t<S>>
                  && has_remove_prefix<S, iterator_t<S>>
        void
        operator()(S& s, PredicateT pred) const
        {
            auto const it = rng::find_if_not(s, pred);

            str::remove_prefix(s, it);
        }
    };

} // namespace _trim_left

inline constexpr _trim_left::api trim_left;

} // namespace stream9::strings

#endif // STRINGS_MODIFIER_TRIM_LEFT_HPP
