#ifndef STRINGS_MODIFIER_FIND_REPLACE_ALL_HPP
#define STRINGS_MODIFIER_FIND_REPLACE_ALL_HPP

#include "../core/concepts.hpp"
#include "../core/regex.hpp"
#include "../core/string_range.hpp"
#include "../core/string_traits.hpp"

#include "../accessor/end.hpp"
#include "../finder/first_finder.hpp"
#include "../finder/regex_finder.hpp"
#include "../query/size.hpp"

#include "replace.hpp"

#include <functional>
#include <iterator>
#include <type_traits>

namespace stream9::strings {

namespace _find_replace_all {

    namespace rng = std::ranges;
    namespace str = strings;

    template<typename S1, typename S2>
    concept can_invoke_replace =
        requires (S1&& s1, S2&& s2, iterator_t<S1> it) {
            str::replace(s1, it, it, s2);
        };

    struct api
    {
        template<forward_string S, forward_string KeywordT, forward_string ReplacementT>
            requires is_same_char_v<S, KeywordT, ReplacementT>
                  && can_invoke_replace<S, ReplacementT>
        void
        operator()(S& s1, KeywordT&& s2, ReplacementT&& s3) const
        {
            first_finder find { s2 };
            operator()(s1, find, s3);
        }

        template<bidirectional_string S, regexp R, forward_string ReplacementT>
            requires is_same_char_v<S, R, ReplacementT>
                  && can_invoke_replace<S, ReplacementT>
        void
        operator()(S& s1, R&& e, ReplacementT&& s2) const
        {
            regex_finder find { std::cref(e) };
            operator()(s1, find, s2);
        }

        template<forward_string S, typename FinderT, forward_string ReplacementT>
            requires is_same_char_v<S, ReplacementT>
                  && finder<FinderT, S>
                  && can_invoke_replace<S, ReplacementT>
        void
        operator()(S& s1, FinderT&& find, ReplacementT&& s2) const
        {
            string_range r1 = s1;
            auto const n = to_difference<S>(str::size(s2));

            while (!r1.empty()) {
                auto const& match = find(r1);
                if (!match) return;

                auto const it =
                    str::replace(s1, match.begin(), match.end(), s2);

                r1 = string_range(rng::next(it, n), str::end(s1));
            }
        }
    };

} // namespace _find_replace_all

inline constexpr _find_replace_all::api find_replace_all;

} // namespace stream9::strings

#endif // STRINGS_MODIFIER_FIND_REPLACE_ALL_HPP
