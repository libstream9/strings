#ifndef STRINGS_MODIFIER_APPEND_HPP
#define STRINGS_MODIFIER_APPEND_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"

#include "../accessor/end.hpp"
#include "../modifier/insert.hpp"

namespace stream9::strings {

namespace _append {

    template<typename S, typename C>
    concept can_invoke_insert_with_ch =
        requires (S&& s, C c) {
            str::insert(s, str::end(s), c);
        };

    template<typename S1, typename S2>
    concept can_invoke_insert_with_str =
        requires (S1&& s1, S2&& s2) {
            str::insert(s1, str::end(s1), s2);
        };

    struct api
    {
        template<string S, character C>
            requires is_same_char_v<S, C>
                  && can_invoke_insert_with_ch<S, C>
        void
        operator()(S& s, C const c) const
        {
            str::insert(s, str::end(s), c);
        }

        template<string S1, string S2>
            requires is_same_char_v<S1, S2>
                  && can_invoke_insert_with_str<S1, S2>
        void
        operator()(S1& s1, S2&& s2) const
        {
            str::insert(s1, str::end(s1), s2);
        }
    };

} // namespace _append

inline constexpr _append::api append;

} // namespace stream9::strings

#endif // STRINGS_MODIFIER_APPEND_HPP
