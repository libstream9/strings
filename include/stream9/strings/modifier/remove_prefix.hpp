#ifndef STRINGS_MODIFIER_REMOVE_PREFIX_HPP
#define STRINGS_MODIFIER_REMOVE_PREFIX_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"
#include "../core/string_traits.hpp"

#include "../accessor/begin.hpp"
#include "../accessor/end.hpp"
#include "../query/iterator_at.hpp"

#include "erase.hpp"

#include <concepts>
#include <ranges>

namespace stream9::strings {

namespace _remove_prefix {

    template<typename S>
    concept has_member =
        requires (S&& s) {
            s.remove_prefix(0);
        };

    template<typename S>
    concept can_invoke_erase_with_index_range =
        requires (S&& s) {
            str::erase(s, 0, 0);
        };

    template<typename S>
    concept can_invoke_erase_with_iter_range =
        requires (S&& s) {
            str::erase(s, str::begin(s), str::begin(s));
        };

    template<typename S, std::integral I>
        requires has_member<S>
              || can_invoke_erase_with_index_range<S>
              || can_invoke_erase_with_iter_range<S>
    void
    remove_prefix_with_length(S& s, I const n)
    {
        if constexpr (has_member<S>) {
            s.remove_prefix(n);
        }
        else if constexpr (can_invoke_erase_with_index_range<S>) {
            str::erase(s, 0u, n);
        }
        else {
            static_assert(can_invoke_erase_with_iter_range<S>);

            auto const first = str::begin(s);
            auto const last = iterator_at(s, n);

            str::erase(s, first, last);
        }
    }

    template<typename S>
        requires has_member<S>
              || can_invoke_erase_with_iter_range<S>
    void
    remove_prefix_with_iter(S& s, iterator_t<S const> it)
    {
        if constexpr (has_member<S>) {
            s.remove_prefix(to_size<S>(std::distance(str::begin(s), it)));
        }
        else {
            static_assert(can_invoke_erase_with_iter_range<S>);

            str::erase(s, str::begin(s), it);
        }
    }

    struct api
    {
        template<string S, std::integral I> // delay conversion of I as long as possible so member function can have flexibility
            requires std::convertible_to<I, str_size_t<S>>
                  && (   has_member<S>
                      || can_invoke_erase_with_index_range<S>
                      || can_invoke_erase_with_iter_range<S>)
        void
        operator()(S& s, I n) const
        {
            remove_prefix_with_length(s, n);
        }

        template<string S>
            requires has_member<S>
                  || can_invoke_erase_with_iter_range<S>
        void
        operator()(S& s, iterator_t<S const> it) const
        {
            remove_prefix_with_iter(s, it);
        }
    };

} // namespace _remove_prefix

inline constexpr _remove_prefix::api remove_prefix;

} // namespace stream9::strings

#endif // STRINGS_MODIFIER_REMOVE_PREFIX_HPP
