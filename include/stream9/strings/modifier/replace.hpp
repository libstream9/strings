#ifndef STRINGS_MODIFIER_REPLACE_HPP
#define STRINGS_MODIFIER_REPLACE_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"
#include "../core/string_traits.hpp"

#include "../accessor/begin.hpp"
#include "../accessor/end.hpp"
#include "../query/index_at.hpp"
#include "../query/iterator_at.hpp"

#include "erase.hpp"
#include "insert.hpp"

#include <concepts>
#include <iterator>

namespace stream9::strings {

namespace _replace {

    template<typename S1, typename S2>
    concept has_member_by_iter1 =
        requires (S1 s1, iterator_t<S1> it, S2 s2) {
            { s1.replace(it, it, s2) } -> std::same_as<iterator_t<S1>>;
        };

    template<typename S1, typename S2>
    concept has_non_member_by_iter1 =
        requires (S1 s1, iterator_t<S1> it, S2 s2) {
            { replace(s1, it, it, s2) } -> std::same_as<iterator_t<S1>>;
        };

    template<typename S1, typename S2>
    concept has_member_by_iter2 =
        random_access_string<S1> &&
        requires (S1 s1, iterator_t<S1> it, S2 s2) {
            s1.replace(it, it, s2);
        };

    template<typename S1, typename S2>
    concept has_non_member_by_iter2 =
        random_access_string<S1> &&
        requires (S1 s1, iterator_t<S1> it, S2 s2) { replace(s1, it, it, s2); };

    template<typename S1, typename S2>
    concept has_member_by_idx =
        random_access_string<S1> &&
        requires (S1 s1, S2 s2) { s1.replace(0, 0, s2); };

    template<typename S1, typename S2>
    concept has_non_member_by_idx =
        random_access_string<S1> &&
        requires (S1 s1, S2 s2) { replace(s1, 0, 0, s2); };

    template<typename S1, typename S2>
    concept can_invoke_erase_and_insert_by_iter_range =
        requires (S1 s1, iterator_t<S1> it, S2 s2) {
            str::erase(s1, it, it);
            str::insert(s1, it, s2);
        };

    template<typename S1, typename S2>
    concept can_invoke_erase_and_insert_by_index_range =
        random_access_string<S1> &&
        requires (S1 s1, S2 s2) {
            str::erase(s1, 0, 0);
            str::insert(s1, 0, s2);
        };

    template<typename S1, typename S2>
    concept has_replace_function =
           has_member_by_iter1<S1, S2>
        || has_non_member_by_iter1<S1, S2>
        || has_member_by_iter2<S1, S2>
        || has_non_member_by_iter2<S1, S2>
        || has_member_by_idx<S1, S2>
        || has_non_member_by_idx<S1, S2>;

    template<typename S1, typename S2>
    concept can_invoke_erase_and_insert =
           can_invoke_erase_and_insert_by_iter_range<S1, S2>
        || can_invoke_erase_and_insert_by_index_range<S1, S2>;

    template<typename S1, typename IteratorT, typename SentinelT, typename S2>
        requires has_replace_function<S1, S2>
              || can_invoke_erase_and_insert<S1, S2>
    iterator_t<S1>
    replace_iter_range_with_str(S1& s1,
        IteratorT const first, SentinelT const last, S2&& s2)
    {
        if constexpr (has_member_by_iter1<S1, S2>) {
            return s1.replace(first, last, s2);
        }
        else if constexpr (has_member_by_iter2<S1, S2>) {
            auto const idx = str::index_at(s1, first);

            s1.replace(first, last, s2);

            return str::iterator_at(s1, idx);
        }
        else if constexpr (has_member_by_idx<S1, S2>) {
            auto const idx = str::index_at(s1, first);
            auto const n = std::distance(first, last);

            s1.replace(idx, n, s2);

            return str::iterator_at(s1, idx);
        }
        else if constexpr (has_non_member_by_iter1<S1, S2>) {
            return replace(s1, first, last, s2);
        }
        else if constexpr (has_non_member_by_iter2<S1, S2>) {
            auto const idx = str::index_at(s1, first);

            replace(s1, first, last, s2);

            return str::iterator_at(s1, idx);
        }
        else if constexpr (has_non_member_by_idx<S1, S2>) {
            auto const idx = str::index_at(s1, first);
            auto const n = std::distance(first, last);

            replace(s1, idx, n, s2);

            return str::iterator_at(s1, idx);
        }
        else if constexpr (can_invoke_erase_and_insert_by_iter_range<S1, S2>) {
            auto it = str::erase(s1, first, last);
            it = str::insert(s1, it, s2);

            return it;
        }
        else {
            static_assert(can_invoke_erase_and_insert_by_index_range<S1, S2>);

            auto const idx = str::index_at(s1, first);
            auto const n = std::distance(first, last);

            str::erase(s1, idx, n);
            str::insert(s1, idx, s2);

            return str::iterator_at(s1, idx);
        }
    }

    template<typename S1, typename IndexT, typename SizeT, typename S2>
        requires has_replace_function<S1, S2>
              || can_invoke_erase_and_insert<S1, S2>
    void
    replace_index_range_with_str(S1& s1,
               IndexT const pos, SizeT const n, S2&& s2)
    {
        if constexpr (has_member_by_iter1<S1, S2> ||
                      has_member_by_iter2<S1, S2>)
        {
            auto const first = str::iterator_at(s1, pos);
            auto const last = std::next(first, to_difference<S1>(n));

            s1.replace(first, last, s2);
        }
        else if constexpr (has_non_member_by_iter1<S1, S2> ||
                           has_non_member_by_iter2<S1, S2>)
        {
            auto const first = str::iterator_at(s1, pos);
            auto const last = std::next(first, to_difference<S1>(n));

            replace(s1, first, last, s2);
        }
        else if constexpr (has_member_by_idx<S1, S2>) {
            s1.replace(pos, n, s2);
        }
        else if constexpr (has_non_member_by_idx<S1, S2>) {
            replace(s1, pos, n, s2);
        }
        else if constexpr (can_invoke_erase_and_insert_by_iter_range<S1, S2>) {
            auto const first = str::iterator_at(s1, pos);
            auto const last = std::next(first, to_difference<S1>(n));

            auto const it = str::erase(s1, first, last);
            str::insert(s1, it, s2);
        }
        else {
            static_assert(can_invoke_erase_and_insert_by_index_range<S1, S2>);

            str::erase(s1, pos, n);
            str::insert(s1, pos, s2);
        }
    }

    struct api
    {
        template<string S1, std::input_iterator I, string S2>
            requires is_same_char_v<S1, S2>
                  && std::convertible_to<I, iterator_t<S1 const>>
                  && (   has_replace_function<S1, S2>
                      || can_invoke_erase_and_insert<S1, S2>)
        iterator_t<S1>
        operator()(S1& s1, I first, rng::sentinel_t<S1 const> last, S2&& s2) const
        {
            return replace_iter_range_with_str(s1, first, last, s2);
        }

        template<random_access_string S1, string S2>
            requires is_same_char_v<S1, S2>
                  && (   has_replace_function<S1, S2>
                      || can_invoke_erase_and_insert<S1, S2>)
        void
        operator()(S1& s1, str_index_t<S1> pos, str_size_t<S1> n, S2&& s2) const
        {
            replace_index_range_with_str(s1, pos, n, s2);
        }
    };

} // namespace _replace

inline constexpr _replace::api replace;

} // namespace stream9::strings

#endif // STRINGS_MODIFIER_REPLACE_HPP
