#ifndef STRINGS_MODIFIER_FIND_ERASE_ALL_HPP
#define STRINGS_MODIFIER_FIND_ERASE_ALL_HPP

#include "../core/concepts.hpp"
#include "../core/regex.hpp"
#include "../core/string_range.hpp"

#include "../accessor/end.hpp"
#include "../finder/first_finder.hpp"
#include "../finder/regex_finder.hpp"
#include "../modifier/erase.hpp"

#include <functional>
#include <type_traits>

namespace stream9::strings {

namespace _find_erase_all {

    template<typename S>
    concept can_invoke_erase =
        requires (S&& s) {
            str::erase(s, str::begin(s), str::end(s));
        };

    struct api
    {
        template<forward_string S, forward_string KeywordT>
            requires is_same_char_v<S, KeywordT>
                  && can_invoke_erase<S>
        void
        operator()(S& s1, KeywordT&& s2) const
        {
            first_finder find { s2 };
            operator()(s1, find);
        }

        template<bidirectional_string S, regexp R>
            requires is_same_char_v<S, R>
                  && can_invoke_erase<S>
        void
        operator()(S& s, R&& e) const
        {
            regex_finder find { std::cref(e) };
            operator()(s, find);
        }

        template<forward_string S, typename FinderT>
            requires finder<FinderT, S>
                  && can_invoke_erase<S>
        void
        operator()(S& s, FinderT&& find) const
        {
            string_range r = s;

            while (!r.empty()) {
                auto const& match = find(r);
                if (!match) return;

                auto const it = str::erase(s, match.begin(), match.end());

                r = string_range(it, str::end(s));
            }
        }
    };

} // namespace _find_erase_all

inline constexpr _find_erase_all::api find_erase_all;

} // namespace stream9::strings

#endif // STRINGS_MODIFIER_FIND_ERASE_ALL_HPP
