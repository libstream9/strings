#ifndef STRINGS_MODIFIER_TRIM_HPP
#define STRINGS_MODIFIER_TRIM_HPP

#include "../core/char/classifier.hpp"
#include "../core/concepts.hpp"
#include "../core/string_traits.hpp"

#include "trim_left.hpp"
#include "trim_right.hpp"

#include <concepts>
#include <locale>

namespace stream9::strings {

namespace _trim {

    namespace str = strings;

    struct api
    {
        template<forward_string S, typename PredicateT>
            requires std::predicate<PredicateT, str_char_t<S>>
        void
        operator()(S& s, PredicateT pred) const
        {
            str::trim_left(s, pred);
            str::trim_right(s, pred);
        }

        template<forward_string S>
        void
        operator()(S& s, std::locale const& loc = {}) const
        {
            operator()(s, is_space<str_char_t<S>>(loc));
        }
    };

} // namespace _trim

inline constexpr _trim::api trim;

} // namespace stream9::strings

#endif // STRINGS_MODIFIER_TRIM_HPP
