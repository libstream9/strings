#ifndef STRINGS_MODIFIER_CLEAR_HPP
#define STRINGS_MODIFIER_CLEAR_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"

#include "erase.hpp"

#include "../accessor/begin.hpp"
#include "../accessor/end.hpp"

//TODO move to ranges library

namespace stream9::strings {

namespace _clear {

    //TODO call member or non member clear if it is available

    template<typename S>
    concept can_invoke_erase_with_iter_range =
        requires (S&& s) {
            str::erase(s, str::begin(s), str::end(s));
        };

    struct api
    {
        template<string S>
            requires can_invoke_erase_with_iter_range<S>
        void
        operator()(S& s) const
        {
            str::erase(s, str::begin(s), str::end(s));
        }
    };

} // namespace _clear

inline constexpr _clear::api clear;

} // namespace stream9::strings

#endif // STRINGS_MODIFIER_CLEAR_HPP
