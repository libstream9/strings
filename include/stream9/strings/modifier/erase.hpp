#ifndef STRINGS_MODIFIER_ERASE_HPP
#define STRINGS_MODIFIER_ERASE_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"
#include "../core/string_traits.hpp"

#include "../accessor/begin.hpp"
#include "../query/index_at.hpp"
#include "../query/iterator_at.hpp"

#include <concepts>
#include <iterator>

namespace stream9::strings {

namespace _erase {

    /*
     * concepts
     */
    template<typename S>
    concept has_member_idx =
        requires (S&& s, str_index_t<S> pos, str_size_t<S> n) {
            s.erase(pos, n);
        };

    template<typename S>
    concept has_non_member_idx =
        requires (S&& s, str_index_t<S> pos, str_size_t<S> n) {
            erase(s, pos, n);
        };

    template<typename S>
    concept has_member_iter1 =
        requires (S&& s, iterator_t<S> pos) {
            s.erase(pos);
        };

    template<typename S>
    concept has_non_member_iter1 =
        requires (S&& s, iterator_t<S> pos) {
            erase(s, pos);
        };

    template<typename S>
    concept has_member_iter2 =
        requires (S&& s, iterator_t<S> it) {
            s.erase(it, it);
        };

    template<typename S>
    concept has_non_member_iter2 =
        requires (S&& s, iterator_t<S> it) {
            erase(s, it, it);
        };

    template<typename S>
    concept has_erase_range_function =
           has_member_idx<S>
        || has_non_member_idx<S>
        || has_member_iter2<S>
        || has_non_member_iter2<S>;

    template<typename S>
    concept has_erase_single_function =
           has_member_iter1<S>
        || has_non_member_iter1<S>;

    /*
     * private functions
     */
    template<typename S, typename IndexT, typename SizeT>
        requires has_erase_range_function<S>
    void
    erase_by_index(S& s, IndexT const pos, SizeT const n)
    {
        if constexpr (has_member_idx<S>) {
            s.erase(to_size<S>(pos), n);
        }
        else if constexpr (has_member_iter2<S>) {
            auto const it = str::iterator_at(s, pos);
            s.erase(it, std::next(it, to_difference<S>(n)));
        }
        else if constexpr (has_non_member_idx<S>) {
            erase(s, pos, n);
        }
        else {
            static_assert(has_non_member_iter2<S>);

            auto const it = str::iterator_at(s, pos);
            erase(s, it, std::next(it, to_difference<S>(n)));
        }
    }

    template<typename S, typename IteratorT>
        requires has_erase_single_function<S>
              || has_erase_range_function<S>
    iterator_t<S>
    erase_by_iter(S& s, IteratorT const pos)
    {
        if constexpr (has_member_iter1<S>) {
            return s.erase(pos);
        }
        else if constexpr (has_member_iter2<S>) {
            return s.erase(pos, std::next(pos));
        }
        else if constexpr (has_member_idx<S>) {
            return s.erase(str::index_at(pos), to_size<S>(1));
        }
        else if constexpr (has_non_member_iter1<S>) {
            return erase(s, pos);
        }
        else if constexpr (has_non_member_iter2<S>) {
            return erase(s, pos, std::next(pos));
        }
        else {
            static_assert(has_non_member_idx<S>);

            return erase(s, str::index_at(pos), to_size<S>(1));
        }
    }

    template<typename S, typename IteratorT, typename SentinelT>
        requires has_erase_range_function<S>
    iterator_t<S>
    erase_by_iter_pair(S& s, IteratorT const first, SentinelT const last)
    {
        if constexpr (has_member_iter2<S>) {
            return s.erase(first, last);
        }
        else if constexpr (has_member_idx<S>) {
            auto const idx = str::index_at(s, first);
            auto const n = to_size<S>(std::distance(first, last));

            return s.erase(idx, n);
        }
        else if constexpr (has_non_member_iter2<S>) {
            return erase(s, first, last);
        }
        else {
            static_assert(has_non_member_idx<S>);

            auto const idx = str::index_at(s, first);
            auto const n = to_size<S>(std::distance(first, last));

            return erase(s, idx, n);
        }
    }

    struct api
    {
        template<random_access_string S>
            requires has_erase_range_function<S>
        void
        operator()(S& s, str_index_t<S> pos, str_size_t<S> n) const
        {
            erase_by_index(s, pos, n);
        }

        template<string S>
            requires has_erase_single_function<S>
        iterator_t<S>
        operator()(S& s, iterator_t<S const> pos) const
        {
            return erase_by_iter(s, pos);
        }

        template<string S, std::input_iterator I>
            requires std::convertible_to<I, iterator_t<S const>>
                  && has_erase_range_function<S>
        iterator_t<S>
        operator()(S& s, I first, sentinel_t<S const> last) const
        {
            return erase_by_iter_pair(s, first, last);
        }
    };

} // namespace _erase

inline constexpr _erase::api erase;

} // namespace stream9::strings

#endif // STRINGS_MODIFIER_ERASE_HPP
