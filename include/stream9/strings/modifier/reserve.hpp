#ifndef STRINGS_MODIFIER_RESERVE_HPP
#define STRINGS_MODIFIER_RESERVE_HPP

#include "../core/concepts.hpp"
#include "../core/string_traits.hpp"

//TODO move to ranges library
//TODO test

namespace stream9::strings {

namespace _reserve {

    template<typename S>
    concept has_member_reserve =
        requires (S&& s) {
            s.reserve(str_size_t<S>());
        };

    template<typename S>
    concept has_non_member_reserve =
        requires (S&& s) {
            reserve(s, str_size_t<S>());
        };

    template<typename S>
    concept has_reserve_function =
           has_member_reserve<S>
        || has_non_member_reserve<S>;

    template<string S>
        requires has_reserve_function<S>
    void reserve_space(S& s, str_size_t<S> const n)
    {
        if constexpr (has_member_reserve<S>) {
            s.reserve(n);
        }
        else {
            static_assert(has_non_member_reserve<S>);

            reserve(s, n);
        }
    }

    struct api
    {
        template<string S>
            requires has_reserve_function<S>
        void
        operator()(S& s, str_size_t<S> const n) const
        {
            reserve_space(s, n);
        }

    };

} // namespace _reserve

inline constexpr _reserve::api reserve;

template<string S>
    requires requires (S&& s) { reserve(s, str_size_t<S>()); }
void
reserve_if_possible(S& s, str_size_t<S> const n)
{
    reserve(s, n);
}

template<string S>
    requires (!requires (S&& s) { reserve(s, str_size_t<S>()); })
void
reserve_if_possible(S&, str_size_t<S>) {}

} // namespace stream9::strings

#endif // STRINGS_MODIFIER_RESERVE_HPP
