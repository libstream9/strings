#ifndef STRINGS_MODIFIER_REMOVE_SUFFIX_HPP
#define STRINGS_MODIFIER_REMOVE_SUFFIX_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"
#include "../core/string_traits.hpp"

#include "../accessor/begin.hpp"
#include "../accessor/end.hpp"
#include "../query/size.hpp"
#include "../query/iterator_at.hpp"

#include "erase.hpp"

#include <concepts>
#include <iterator>

namespace stream9::strings {

namespace _remove_suffix {

    template<typename S>
    concept has_member =
        requires (S s) {
            s.remove_suffix(0);
        };

    template<typename S>
    concept can_invoke_erase_with_index_range =
        requires (S s) {
            str::erase(s, 0, 0);
        };

    template<typename S>
    concept can_invoke_erase_with_iter_range =
        requires (S s) {
            str::erase(s, str::begin(s), str::begin(s));
        };

    template<typename S>
        requires has_member<S>
              || can_invoke_erase_with_index_range<S>
              || can_invoke_erase_with_iter_range<S>
    void
    remove_suffix_with_length(S& s, str_size_t<S> const n)
    {
        if constexpr (has_member<S>) {
            s.remove_suffix(n);
        }
        else if constexpr (can_invoke_erase_with_index_range<S>) {
            auto len = str::size(s);
            auto const i = n > len ? 0u : (len - n);

            str::erase(s, to_index<S>(i), n);
        }
        else {
            static_assert(can_invoke_erase_with_iter_range<S>);

            auto const idx = str::size(s) - n;
            auto const first = iterator_at(s, idx);

            s.erase(first, str::end(s));
        }
    }

    template<typename S>
        requires has_member<S>
              || can_invoke_erase_with_iter_range<S>
    void
    remove_suffix_with_iter(S& s, iterator_t<S const> const it)
    {
        if constexpr (has_member<S>) {
            auto const n = to_size<S>(std::distance(it, str::end(s)));

            s.remove_suffix(n);
        }
        else {
            static_assert(can_invoke_erase_with_iter_range<S>);

            str::erase(s, it, str::end(s));
        }
    }

    struct api
    {
        template<string S, std::integral I> // delay conversion of I as long as possible so member function can have flexibility
            requires std::convertible_to<I, str_size_t<S>>
                  && (   has_member<S>
                      || can_invoke_erase_with_index_range<S>
                      || can_invoke_erase_with_iter_range<S>)
        void
        operator()(S& s, I const n) const
        {
            remove_suffix_with_length(s, n);
        }

        template<string S>
            requires has_member<S>
                  || can_invoke_erase_with_iter_range<S>
        void
        operator()(S& s, iterator_t<S const> it) const
        {
            remove_suffix_with_iter(s, it);
        }
    };

} // namespace _remove_suffix

inline constexpr _remove_suffix::api remove_suffix;

} // namespace stream9::strings

#endif // STRINGS_MODIFIER_REMOVE_SUFFIX_HPP
