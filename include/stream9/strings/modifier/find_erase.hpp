#ifndef STRINGS_MODIFIER_FIND_ERASE_HPP
#define STRINGS_MODIFIER_FIND_ERASE_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"
#include "../core/finder.hpp"
#include "../core/regex.hpp"

#include "../query/find_first.hpp"
#include "../modifier/erase.hpp"

#include <type_traits>

namespace stream9::strings {

namespace _find_erase {

    template<typename S1, typename S2>
    concept can_invoke_find_first_with_str =
        requires (S1&& s1, S2&& s2) {
            str::find_first(s1, s2);
        };

    template<typename S, typename R>
    concept can_invoke_find_first_with_regexp =
        requires (S&& s, R&& re) {
            str::find_first(s, re);
        };

    template<typename S, typename F>
    concept can_invoke_find_first_with_finder =
        requires (S&& s, F&& f) {
            str::find_first(s, f);
        };

    template<typename S>
    concept can_invoke_erase =
        requires (S&& s) {
            str::erase(s, str::begin(s), str::end(s));
        };

    struct api
    {
        template<forward_string S, forward_string KeywordT>
            requires is_same_char_v<S, KeywordT>
                  && can_invoke_find_first_with_str<S, KeywordT>
                  && can_invoke_erase<S>
        void
        operator()(S& s1, KeywordT&& s2) const
        {
            auto const r = str::find_first(s1, s2);
            str::erase(s1, r.begin(), r.end());
        }

        template<bidirectional_string S, regexp R>
            requires is_same_char_v<S, R>
                  && can_invoke_find_first_with_regexp<S, R>
                  && can_invoke_erase<S>
        void
        operator()(S& s, R&& e) const
        {
            auto const r = str::find_first(s, e);
            str::erase(s, r.begin(), r.end());
        }

        template<forward_string S, typename FinderT>
            requires finder<FinderT, S>
                  && can_invoke_find_first_with_finder<S, FinderT>
                  && can_invoke_erase<S>
        void
        operator()(S& s1, FinderT&& finder) const
        {
            auto const r = finder(s1);
            str::erase(s1, r.begin(), r.end());
        }
    };

} // namespace _find_erase

inline constexpr _find_erase::api find_erase;

} // namespace stream9::strings

#endif // STRINGS_MODIFIER_FIND_ERASE_HPP
