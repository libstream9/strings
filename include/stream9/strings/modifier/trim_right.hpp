#ifndef STRINGS_MODIFIER_TRIM_RIGHT_HPP
#define STRINGS_MODIFIER_TRIM_RIGHT_HPP

#include "../core/char/classifier.hpp"
#include "../core/concepts.hpp"
#include "../core/string_traits.hpp"

#include "../accessor/begin.hpp"
#include "../accessor/end.hpp"

#include "remove_suffix.hpp"

#include <algorithm>
#include <concepts>
#include <locale>

namespace stream9::strings {

namespace _trim_right {

    namespace rng = std::ranges;
    namespace str = strings;

    template<typename S, typename I>
    concept has_remove_suffix =
        requires (S s, I i) {
            str::remove_suffix(s, i);
        };

    template<typename S, typename PredicateT>
    auto
    find_suffix(S&& s, PredicateT pred) //TODO this should be find_last_not_of maybe
    {
        if constexpr (bidirectional_string<S>) {
            auto first = rng::find_if_not(
                str::rbegin(s), str::rend(s), pred
            );

            return first.base();
        }
        else {
            auto result = str::begin(s);
            auto last = str::end(s);

            for (auto it = str::begin(s); it != last; ++it) {
                if (!pred(*it)) {
                    result = it;
                }
            }

            return result;
        }
    }

    struct api
    {
        template<forward_string S>
        void
        operator()(S& s, std::locale const& loc = {}) const
        {
            operator()(s, str::is_space<str_char_t<S>>(loc));
        }

        template<forward_string S, typename PredicateT>
            requires std::predicate<PredicateT, str_char_t<S>>
                  && has_remove_suffix<S, iterator_t<S>>
        void
        operator()(S& s, PredicateT pred) const
        {
            auto const it = find_suffix(s, pred);

            str::remove_suffix(s, it);
        }
    };

} // namespace _trim_right

inline constexpr _trim_right::api trim_right;

} // namespace stream9::strings

#endif // STRINGS_MODIFIER_TRIM_RIGHT_HPP
