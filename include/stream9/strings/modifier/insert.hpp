#ifndef STRINGS_MODIFIER_INSERT_HPP
#define STRINGS_MODIFIER_INSERT_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"
#include "../core/string_traits.hpp"

#include "../accessor/begin.hpp"
#include "../accessor/data.hpp"
#include "../accessor/end.hpp"
#include "../query/index_at.hpp"
#include "../query/iterator_at.hpp"
#include "../query/size.hpp"

#include <concepts>
#include <ranges>

#include <stream9/container/modifier/insert.hpp>

namespace stream9::strings {

namespace _insert {

    //TODO if this is constexpr bool instead of concept, logical operation
    //     won't short cut, even when it encounter false expression.
    //     need to check out othe place too.
    template<typename S, typename C>
    concept has_member_insert_ch_by_idx =
        random_access_string<S> &&
        requires (S s, str_index_t<S> pos, C ch) {
            s.insert(pos, ch);
        };

    template<typename S, typename C>
    concept has_member_insert_ch_by_iter =
        string<S> &&
        requires (S s, iterator_t<S const> pos, C ch) {
            s.insert(pos, ch);
        };

    template<typename S, typename C>
    concept has_adl_insert_ch_by_idx =
        random_access_string<S> &&
        requires (S s, str_index_t<S> pos, C ch) {
            insert(s, pos, ch);
        };

    template<typename S, typename C>
    concept has_adl_insert_ch_by_iter =
        string<S> &&
        requires (S s, iterator_t<S const> pos, C ch) {
            insert(s, pos, ch);
        };

    template<typename S1, typename S2>
    concept has_member_insert_str_by_idx =
        string<S1> &&
        contiguous_string<S2> &&
        requires (S1 s1, S2 s2) {
            s1.insert(str_index_t<S1>(), str::data(s2), str::size(s2));
        };

    template<typename S1, typename S2>
    concept has_adl_insert_str_by_idx =
        string<S1> &&
        contiguous_string<S2> &&
        requires (S1 s1, S2 s2) {
            insert(s1, str_index_t<S1>(), str::data(s2), str::size(s2));
        };

    template<typename S1, typename S2>
    concept has_member_insert_str_by_iter =
        string<S1> &&
        string<S2> &&
        requires (S1 s1, S2 s2) {
            s1.insert(str::begin(s1), str::begin(s2), str::end(s2));
        };

    template<typename S1, typename S2>
    concept has_adl_insert_by_iter =
        string<S1> &&
        string<S2> &&
        requires (S1 s1, S2 s2) {
            insert(s1, str::begin(s1), str::begin(s2), str::end(s2));
        };

    template<typename S1, typename S2>
    concept has_container_insert_str_by_iter =
        string<S1> &&
        string<S2> &&
        requires (S1 s1, S2 s2) {
            con::insert(s1, str::begin(s1), str::begin(s2), str::end(s2));
        };

    template<typename S1, typename C>
    concept has_insert_ch_function =
           has_member_insert_ch_by_idx<S1, C>
        || has_adl_insert_ch_by_idx<S1, C>
        || has_member_insert_ch_by_iter<S1, C>
        || has_adl_insert_ch_by_iter<S1, C>;

    template<typename S1, typename S2>
    concept has_insert_str_function =
           has_member_insert_str_by_idx<S1, S2>
        || has_adl_insert_str_by_idx<S1, S2>
        || has_member_insert_str_by_iter<S1, S2>
        || has_adl_insert_by_iter<S1, S2>
        || has_container_insert_str_by_iter<S1, S2>;

    /*
     * insert character by index
     */
    template<typename S, typename IndexT, typename C>
    void
    insert_char_by_index(S& s, IndexT const pos, C const ch)
    {
        if constexpr (has_member_insert_ch_by_idx<S, C>) {
            s.insert(pos.ch);
        }
        else if constexpr (has_member_insert_ch_by_iter<S, C>) {
            s.insert(str::iterator_at(s, pos), ch);
        }
        else if constexpr (has_adl_insert_ch_by_idx<S,C>) {
            insert(s, pos, ch);
        }
        else {
            static_assert(has_adl_insert_ch_by_iter<S, C>);
            insert(s, str::iterator_at(s, pos), ch);
        }
    }

    /*
     * insert character by iterator
     */
    template<typename S, typename IteratorT, typename C>
    iterator_t<S>
    insert_char_by_iter(S& s, IteratorT const it, C const ch)
    {
        if constexpr (has_member_insert_ch_by_iter<S, C>) {
            return s.insert(it, ch);
        }
        else if constexpr (has_member_insert_ch_by_idx<S, C>) {
            return s.insert(str::index_at(s, it), ch);
        }
        else if constexpr (has_adl_insert_ch_by_iter<S, C>) {
            return insert(s, it, ch);
        }
        else {
            static_assert(has_adl_insert_ch_by_idx<S, C>);
            return insert(s, str::index_at(s, it), ch);
        }
    }

    /*
     * insert string by index
     */
    template<typename S1, typename IndexT, typename S2>
    void
    insert_str_by_index(S1& s1, IndexT const pos, S2&& s2)
    {
        if constexpr (has_member_insert_str_by_idx<S1, S2>) {
            s1.insert(to_size<S1>(pos), str::data(s2), to_size<S1>(str::size(s2)));
        }
        else if constexpr (has_member_insert_str_by_iter<S1, S2>) {
            s1.insert(iterator_at(s1, pos), str::begin(s2), str::end(s2));
        }
        else if constexpr (has_adl_insert_str_by_idx<S1, S2>) {
            insert(s1, to_size<S1>(pos), str::data(s2), to_size<S1>(str::size(s2)));
        }
        else if constexpr (has_adl_insert_by_iter<S1, S2>) {
            insert(s1, iterator_at(s1, pos), str::begin(s2), str::end(s2));
        }
        else {
            static_assert(has_container_insert_str_by_iter<S1, S2>);
            return con::insert(s1, iterator_at(s1, pos),
                                   str::begin(s2), str::end(s2));
        }
    }

    /*
     * insert string by iterator
     */
    template<typename S1, typename IteratorT, typename S2>
    iterator_t<S1>
    insert_str_by_iter(S1& s1, IteratorT const pos, S2&& s2)
    {
        if constexpr (has_member_insert_str_by_iter<S1, S2>) {
            return s1.insert(pos, str::begin(s2), str::end(s2));
        }
        else if constexpr (has_member_insert_str_by_idx<S1, S2>) {
            return s1.insert(str::index_at(s1, pos),
                             str::data(s2), to_size<S1>(str::size(s2)));
        }
        else if constexpr (has_adl_insert_by_iter<S1, S2>) {
            return insert(s1, pos, str::begin(s2), str::end(s2));
        }
        else if constexpr (has_adl_insert_str_by_idx<S1, S2>) {
            return insert(s1, str::index_at(s1, pos),
                              str::data(s2), to_size<S1>(str::size(s2)));
        }
        else {
            static_assert(has_container_insert_str_by_iter<S1, S2>);
            return con::insert(s1, pos, str::begin(s2), str::end(s2));
        }
    }

    struct api
    {
        /*
         * insert a character to a given positiona of a string
         */
        template<random_access_string S, character C>
            requires is_same_char_v<S, C>
                  && has_insert_ch_function<S, C>
        void
        operator()(S& s, str_index_t<S> pos, C ch) const
        {
            insert_char_by_index(s, pos, ch);
        }

        template<string S, std::input_iterator I, character C>
            requires is_same_char_v<S, C>
                  && std::convertible_to<I, iterator_t<S const>>
                  && has_insert_ch_function<S, C>
        iterator_t<S>
        operator()(S& s, I it, C ch) const
        {
            return insert_char_by_iter(s, it, ch);
        }

        /*
         * insert a string to a given positiona of other string
         */
        template<random_access_string S1, string S2>
            requires is_same_char_v<S1, S2>
                  && has_insert_str_function<S1, S2>
        void
        operator()(S1& s1, str_index_t<S1> pos, S2&& s2) const
        {
            insert_str_by_index(s1, pos, s2);
        }

        template<string S1, std::input_iterator I, string S2>
            requires is_same_char_v<S1, S2>
                  && std::convertible_to<I, iterator_t<S1 const>>
                  && has_insert_str_function<S1, S2>
        iterator_t<S1>
        operator()(S1& s1, I pos, S2&& s2) const
        {
            return insert_str_by_iter(s1, pos, s2);
        }

    };

} // namespace _insert

inline constexpr _insert::api insert;

} // namespace stream9::strings

#endif // STRINGS_MODIFIER_INSERT_HPP
