#ifndef STRINGS_FINDER_LAST_FINDER_HPP
#define STRINGS_FINDER_LAST_FINDER_HPP

#include "../namespace.hpp"

#include "first_finder.hpp"

#include "../core/concepts.hpp"
#include "../core/finder.hpp"
#include "../core/string_range.hpp"
#include "../core/string_traits.hpp"
#include "../core/char/classifier.hpp"
#include "../core/char/comparator.hpp"

#include "../accessor/begin.hpp"
#include "../accessor/data.hpp"
#include "../accessor/end.hpp"
#include "../query/iterator_at.hpp"

#include <algorithm>
#include <iterator>

namespace stream9::strings {

namespace _last_finder {

    template<typename, typename> class last_finder;

    /*
     * search by character
     */
    template<typename S>
    inline constexpr bool std_string_ch = requires (S s, str_char_t<S> c) {
        s.rfind(c) == s.npos;
    };

    template<typename S, typename C, typename ComparatorT>
    string_range<iterator_t<S>>
    rfind_char(S&& s, C const c, ComparatorT comp)
    {
        auto const rfirst = str::rbegin(s);
        auto const rlast = str::rend(s);
        auto const first = str::begin(s);

        if (rfirst == rlast) {
            return { first, first };
        }

        auto const rit = rng::find_if(rfirst, rlast,
                                      [&](auto ch) { return comp(c, ch); });
        if (rit == rlast) {
            return { first, first };
        }
        else {
            auto const it = std::prev(rit.base());

            return { it, std::next(it) };
        }
    }

    template<typename S, typename C>
    string_range<iterator_t<S>>
    rfind_char(S&& s, C const c)
    {
        if constexpr (std_string_ch<S>) {
            auto const idx = s.rfind(c);

            if (idx == s.npos) {
                auto const first = str::begin(s);

                return { first, first };
            }
            else {
                auto const it1 = iterator_at(s, idx);

                return { it1, it1 + 1 };
            }
        }
        else {
            return rfind_char(s, c, str::equal_to<C>());
        }
    }

    template<character C>
    class last_finder<C, void>
    {
    public:
        last_finder(C const c)
            : m_test { c } {}

        template<bidirectional_string S>
            requires is_same_char_v<S, C>
        string_range<iterator_t<S>>
        operator()(S&& s) const
        {
            return rfind_char(s, m_test);
        }

    private:
        C m_test;
    };

    template<character C>
    last_finder(C) -> last_finder<C, void>;

    template<character C, typename ComparatorT>
        requires char_comparator<ComparatorT, C>
    class last_finder<C, ComparatorT>
    {
    public:
        last_finder(C const c, ComparatorT comp)
            : m_test { c }
            , m_comp { comp } {}

        last_finder(C const c)
            : m_test { c } {}

        template<bidirectional_string S>
            requires is_same_char_v<S, C>
        string_range<iterator_t<S>>
        operator()(S&& s) const
        {
            return rfind_char(s, m_test, m_comp);
        }

    private:
        C m_test;
        ComparatorT m_comp;
    };

    template<character C, typename ComparatorT>
    last_finder(C, ComparatorT) -> last_finder<C, ComparatorT>;

    /*
     * find by classifier
     */
    template<character C, classifier<C> Fn>
    class last_finder<C, Fn>
    {
    public:
        last_finder(Fn pred)
            : m_pred { std::move(pred) }
        {}

        template<bidirectional_string S>
            requires is_same_char_v<S, C>
        string_range<iterator_t<S>>
        operator()(S&& s_) const
        {
            string_range s = s_;

            auto const rfirst = str::rbegin(s);
            auto const rlast = str::rend(s);

            auto const rit = rng::find_if(rfirst, rlast, m_pred);
            auto const first = str::begin(s);

            if (rit == rlast) {
                return { first, first };
            }
            else {
                auto const it = std::prev(rit.base(), 1);

                return { it, std::next(it) };
            }
        }

    private:
        Fn m_pred;
    };

    /*
     * search by string
     */
    template<typename T>
    inline constexpr bool std_string_str = requires (T s, str_char_t<T>* ptr) {
        s.rfind(ptr, 0, 0) == s.npos;
    };

    template<typename S1, typename S2, typename ComparatorT>
    string_range<iterator_t<S1>>
    rfind_substr(S1&& s1, S2&& s2, ComparatorT comp)
    {
        auto const r = rng::search(str::rbegin(s1), str::rend(s1),
                                   str::rbegin(s2), str::rend(s2), comp);
        if (!r) {
            auto const first = str::begin(s1);

            return { first, first };
        }
        else {
            auto const it1 = r.end().base();
            auto const it2 = r.begin().base();

            return { it1, it2 };
        }
    }

    template<typename S1, typename S2>
    string_range<iterator_t<S1>>
    rfind_substr(S1&& s1, S2&& s2)
    {
        if constexpr (std_string_str<S1> && contiguous_string<S2>) {
            auto const idx = s1.rfind(s2.data(), s1.npos, to_size<S1>(s2.size()));

            if (idx == s1.npos) {
                auto const first = str::begin(s1);

                return { first, first };
            }
            else {
                auto const first = iterator_at(s1, idx);
                auto const last = iterator_at(s1, idx + to_size<S1>(s2.size()));

                return { first, last };
            }
        }
        else {
            return rfind_substr(s1, s2, str::equal_to<str_char_t<S1>>());
        }
    }

    /*
     * public interface
     */
    template<bidirectional_string S>
    class last_finder<S, void>
    {
    public:
        template<bidirectional_string T>
        last_finder(T&& pattern)
            : m_pattern { pattern } {}

        template<bidirectional_string T>
            requires is_same_char_v<S, T>
        string_range<iterator_t<T>>
        operator()(T&& s) const
        {
            return rfind_substr(s, m_pattern);
        }

    private:
        string_range<iterator_t<S>> m_pattern;
    };

    template<bidirectional_string S>
    last_finder(S&&) -> last_finder<_unref<S>, void>;

    template<bidirectional_string S, typename ComparatorT>
        requires char_comparator<ComparatorT, str_char_t<S>>
    class last_finder<S, ComparatorT>
    {
    public:
        template<bidirectional_string T>
        last_finder(T&& pattern, ComparatorT comp)
            : m_pattern { pattern }
            , m_comp { comp } {}

        template<bidirectional_string T>
            requires is_same_char_v<S, T>
        string_range<iterator_t<T>>
        operator()(T&& s) const
        {
            return rfind_substr(s, m_pattern, m_comp);
        }

    private:
        string_range<iterator_t<S>> m_pattern;
        ComparatorT m_comp;
    };

    template<bidirectional_string S, typename PredicateT>
    last_finder(S&&, PredicateT&&) -> last_finder<_unref<S>, _unref<PredicateT>>;

} // namespace _last_finder

using _last_finder::last_finder;

template<typename T, typename U>
struct finder_traits<last_finder<T, U>>
{
    using direction = backward_finder_tag;
};

} // namespace stream9::strings

#endif // STRINGS_FINDER_LAST_FINDER_HPP
