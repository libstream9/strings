#ifndef STRINGS_REGEX_RE2_HPP
#define STRINGS_REGEX_RE2_HPP

#include "../core/concepts.hpp"
#include "../core/regex.hpp"
#include "../core/string_range.hpp"
#include "../core/string_traits.hpp"

#include "../accessor/data.hpp"
#include "../query/iterator_at.hpp"

#include <re2/re2.h>
#include <re2/stringpiece.h>

namespace stream9::strings {

template<>
struct regex_adaptor<re2::RE2>
{
    using char_type = char;

    template<contiguous_string S>
        requires is_same_char_v<S, char>
    static string_range<iterator_t<S>>
    find(S&& s, re2::RE2 const& re)
    {
        re2::StringPiece sp { str::data(s), static_cast<size_t>(str::size(s)) };
        re2::StringPiece match;

        if (re.Match(sp, 0, sp.size(), re.UNANCHORED, &match, 1)) {
            auto const index = match.begin() - sp.begin();
            auto const first = str::iterator_at(s, index);
            auto const last = first + to_difference<S>(match.size());

            return { first, last };
        }
        else {
            auto const last = str::end(s);

            return { last, last };
        }
    }
};

} // namespace stream9::strings

#endif // STRINGS_REGEX_RE2_HPP
