#ifndef STRINGS_REGEX_PCRE2_HPP
#define STRINGS_REGEX_PCRE2_HPP

#include "../core/concepts.hpp"
#include "../core/string_range.hpp"
#include "../core/regex.hpp"

#include "../accessor/data.hpp"
#include "../accessor/end.hpp"
#include "../query/iterator_at.hpp"
#include "../query/size.hpp"

#include <pcre2.h>

namespace stream9::strings {

template<>
struct regex_adaptor<pcre2_code*>
{
    using char_type = char;

    class match_data
    {
    public:
        match_data(pcre2_code* const re)
        {
            m_ptr = ::pcre2_match_data_create_from_pattern(re, nullptr);
        }

        ~match_data() noexcept
        {
            ::pcre2_match_data_free(m_ptr);
        }

        auto*
        ovector() const
        {
            return ::pcre2_get_ovector_pointer(m_ptr);
        }

        auto
        get() const
        {
            return m_ptr;
        }

    private:
        pcre2_match_data* m_ptr = nullptr;
    };

    template<contiguous_string S>
        requires is_same_char_v<S, char>
    static string_range<iterator_t<S>>
    find(S&& s, pcre2_code* const re)
    {
        match_data m { re };
        auto const ptr = str::data(s);
        auto const len = str::size(s);

        auto const rc = ::pcre2_match(
            re,
            reinterpret_cast<PCRE2_SPTR>(ptr),
            static_cast<std::size_t>(len),
            0, // start offset
            0, // default option
            m.get(),
            nullptr // default match context
        );

        if (rc < 0) {
            auto const last = str::end(s);
            return { last, last };
        }

        auto const ovector = m.ovector();
        auto const first = str::iterator_at(s, ovector[0]);
        auto const last = str::iterator_at(s, ovector[1]);

        return { first, last };
    }
};

} // namespace stream9::strings

#endif // STRINGS_REGEX_PCRE2_HPP
